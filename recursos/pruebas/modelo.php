<?php
	require_once dirname(__DIR__).'/config/conexion.php';

	class Model_addUser{

		function __construct(){	}

		public static function adduser($empleado, $usuario, $password){
			$s = conexion::con();
			$query ="insert into Usuario VALUES(:id, :usuario, :pass)";
			$prepared = $s->prepare($query);
			$prepared->bindParam(":id", $empleado);
			$prepared->bindParam(":usuario", $usuario);
			$prepared->bindParam(":pass", $password);
			$prepared->execute();
		}
	}
?>