function buscar(id){

	if (document.getElementById("date").verify){

	}
	tabla = document.querySelector("table");
	tabla.tBodies.result.remove();

	tbody = document.createElement('tbody');
	tbody.setAttribute('name', 'result');

	tabla.append(tbody);
	fecha = document.getElementById("date").value;
	request(id, fecha)

}

function request(id, fecha){
	url="http://localhost/proyecto_hospital/app/ServiceWeb/Expediente_rest.php";
	ajax = new XMLHttpRequest();
	ajax.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			json = JSON.parse(this.responseText);
			vista(json);
		} //fin del if
	}
	ajax.open('POST',url,true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send('id='+id+'&fecha='+fecha);
}

function vista(json){
	// console.log(json);
	tabla = document.querySelector("table");
	tabla.tBodies.result.remove();

	tbody = document.createElement('tbody');
	tbody.setAttribute('name', 'result');

	tabla.append(tbody);
	//"javascript:ventanaSecundaria('receta',{$value['Folio']})"

	if (json['Notas Medicas'] != null){
		tam = json['Notas Medicas'].length;
		
		for (i = 0 ; i < tam; i++) {
			a = document.createElement('a');
			a.setAttribute('class','btn');
			a.textContent = 'abrir';
			a.href = "javascript:ventanaSecundaria('nota',"+json['Notas Medicas'][i].IdNota+")";

			tr = document.createElement('tr');


			td1 = document.createElement('td');
			td2 = document.createElement('td');
			td3 = document.createElement('td');
			td4 = document.createElement('td');
			td5 = document.createElement('td');

			td1.textContent = json['Notas Medicas'][i].Fecha;
			td2.textContent = 'Notas Medicas';
			td3.textContent = json['Notas Medicas'][i].Medico
			td4.textContent = json['Notas Medicas'][i].Especialidad
			td5.append(a);

			tr.append(td1);
			tr.append(td2);
			tr.append(td3);
			tr.append(td4);
			tr.append(td5);

			tabla.tBodies.result.append(tr);

			//console.log(json['Notas Medicas'][i].IdNota);
			//console.log(json['Notas Medicas'][i].Fecha);
			//console.log('Notas Medicas');
			//console.log(json['Notas Medicas'][i].Medico);
			//console.log(json['Notas Medicas'][i].Especialidad);
		}
	}

	if (json['Recetas Medicas'] != null){
		tam = json['Recetas Medicas'].length;
		
		for (i = 0 ; i < tam; i++) {
			a = document.createElement('a');
			a.setAttribute('class','btn');
			a.textContent = 'abrir';
			a.href = "javascript:ventanaSecundaria('receta',"+json['Recetas Medicas'][i].Folio+")";

			tr = document.createElement('tr');
			
			td1 = document.createElement('td');
			td2 = document.createElement('td');
			td3 = document.createElement('td');
			td4 = document.createElement('td');
			td5 = document.createElement('td');

			td1.textContent = json['Recetas Medicas'][i].Fecha;
			td2.textContent = 'Recetas Medicas';
			td3.textContent = json['Recetas Medicas'][i].Medico
			td4.textContent = json['Recetas Medicas'][i].Especialidad
			td5.append(a);

			tr.append(td1);
			tr.append(td2);
			tr.append(td3);
			tr.append(td4);
			tr.append(td5);

			tabla.tBodies.result.append(tr);
			//console.log(json['Recetas Medicas'][i].Folio);
		}
	}

	if (json['Certificados'] != null){
		tam = json['Certificados'].length;

		for (i = 0 ; i < tam; i++) {
			a = document.createElement('a');
			a.setAttribute('class','btn');
			a.textContent = 'abrir';
			a.href = "javascript:ventanaSecundaria('certificado',"+json['Certificados'][i].IdcerM+")";
			tr = document.createElement('tr');
			
			td1 = document.createElement('td');
			td2 = document.createElement('td');
			td3 = document.createElement('td');
			td4 = document.createElement('td');
			td5 = document.createElement('td');

			td1.textContent = json['Certificados'][i].FechaActual;
			td2.textContent = 'Certificados';
			td3.textContent = json['Certificados'][i].Medico
			td4.textContent = json['Certificados'][i].Especialidad
			td5.append(a);

			tr.append(td1);
			tr.append(td2);
			tr.append(td3);
			tr.append(td4);
			tr.append(td5);

			tabla.tBodies.result.append(tr);

			//console.log(json['Certificados'][i].IdCertificado);
		}
	}

	if (json['Constancias'] != null){
		tam = json['Constancias'].length;

		for (i = 0 ; i < tam; i++) {
			a = document.createElement('a');
			a.setAttribute('class','btn');
			a.textContent = 'abrir';
			a.href = "javascript:ventanaSecundaria('constancia',"+json['Constancias'][i].IdConsM+")";
			tr = document.createElement('tr');
			
			td1 = document.createElement('td');
			td2 = document.createElement('td');
			td3 = document.createElement('td');
			td4 = document.createElement('td');
			td5 = document.createElement('td');

			td1.textContent = json['Constancias'][i].FechaActual;
			td2.textContent = 'Constancias';
			td3.textContent = json['Constancias'][i].Medico
			td4.textContent = json['Constancias'][i].Especialidad
			td5.append(a);

			tr.append(td1);
			tr.append(td2);
			tr.append(td3);
			tr.append(td4);
			tr.append(td5);

			tabla.tBodies.result.append(tr);

			//console.log(json['Certificados'][i].IdCertificado);
		}
	}

	if (json['Resumenes'] != null){
		tam = json['Resumenes'].length;
		
		for (i = 0 ; i < tam; i++) {
			a = document.createElement('a');
			a.setAttribute('class','btn');
			a.textContent = 'abrir';
			a.href = "javascript:ventanaSecundaria('resumen',"+json['Resumenes'][i].IdResC+")";

			tr = document.createElement('tr');
			
			td1 = document.createElement('td');
			td2 = document.createElement('td');
			td3 = document.createElement('td');
			td4 = document.createElement('td');
			td5 = document.createElement('td');

			td1.textContent = json['Resumenes'][i].FechaActual;
			td2.textContent = 'Resumenes';
			td3.textContent = json['Resumenes'][i].Medico
			td4.textContent = json['Resumenes'][i].Especialidad
			td5.append(a);

			tr.append(td1);
			tr.append(td2);
			tr.append(td3);
			tr.append(td4);
			tr.append(td5);

			tabla.tBodies.result.append(tr);
			//console.log(json['Resumenes'][i].IdResumen);
		}
	}

	if (json['Interconsultas'] != null){
		tam = json['Interconsultas'].length;

		for (i = 0 ; i < tam; i++) {
			a = document.createElement('a');
			a.setAttribute('class','btn');
			a.textContent = 'abrir';
			a.href = "javascript:ventanaSecundaria('interconsultaInfo',"+json['Interconsultas'][i].IdInterconsulta+")";
			tr = document.createElement('tr');
			
			td1 = document.createElement('td');
			td2 = document.createElement('td');
			td3 = document.createElement('td');
			td4 = document.createElement('td');
			td5 = document.createElement('td');

			td1.textContent = json['Interconsultas'][i].Fecha;
			td2.textContent = 'Interconsultas';
			td3.textContent = json['Interconsultas'][i].Medico
			td4.textContent = json['Interconsultas'][i].Especialidad
			td5.append(a);

			tr.append(td1);
			tr.append(td2);
			tr.append(td3);
			tr.append(td4);
			tr.append(td5);

			tabla.tBodies.result.append(tr);
			//console.log(json['Interconsultas'][i].IdInterconsulta);
		}
	}


}
/*
window.onload = function (){
	alert("tecnobyte");
}*/