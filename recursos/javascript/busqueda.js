(function(){
	var Folio = document.getElementById("buscarfol");
	var Pila = document.getElementById("buscarAp");

	Folio.addEventListener('click', BuscarPorFolio, false);
	Pila.addEventListener('click', BuscarPorPila, false);
})();

(function(){
	document.getElementById('fol').addEventListener("keydown",function(event){
		if (event.keyCode == 13){
			BuscarPorFolio();
		}
	});

})();


function busqueda(posicion){
	tipo = document.getElementById("tipos").value;

	if(tipo == "folio"){
		if (document.getElementById("tmp") != null){
			posicion.resultado2.removeChild(document.getElementById("tmp"));
			posicion.resultado2.removeChild(document.getElementById("lista"));
		}
		document.getElementById("Bid").style.display= "block";
		document.getElementById("BAp").style.display= "none";
	}else{
		if(tipo == "apellido"){
			if (document.getElementById("tmp") != null){
			posicion.resultado1.removeChild(document.getElementById("tmp"));
			posicion.resultado1.removeChild(document.getElementById("lista"));
		}
			document.getElementById("BAp").style.display= "block";
			document.getElementById("Bid").style.display= "none";
		}else{
			document.getElementById("Bid").style.display= "none";
			document.getElementById("BAp").style.display= "none";
		}
	}
}

function request(folio, nombre, ap, am, tipoBusqueda, posicion){
	url = window.location.origin + "/proyecto_hospital/app/controller/controller_busqueda.php?action="+tipoBusqueda;
	ajax = new XMLHttpRequest();
	ajax.onreadystatechange = function(){
		if(this.status == 200 && this.readyState== 4){
			json = JSON.parse(this.responseText);
			if (document.getElementById("tmp") != null){
				posicion.removeChild(document.getElementById("tmp"));
				posicion.removeChild(document.getElementById("lista"));
			}
			tabla(json, posicion);
		}
	}
	ajax.open("POST", url, true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("folio="+folio+"&nombre="+nombre+"&paterno="+ap+"&materno="+am);
}

function BuscarPorFolio(){
	if(document.getElementById("fol").value === ""){
		alert("Ingrese un numero de expediente");
	}else{
		folio = document.getElementById("fol").value;
		posicion = document.getElementById("resultado1"); // <-- raiz padre
		request(folio, "", "", "", 1, posicion);

	}
}

function BuscarPorPila(){
	if(document.getElementById("dato1").value === "" && document.getElementById("dato2").value === "" && document.getElementById("dato3").value === ""){
		alert("todos los campos tienen que llenarse");
	}else{
		nombre = document.getElementById("dato1").value;
		ap = document.getElementById("dato2").value;
		am = document.getElementById("dato3").value;
		posicion = document.getElementById("resultado2"); // <-- raiz padre
		request("", nombre, ap, am, 2, posicion);
	}
}

function respuestas(resultado){
	if(resultado.Error =="vacio"){
		document.getElementById("mensaje").innerHTML = "No se encontro ninguna coincidencia";
	}else{
		dimension = resultado.length;
		for (i = 0 ; i < dimension; i++) {
			console.log(resultado[i].IdPaciente);
			console.log(resultado[i].NombrePaciente);
			console.log(resultado[i].PrimerApallidoP);
			console.log(resultado[i].SegundoApellidoP);
			console.log(resultado[i].SeguroPopular);
		}
	}
}

function tabla(resultado, posicion){

	if(resultado.Error =="vacio"){
		document.getElementById("mensaje").innerHTML = "No se encontro ninguna coincidencia";
	}else{
		document.getElementById("mensaje").innerHTML = "";
		momentoActual = new Date();
		dimension = resultado.length;

		h2 = document.createElement("h2");
		h2.setAttribute("id", "lista");
		h2.textContent="Expedientes";
		posicion.appendChild(h2);

		tablaR = document.createElement("table");
		tablaR.setAttribute("class", "table");
		tablaR.setAttribute("id", "tmp");

		thead = document.createElement("thead");
		tbody = document.createElement("tbody");

		nombre = new Array();
		nombre[0] = "Expediente";
		nombre[1] = "Nombre";
		nombre[2] = "Apellido paterno";
		nombre[3] = "Apellido materno";
		nombre[4] = "Edad";
		nombre[5] = "Seguro popular";
		
		thead = document.createElement("thead");
		tbody = document.createElement("tbody");

		trhead = document.createElement("tr");

		for (i = 0 ; i < nombre.length; i++) {
			th = document.createElement("th");
			th.textContent = nombre[i];
			trhead.appendChild(th);
		}

		for (i = 0 ; i < dimension; i++) {
			tr = document.createElement("tr");

			//----------------------------------- td
				td = document.createElement("td");
				td.textContent = resultado[i].IdPaciente;
				tr.appendChild(td);

				td = document.createElement("td");
				td.textContent = resultado[i].NombrePaciente;
				tr.appendChild(td);

				td = document.createElement("td");
				td.textContent = resultado[i].PrimerApallidoP;
				tr.appendChild(td);

				td = document.createElement("td");
				td.textContent = resultado[i].SegundoApellidoP;
				tr.appendChild(td);

				td = document.createElement("td");
				td.textContent = (momentoActual.getFullYear() - resultado[i].Fecha );
				tr.appendChild(td);
				
				td = document.createElement("td");
				td.textContent = resultado[i].SeguroPopular;
				tr.appendChild(td)

				a = document.createElement("a");
				a.textContent = "Abrir";
				a.setAttribute("href", "../Expediente/view_expediente.php?Expediente="+resultado[i].IdPaciente);
				a.setAttribute("class", "btn btn-primar");
				td = document.createElement("td");
				td.appendChild(a);
				tr.appendChild(td)
			
			//----------------------------------- td
			tbody.appendChild(tr);
		}

		thead.appendChild(trhead);
		tablaR.appendChild(thead);
		tablaR.appendChild(tbody);

		posicion.appendChild(tablaR);
	}

}