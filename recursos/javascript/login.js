(function(){
	ingresar = document.getElementById("ok");
	ingresar.addEventListener("click" , ingreso,false );

	cancelar = document.getElementById("null");
	cancelar.addEventListener("click", limpiar, false);
})();

(function(){
	addEventListener("keydown", function(event){
		//console.log(event.keyCode);
		if (event.keyCode == 13){
			this.ingreso();
		}
	})

})();


function servicio(){
	var ajax;

	if (window.XMLHttpRequest){
		ajax = new XMLHttpRequest(); //para navegadores mas modernos
		return ajax;
	}else{
		ajax = new ActiveXObject("Microsoft.XMLHTTP"); // para navegadores antiguos
		return ajax;
	}
	return null;
}

function enviar(a){
	url = window.location.origin+'/proyecto_hospital/app/controller/controller_login.php';
	ajax = servicio();
	ajax.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			json = JSON.parse(this.responseText);
			if(json.error){
				window.location.href = json.url;
			}else{
				document.getElementById("mensaje").innerHTML = json.mensaje;
			}

		} //fin del if
	}// fin del statechange

	ajax.open('POST', url, true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("usuario="+a[0].value+"&pass="+a[1].value); // <-- solo se aplica para post
} //fin de enviar

function ingreso(){
	a = document.getElementById("ingreso");
	enviar(a);
}
function limpiar(){
	a = document.getElementById("ingreso");
	a[0].value='';
	a[1].value='';
}