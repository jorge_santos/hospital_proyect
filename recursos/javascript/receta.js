function request(opcion, fecha, paciente, empleado, edad, dato1, dato2, dato3, dato4, diagnostico){
	url = self.location.origin+"/proyecto_hospital/app/controller/controller_AddHojasExpedientes.php";
	ajax = new XMLHttpRequest();
	ajax.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			//console.log(this.responseText);
		}
	}
	ajax.open('POST', url, true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("option="+opcion+"&fecha="+fecha+"&idPaciente="+paciente+"&IdEmpleado="+empleado+"&edad="+edad+"&Medicamento="+dato1+"&Presentacion="+dato2+"&cantidad="+dato3+"&Indicaciones="+dato4+"&diagnostico="+diagnostico);

}

function datos(){
	comprobacion = false;
	opcion = document.recetaM.children.option.value;
	fecha = document.recetaM.children.fecha.value;
	paciente = document.recetaM.children.idPaciente.value;
	empleado = document.recetaM.children.IdEmpleado.value;
	edad = document.recetaM.children.edad.value;

	var dato1 = Array();
	var dato2 = Array();
	var dato3 = Array();
	var dato4 = Array();

	nombre = document.getElementsByClassName('NombreM');
	presentacion = document.getElementsByClassName('presentacionM');
	cantidad = document.getElementsByClassName('cantidad');
	indicacion = document.getElementsByClassName('indicacion');

	diagnostico = document.getElementById('diag').value;

	a = nombre.length;

	if (document.recetaM.checkValidity()){ 
		for (i = 0; i < a;i++){
			dato1.push(nombre[i].value);
			dato2.push(presentacion[i].value);
			dato3.push(cantidad[i].value);
			dato4.push(indicacion[i].value);
		}
		
		this.request(opcion, fecha, paciente, empleado, edad, dato1, dato2, dato3, dato4, diagnostico);
		setTimeout(function(){
			alert("Datos guardados con exito");
			window.location.reload();
		}, 2000);
		
	}
}

function agregarMedicamento() { // no se uso, pero aun asi no borrar
	//var cantidad = 1;
	tabla = document.querySelector("table");
	tr = document.createElement("tr");

	td1 = document.createElement("td");
	select = document.createElement("select");
	select.name ="Medicamento"

	option = document.createElement("option");
	option.setAttribute("value","Paracetamol");
	option.textContent="Paracetamos";
	select.appendChild(option);
	td1.appendChild(select)

	td2 = document.createElement("td");
	select1 = document.createElement("select");
	select1.name ="Presentacion"

	option1 = document.createElement("option");
	option1.setAttribute("value","tableta");
	option1.textContent="tableta";
	select1.appendChild(option1);
	td2.appendChild(select1)

	td3 = document.createElement("td");
	input = document.createElement("input");
	input.type = "text";
	input.name = "cantidad";
	input.setAttribute("required","");
	td3.appendChild(input);

	td4 = document.createElement("td");
	img = document.createElement("img");
	img.src = "../../../recursos/iconos/icons8-waste-24.png";
	img.alt = "Eliminar Medicamento";
	//img.setAttribute("onclick","eliminarMedicamento("+this.cantidad+++")"); // <--Manda la posicion pero no funciono
	//aqui hiria un Tooltip, pero por tiempo no se implementa
	img.setAttribute("onclick","eliminarMedicamento()");
	td4.appendChild(img);

	td5 = document.createElement("td");
	area = document.createElement("textarea");
	area.name = "Indicaciones";
	area.cols = "40";
	area.rows = "3";
	area.setAttribute("required","");
	td5.appendChild(area);

	tr.appendChild(td1);
	tr.appendChild(td2);
	tr.appendChild(td3);
	tr.appendChild(td5);
	tr.appendChild(td4);

	//tabla.tBodies.cuerpo.insertRow(tr);
	tabla.tBodies.cuerpo.appendChild(tr);
}

function eliminarMedicamento() {
	tabla = document.querySelector("table");
	total = tabla.rows.length;
	if(total > 2){
		tabla.deleteRow(total-1);
		//tabla.deleteRow(posicion);
	}
}

// parte del ejercicio
// GET 3 -> json

//modificaciones

/*
eliminado la columna id medicamento receta
modifcado la columna fecha
eliminado el diagnostico 2

se modifico la columna indicaciones de la tabla recetaXexpedi
*/