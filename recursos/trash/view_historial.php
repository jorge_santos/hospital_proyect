<?php  
require_once '../../controller/controller_historial.php';
$Nexpediente = isset($_GET['expediente']) ? $_GET['expediente'] : '';
$Nexpediente = isset( $_COOKIE['expediente'] ) ? $_COOKIE['expediente'] : '';

    // filtrar la entrada del Nexpediente para evitar xss, csfx y sqlinyection
    // si no es valido la entrada, no ejecutar las function vista
$Expediente = new controller_historial();
$datosPaciente = $Expediente->paciente($Nexpediente);
$nacimiento = explode("-", $datosPaciente['FechaNacimiento']);
$edad = (date("Y") - $nacimiento[0]);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">

    <title>historial</title>
</head>

<body>
    <p>Nombre:<span><?php echo $datosPaciente['Nombre']?></span> | edad:<span><?php echo $edad?></span></p>
    <center>
        <h1>Número Expediente : <?php echo $Nexpediente ?></h1>
    </center>

    <div class="row">
        <noscript>
            <center>
                <h1>Habilitar javascript para una mejor experiencia</h1>
            </center>
        </noscript>
        <div class="col-lg-12 text-center">
            <div class="section-title">
                <div class="contenido-expediente">

                    <center>
                        <div class="table-responsive">

                            <table class="table" border="1" text-align="center">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Contenido</th>
                                        <th>Medico</th>
                                        <th>Especialidad</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $Expediente->vista($Nexpediente);
                                    ?>    
                                </tbody>
                            </table>
                            <!-- Limite de la tabla que se trabajara, hasta parar -->
                            
                        </div>
                    </center>

                </div>
            </div>
        </div>
    </div>

</body>

<script>
    function ventanaSecundaria(tipoVentana, id) {
        window.open("view_" + tipoVentana + ".php?id="+id, tipoVentana, "width=300,height=300");
    } 
</script>

</html>