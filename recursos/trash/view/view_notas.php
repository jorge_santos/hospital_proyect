<?php


if (isset($_REQUEST['Recetas'])){
    header("Location: view_recetaMedica.php");
}
if (isset($_REQUEST['Interconsulta'])){
    header("Location: view_interconsulta.php");
}
if (isset($_REQUEST['expediente'])){
    header("Location: view_expediente.php");
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/Dbusqueda.css"">
    <title>Nota medica</title>

    <style>
        #word{
            padding: 1% 1%;
            display: flex;
            flex-direction: column;
        }

    </style>
</head>

<body onload=" mueveReloj()">

    <div id="buscador">
        <div class="cabecera">
            <h2>Nota</h2>
        </div>
        <hr>
        <center>
            <div>
                <form method="post">
                    <p>Crear:</p>
                    <a focus="true" class="btn">Nota medica</a>
                    <button class="btn" id="receta" name="Recetas" value="receta">Receta medica</button>
                    <button class="btn" id="interconsulta" name="Interconsulta" value="interconsulta">Interconsulta</button>
                    <br><br><br><br><br><br><br>
                    <button class="btn" id="expediente" name="expediente" value="expediente">Volver al expediente</button>

                </form>
            </div>
        </center>
    </div>

    </div>
    <div style="display: flex;justify-content: space-between;">
        <img src="../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>
    <hr>

    <div id="word">
        <form>
            <label for="fc">Fc:</label>
            <input type="text" name="fc">

            <label for="fr">Fr:</label>
            <input type="text" name="fr">

            <label for="ta">Ta:</label>
            <input type="text" name="ta">

            <label for="temp">Temp:</label>
            <input type="text" name="temp">
            <br>
            <label for="talla">Talla:</label>
            <input type="text" name="talla">

            <label for="peso">Peso:</label>
            <input type="text" name="peso">

            <label for="dx">DX:</label>
            <input type="text" name="dx">

            <input type="hidden" name="option" value="Nota">

            <div class="form-group">
                <label for="exampleFormControlTextarea1" style="text-align:center;">Consulta medica</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>

            <input class="btn-accept" type="submit" value="Guardar">
        </form>
    </div>

    </body>
    <!-- <script src="../../recursos/javascript/doctor.js"></script> -->
    <script src="../../recursos/javascript/reloj.js"></script>
    <script src="../../recursos/javascript/slider.js"></script>


    <script>

    </script>

</html>