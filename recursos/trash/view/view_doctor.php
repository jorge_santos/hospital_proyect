<?php
session_start();
include_once dirname(__DIR__)."/controller/controller_doctor.php";

if ( !isset($_SESSION['inicio']) ){
    header("Location: ../../");
}

if (isset($_REQUEST['lobby'])){
    header("Location: ../../");
    session_destroy();
}

$controller = new controller_doctor();

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

<!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->

    
    <link rel="stylesheet" href="../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/Dcitas.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/Dbusqueda.css">
    <link rel=" stylesheet" href="../../recursos/estilos/doctores/Dexpedientes.css">

    <title>Doctores</title>
</head>

<body onload="mueveReloj()">
    <noscript>
        <center>
            <h1>Hbilitar javascript para una mejor experiencia</h1>
        </center>
    </noscript>

    <div id="buscador">
        <div class="cabecera">
            <h2>Doctores</h2>
        </div>
        <hr>
        <center>
            <div>
                <h3 style="color: white"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h3>
                <a focus="true" class="btn">Citas</a>
                <a href="view_busqueda.php" class="btn">Busqueda</a>
                <br><br><br><br><br><br><br>
                <form method="post">
                    <button class="btn" id="lobby" name="lobby" value="lobby">Cerrar sesion</button>
                </form>
            </div>
        </center>
    </div>
    </div>

    <div style="display: flex;justify-content: space-between;">
        <img src="../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()" alt="Ocultar">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>
    <hr>

    <div id="Dcitas" class="modulos">
        <center>
            <div class="table-responsive">
                <h2>
                    <caption>Citas Medicas</caption>
                </h2>

                <? controller_doctor::vista(); ?>
            </div>
        </center>
    </div>

</body>

<!-- <script src="../../recursos/javascript/doctor.js"></script> -->
<script src="../../recursos/javascript/reloj.js"></script>
<script src="../../recursos/javascript/slider.js"></script>

<script type="text/javascript">

</script>

</html>