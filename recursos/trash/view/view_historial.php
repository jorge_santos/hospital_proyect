<?php  
	require_once dirname(__DIR__).'/controller/controller_historial.php';
    $Nexpediente = isset($_GET['expediente']) ? $_GET['expediente'] : '';
    $Nexpediente = isset( $_COOKIE['expediente'] ) ? $_COOKIE['expediente'] : '';
    
    // filtrar la entrada del Nexpediente para evitar xss, csfx y sqlinyection
    // si no es valido la entrada, no ejecutar las function vista
    $Expediente = new controller_historial();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->

    <link rel="stylesheet" href="../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/Dcitas.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/Dbusqueda.css">
    <link rel=" stylesheet" href="../../recursos/estilos/doctores/Dexpedientes.css">
    <title>historial</title>
</head>

<body>
    <center>
        <h1>Numero Expediente : <?php echo $Nexpediente ?></h1>
    </center>

    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="section-title">
                <div class="contenido-expediente">

                    <center>
                        <div class="table-responsive">

                            <table class="table" border="1" text-align="center">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Contenido</th>
                                        <th>Medico</th>
                                        <th>Especialidad</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                        $Expediente->vista($Nexpediente);
                                    ?>    
                                </tbody>
                            </table>
                            <!-- Limite de la tabla que se trabajara, hasta parar -->
                            
                        </div>
                    </center>

                </div>
            </div>
        </div>
    </div>

</body>

<script>
    function ventanaSecundaria(tipoVentana) {
        window.open("view_" + tipoVentana + ".html", tipoVentana, "width=300,height=300");
    } 
</script>

</html>