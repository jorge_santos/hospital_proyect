<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/Dbusqueda.css">

    <style>
         table, th, td {
            border: 1px solid black;
         }
      </style>
    <title>Reportes</title>
</head>
<body onload="mueveReloj()"> 
<div id="buscador">
        <div class="cabecera">
            <h2>Doctores</h2>
        </div>
        <hr>
        <center>
            <div>
                <a href="view_doctor.php" class="btn">Citas</a>
                <a href="view_busqueda.php" class="btn">Busqueda</a>
                <a focus="true" class="btn">Reporte</a>
            </div>   
        </center>
        </div>

    </div>
    <div style="display: flex;justify-content: space-between;">
        <img src="../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>
    
<div id="Co" class="tras">
<label>Tipo de Reporte a realizarse</label>
        <select>
                <option> Seleccionar </option>
                <option value="resumen"> Resumen </option>
                <option value="certificado"> Certificado </option>
                <option value="prenupcial"> Prenupcial </option>
        </select>               
            <form method="post">
                <label for="folio">Busqueda de expediente:</label>
                <input type="text" name="folio" id="fol" placeholder="Numero de expediente" >
                <input  class="btn-accept" type="submit" value="aceptar">
            </form>

<!--  */*/*/*/*/*/*/*/*/*/*/*/*/*/*/  datos de la tabla modo beta */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/     -->
            <div class="datosTabla">
                <hr>
                <center>
                    <caption><h2>Lista de Pacientes para generar Reportes </h2></caption>
                <table border="1">
                    
                    <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Nombre(s) </th>
                        <th> Apellido Paterno</th>
                        <th> Apellido Materno</th>
                        <th> Edad</th>
                        <th> Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>12930913</td>
                        <td>Jorge</td>  
                        <td>Luis</td>         
                        <td>Garcia</td>                   
                        <td>22</td>
    
                        <td>
                        <label for="Select"></label>
                            <input type="radio" name="gender" id="select" value="sel"><br>
                        </td>
                    </tr>
                    </tbody> 
                    
                    <tbody>
                    <tr>
                        <td>234231</td>
                        <td>Gato</td>  
                        <td>pepe</td>         
                        <td>lenny</td>                   
                        <td>15</td>
    
                        <td>
                        <label for="Select"></label>
                            <input type="radio" name="gender" id="select" value="sel"><br>
                        </td>
                    </tr>
                    </tbody> 
                 </table>
                </center>
            </div>
    </div> 

<div> 
<input type="button" style="backgroundcolor:blue;black:blue;width:150px;height:40px;" value="Siguiente" onclick="myFunction()">

    </body>
</div>
    
</body>

<script src="../../recursos/javascript/doctor.js"></script>
<script src="../../recursos/javascript/reloj.js"></script>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
  <!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">

    function ventana(idx){
        window.location.href="view_expediente.php";
    }

    $(document).ready(function() {
      $('#sidebarCollapse').on('click', function() {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
      });
    });
  </script>
</html>