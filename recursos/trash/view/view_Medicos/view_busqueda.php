<?php
session_start();

if(isset($_REQUEST['ventana'])){
    header("Location: ../Expediente/view_expediente.php");
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dcitas.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dbusqueda.css">
    <link rel=" stylesheet" href="../../../recursos/estilos/doctores/Dexpedientes.css">
    <title>Doctores</title>
</head>

<body onload="mueveReloj()">
    <noscript>
        <center>
            <h1>Habilitar javascript para una mejor experiencia</h1>
        </center>
    </noscript>

    <div id="buscador">
        <div class="cabecera">
            <h2>Doctores</h2>
        </div>
        <hr>
        <center>
            <div>
                <h3 style="color: white"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h3>
                <a href="view_doctor.php" class="btn">Citas</a>
                <a focus="true" class="btn">Busqueda</a>
            </div>
        </center>
    </div>
    </div>

    <div style="display: flex;justify-content: space-between;">
        <img src="../../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()" alt="Ocultar">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>    
    </div>

    <hr>

    <div id="Dcitas" class="modulos">
        <label for="a">Buscar por</label>
        <select name="a" id="tipos" onchange="busqueda(document.getElementsByClassName('table-responsive'))">
            <option>elija una opcion</option>
            <option value="folio">Folio</option>
            <option value="apellido">apellidos</option>
        </select>
        <hr>
        <!-- div por folio -->
        <div id="Bid" class="tras">
            <h2>Busqueda por Folio</h2>
            <div>
                <label for="folio">Folio:</label>
                <input type="text" name="folio" id="fol" placeholder="Numero de expediente">
                <button class="btn-accept" id="buscarfol">Buscar</button>
            </div>

            <div>
                <hr>
                <center>
                    <div class="table-responsive" id="resultado1" >
                        <h1 id="mensaje"></h1>

                    </div>
                </center>

            </div>
        </div>

        <!-- busqueda por apellidos -->
        <div id="BAp">
            <h2>Busqueda por Apellidos</h2>
            <div class="Datos">
                <div>
                    <label for="apellidoP">Apellido paterno</label>
                    <input type="text" name="apellidoP" id="dato1" placeholder="Numero de expediente" >
                </div>
                <div>
                    <label for="apellidoM">Apellido materno</label>
                    <input type="text" name="apellidoM" id="dato2" placeholder="Numero de expediente" >
                </div>
                <div>
                    <label for="Nombre">Nombre(s)</label>
                    <input type="text" name="Nombre" id="dato3" placeholder="Numero de expediente">
                </div>
                <button class="btn-accept" id="buscarAp">Buscar</button>
            </div>
            
            <center>
                <div class="table-responsive" id="resultado2" >
                    <h1 id="mensaje"></h1>

                </div>
            </center>
        </div>

    </div>
</body>

<script src="../../../recursos/javascript/busqueda.js"></script>
<script src="../../../recursos/javascript/reloj.js"></script>
<script src="../../../recursos/javascript/slider.js"></script>

<script type="text/javascript">

</script>

</html>