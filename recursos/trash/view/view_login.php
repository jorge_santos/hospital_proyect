<?php
session_start();

if ( isset($_SESSION['inicio']) ) {
	if($_SESSION['inicio'][2] == "Medico"){
		header("Location: app/view/view_Medicos/view_doctor.php");
	}
	if($_SESSION['inicio'][2] == "Recepcionista"){
		header("Location: app/view/view_agendas/view_agenda.php");
	}
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href= "recursos/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href= "recursos/estilos/login.css">
		<title>login</title>
	</head>

	<body>
		<noscript>
        	<center><h1>Hbilitar javascript para una mejor experiencia</h1></center>
    	</noscript>
		<div id="login">
			<h2>Inicio de sesion</h2>
			<p id="mensaje"></p>
			<div>
				<form method="POST" id="ingreso">
					<label for="usuario">Usuario</label>
					<input type="text" name="usuario" id="usuario" class="datos" placeholder="usuario"><br><br>
					<label for="contraseña">Contraseña</label>
					<input type="password" name="pass" id="contraseña" class="datos" placeholder="contraseña"><br>
				</form>

				<button id="ok" class="btn btn-accept">Ingresar</button>
				<button id="null" class="btn btn-cancel">Cancelar</button>
			</div>
		</div>
		<!-- 
		<div class="card">
			<center> 
  				<div class="card-body">
    				<h5 class="card-title">En caso de:</h5>
    					<a href="app\view\view_doctor.php" class="btn btn-primary">Doctor</a>
    					<a href="app\view\view_agenda.php" class="btn btn-primary">Recepcionista</a>
  				</div>
			</center>
		</div>
		-->

	</body>

	<script src="recursos/javascript/login.js"></script>
</html>