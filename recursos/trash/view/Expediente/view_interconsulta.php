<?php

if (isset($_REQUEST['notas'])){
    header("Location: view_notas.php ");
}
if (isset($_REQUEST['Recetas'])){
    header("Location: view_recetaMedica.php");
}
if (isset($_REQUEST['expediente'])){
    header("Location: view_expediente.php");
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <link rel="stylesheet" href="../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/Dbusqueda.css">
    <title>Interconsulta</title>
</head>

<body onload="mueveReloj()">
    <div id="buscador">
        <div class="cabecera">
            <h2>Interconsulta</h2>
        </div>
        <hr>
        <center>
            <div>
                <form method="post">
                    <p>Crear:</p>
                    <button class="btn" id="nota" name="notas" value="notas">Nota medica</button>
                    <button class="btn" id="receta" name="Recetas" value="receta">Receta medica</button>
                    <a focus="true" class="btn">Interconsulta</a>
                    <br><br><br><br><br><br><br>
                    <button class="btn" id="expediente" name="expediente" value="expediente">Volver al expediente</button>

                </form>
            </div>
        </center>
    </div>


    <div>
        <div align="center">
            <br><br>
            <label for="nombre">Nombre:_____</label>
            <label for="PA">Apellido Paterno:___</label>
            <label for="SA">Apellido materno:____</label>
            <br><br>
        </div>
        <div align="center">
            <label for="a">Especialidad</label>
            <select align="letf" name="a" id="tipos" onchange="busqueda()">
                <!--<optgroup label="tipo de busqueda">-->
                <option></option>
                <option value="especialidad1">especialidad 1</option>
                <option value="especialidad2">especialidad 2</option>
                <option value="especialidad3">especialidad 3</option>

                <!--</optgroup>-->
            </select>
            <label for="a">Doctor</label>
            <select align="letf" name="a" id="tipos" onchange="busqueda()">
                <!--<optgroup label="tipo de busqueda">-->
                <option></option>
                <option value="doctor1">paul mejia </option>
                <option value="doctor2">gerardo chale </option>
                <option value="doctor3">jorge santos </option>
                <option value="doctor3">kris</option>

                <!--</optgroup>-->
            </select>

        </div>
        <br><br>
        <div align="center">
            <p>Causas/Observaciones</p>
            <textarea name="comentarios" rows="10" cols="40"></textarea>
            <br>
            <br>
            <button class="btn-accept">guardar</button>

        </div>

    </div>
</body>
<script src="../../recursos/javascript/doctor.js"></script>
<script src="../../recursos/javascript/reloj.js"></script>


</html>