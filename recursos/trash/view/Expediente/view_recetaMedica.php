<?php

if (isset($_REQUEST['notas'])){
    header("Location: view_notas.php ");
}
if (isset($_REQUEST['Interconsulta'])){
    header("Location: view_interconsulta.php ");
}
if (isset($_REQUEST['expediente'])){
    header("Location: view_expediente.php");
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->


    <link rel="stylesheet" href="../../recursos/estilos/doctores/css.css">
    <!-- <link rel="stylesheet" href="../../recursos/bootstrap/css/bootstrap.css"> -->
    <link rel="stylesheet" href="../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../recursos/estilos/doctores/Dbusqueda.css">
    <title>Receta medica</title>

    <style>

    img{

    }

    img:hover{

    }
    #receta{
        display: flex;
        flex-direction: column;
        overflow: auto;
        padding: 0% 1% 0%;
    }

    .Datos{
        display: flex;
        overflow: auto;
        clear: left;
        justify-content: space-between;
    }

    </style>
</head>

<body onload=" mueveReloj()">

    <div id="buscador">
        <div class="cabecera">
            <h2>Receta medica</h2>
        </div>
        <hr>
        <center>
            <div>
                <form method="post">
                    <p>Crear:</p>
                    <button class="btn" id="nota" name="notas" value="notas">Nota medica</button>
                    <a focus="true" class="btn">Receta</a>
                    <button class="btn" id="interconsulta" name="Interconsulta"
                        value="interconsulta">Interconsulta</button>
                    <br><br><br><br><br><br><br>
                    <button class="btn" id="expediente" name="expediente" value="expediente">Volver al expediente</button>

                </form>
            </div>
        </center>
    </div>

    <div style="display: flex;justify-content: space-between;">
        <img src="../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>
    <hr>

    <div id="receta" class="modulos">
        <center><h3>Formato unico de receta medica</h3></center>
        <hr>

        <form name="recetaM">
            <input type="hidden" name="option" value="Receta">
            <div class="Datos_institucion">

                <label for="UM">Hospital General de Chetumal</label>

                <label for="municipio">Othon P. Blanco</label>

                <span id="fecha"><label for="servicio">Servicio: Medico</label></span>

            </div>
            <hr>

            <h4>
            <div class="Datos_paciente">

                <label for="paciente">Paciente: Fernando Lopez Peres</label>

                <label for="edad">| Edad: 32</label>

                <label for="sexo">| M</label>

                <label for="nsp">| SP:1213412313</label>

            </div>
            </h4>
            <hr>

            <!--<img src="" onclick="console.log('apretado')" alt="Agregar Medicamento">-->
            <!--<img src="" onclick="console.log('apretado')" alt="Eliminar Medicamento">-->
            <div id="medicamentos" class="Datos_medicamentos">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Medicamento (nombre generico)</th>
                            <th>Presentacion</th>
                            <th>cantidad</th>
                            <th>Indicaciones de administracion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <select name="" id="">
                                    <option value="Paracetamol">Paracetamos</option>
                                    <option value="Ambroxol">ambroxol</option>
                                </select>
                            </td>
                            <td>
                                <select name="" id="">
                                    <option value="tableta">tableta</option>
                                    <option value="jarabe">jarable</option>
                                    <option value="Crema">cutanea</option>
                                    <option value="intramuscular">inyectable</option>
                                </select>
                            </td>
                            <td><input type="text" name="cantidad" id=""></td>
                            <td><textarea name="" id="" cols="40" rows="3"></textarea></td>
                        </tr>
                        <tr>
                            <td>
                                <select name="" id="">
                                    <option value="Paracetamol">Paracetamos</option>
                                    <option value="Ambroxol">ambroxol</option>
                                </select>
                            </td>
                            <td>
                                <select name="" id="">
                                    <option value="tableta">tableta</option>
                                    <option value="jarabe">jarable</option>
                                    <option value="Crema">cutanea</option>
                                    <option value="intramuscular">inyectable</option>
                                </select>
                            </td>
                            <td><input type="text" name="cantidad" id=""></td>
                            <td><textarea name="" id="" cols="40" rows="3"></textarea></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr>

            <div class="Datos_diafnostico">
                <div class="form-group">
                    <label for="diagnostico">Diagnostico</label>
                    <textarea name="diagnostico" cols="25" rows="2" placeholder="Diagnostico"></textarea>
                    &emsp13; &emsp13; &emsp13; &emsp13; &emsp13;
                    <input class="btn-accept" type="submit" value="Guardar">
                </div>
                
            </div>
            
        </form>
    </div>

    </body>
    <!-- <script src="../../recursos/javascript/doctor.js"></script> -->
    <script src="../../recursos/javascript/reloj.js"></script>
    <script src="../../recursos/javascript/slider.js"></script>


    <script>
        function agregarMedicamento() {

        }

        function eliminarMedicamento() {

        }
    </script>

</html>