<?php
//incluye la clase Libro y CrudLibro
session_start();

if ( !isset($_SESSION['inicio']) ){
    header("Location: ../../..");
}

if (isset($_REQUEST['lobby'])){
    header("Location: ../../..");
    session_destroy();
}

require_once dirname(__DIR__).'../../model/model_agenda/model_agendaPaciente.php';
require_once dirname(__DIR__).'../../config/conexion.php';

require_once dirname(__DIR__).'../../controller/controller_ListaPaciente.php';
$crud=new CrudPaciente();
$listaPacinet= new ListaPaciente();
//obtiene todos los libros con el método mostrar de la clase crud
$FechaCita = isset($_POST['FechaCita']) ? $_POST['FechaCita'] : '';
$IdEspecialidad = isset($_POST['IdEspecialidad']) ? $_POST['IdEspecialidad'] : '';
$IdEmpleado = isset($_POST['IdEmpleado']) ? $_POST['IdEmpleado'] : '';	

	if (!(empty($FechaCita)) && !(empty($IdEspecialidad)) && !(empty($IdEmpleado))){

        $listaPaciente=$crud->mostrarFiltro($FechaCita,$IdEspecialidad,$IdEmpleado);
        
	}else{

        $listaPaciente=$crud->mostrar();
       
    }


?>

<!DOCTYPE html>
<html lang="en">

<head>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../../recursos/estilos/css.css">

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <title>Archivo Clinico</title>
</head>

<body onload="mueveReloj()">
    <header>
        <div>
            <h2>Archivo Clìnico</h2>
        </div>
    </header>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Archivo Clìnico </h3>
                </h3>
            </div>

            <ul class="list-unstyled components">

                <li class="active">
                    <a focus="true" class="btn">Agenda General</a>
                    <a href="view_agendarcita.php" class="btn">Agendar Cita</a>
                    <a href="view_crearexpediente.php" class="btn">Crear Expediente</a>
                    <br><br><br><br><br><br><br>
                    <form method="post">
                        <button class="btn" id="lobby" name="lobby" value="lobby">Cerrar sesion</button>
                    </form>

                </li>

            </ul>


        </nav>

    </div>
     <div style="display: flex;justify-content: space-between;">
        <img src="../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()" alt="Ocultar">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>

    <!--- parte del cuerpo-->
    <div id="content">
        <div class="container ">
            <div>
                <div class="card ">
                    
                    <div class="card-body align-middle">
                    <div class="table-active font-weight-bold text-uppercase ">
                        <h5>Busqueda de citas</h5>
                    </div>
                    <form action='view_agenda.php' method='post'>
                        <table class="table">

                            <tr>
                                <th>
                                    <div class="md-form">
                                   
                                        <label>Fecha</label> <br>
                                        <input name="FechaCita" class="form-control" type="date" value="<?php echo date('Y-m-d')?>"/>

                                    
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                        <label>Especialidad</label>
                                        <select class="form-control" id="exampleFormControlSelect2" name="IdEspecialidad">
                                        <?php
                                                	$db=Db::conectar();
                                                  
                                                    $select=$db->query('select * from Especialidad');
                                                    
                                                    $rows = $select->fetchAll(PDO::FETCH_ASSOC);

                                                foreach ($rows as $row)
                                                 echo "<option value='".$row["IdEspecialidad"]."'>" . $row["Especialidad"]."</option>";
                                                         
                                                              
                                                             ?>
                                        </select>
                                    </div>
                                </th>
                               
                                <th>
                                    <div class="form-group">
                                        <label>Medico</label>
                                        <select name='IdEmpleado' class="form-control" id="exampleFormControlSelect1">
                                        <?php
                                                	
                                                  
                                                    $select=$db->query('select * from Empleado');
                                                    
                                                    $rows = $select->fetchAll(PDO::FETCH_ASSOC);

                                                foreach ($rows as $row)
                                                 echo "<option value='".$row["IdEmpleado"]."'>". $row["NombreEmpleado"]." ".$row["PrimerApellidoE"]." ".$row["SegundoApellidoE"]."</option>";
                                                         
                                                              
                                                             ?>
                                        </select>
                                    </div>
                                </th>
                                <th>
                                    &emsp13; &emsp13; &emsp13;<br>
                                    <button name="submit" type="submit" class="btn btn-danger active"> <i class="fas fa-search"></i> BUSCAR</button>
                                </th>
                            </tr>

                        </table>
                        </form>


                    </div>
                </div>

            </div>


            <div class="table-responsive">
                <br>
                <caption>
                    <h5>Lista de Pacientes</h5>
                </caption>
                <table class="table table-striped">

                    <tr >
                        <th>Nº Expediente</th>
                        <th>Nombre</th>
                        <th>Doctor</th>
                        <th>Especialidad</th>
                        <th>Turno</th>
                        <th>Acciones</th>
                        
                    </tr>

                    <?php foreach ($listaPaciente as $listaPacinet) {?>
		        	<tr>
				        <td><?php echo $listaPacinet->getFolio() ?></td>
                        <td><?php echo $listaPacinet->getNombrePaciente()." ". $listaPacinet->getApellidoPaternoPaciente()." ".$listaPacinet->getApellidoMaternoPaciente();
                            ?></td>
				        <td><?php echo $listaPacinet->getNombreDoctor()." ". $listaPacinet->getApellidoPaternoDoctor()." ".$listaPacinet->getApellidoMaternoDoctor(); ?></td>
				        <td><?php echo $listaPacinet->getEspecialidad() ?></td>
				        <td><?php echo $listaPacinet->getTurno() ?></td>
				        <td><a class="btn btn-primary" href="model/model_agendaPaciente.php?cveart=<?php echo $listaPacinet->getFolio()?>&accion=a"> <i class="fas fa-user-edit"></i> Actualizar</a> 
				        <a class="btn btn-danger far fa-trash-alt" href="../../controller/controller_ListaPaciente.php?IdCita=<?php echo $listaPacinet->getId()?>"> Eliminar</a>   </td>
			  
			         </tr>
			        <?php }?>

                        
                </table>


            </div>
        </div>

</body>
<script src="../../recursos/javascript/reloj.js"></script>
<script src="../../recursos/javascript/slider.js"></script>
<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- jQuery Custom Scroller CDN -->
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {


        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>

</html>