<?php
//incluye la clase Libro y CrudLibro
require_once dirname(__DIR__).'../../model/model_agenda/model_agendaPaciente.php';
require_once dirname(__DIR__).'../../config/conexion.php';

require_once dirname(__DIR__).'../../controller/controller_ListaPaciente.php';
$crud=new CrudPaciente();
$agendar= new ListaPaciente();
//obtiene todos los libros con el método mostrar de la clase crud

 $IdPersona = isset($_GET['IdPersona']) ? $_GET['IdPersona'] : '';
	

	if ( !(empty($IdPersona)) ){
	    $agendar=$crud->PacienteAgendar($IdPersona);
	}else{

        header('Location: /proyecto/app/view/view_agenda/view_agendarcita.php');
    }
	


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../../recursos/estilos/css.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <title>Archivo Clinico</title>
</head>

<body>
    <header>
        <div>
            <h2>Archivo Clìnico</h2>
        </div>
    </header>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Archivo Clìnico </h3>
                </h3>
            </div>

            <ul class="list-unstyled components">

                <li class="active">
                    <a href="view_agenda.php" class="btn">Agenda General</a>
                    <a href="view_agendarcita.php" class="btn">Agendar Cita</a>
                    <a href="view_crearexpediente.php" class="btn">Crear Expediente</a>

                </li>

            </ul>


        </nav>

    </div>

    <!--- parte del cuerpo-->
    <div id="content">
    <div class="Datos_personales">
            <div class="card" >
             <div class="card-body">
            <form action='../../model/model_agenda/NuevaCita.php' method='post'>
             <h5 class="card-title">Cita Medica de <?php echo $agendar->getNombrePaciente()." ". $agendar->getApellidoPaternoPaciente()." ".$agendar->getApellidoMaternoPaciente();?></h5>
             
            
            
   
                         
                         <input type="hidden" name="idPaciente" value="<?php echo $agendar->getFolio() ?>">
                         
    </div>
  
</div>
				       
				        
			


                    <label for="Consultorio">Consultorio</label>
                    <select class="form-control" name="Consultorio" >
                    <?php
                    $db=Db::conectar();
                    $select=$db->query('select * from consultorio');           
                    $rows = $select->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($rows as $row)
                    echo "<option value='".$row["IdConsultorio"]."'>". $row["Ubicacion"]."</option>";
                    ?>
                    </select>
                    <div>
                    <label for="Empleado">Medico</label>
                    <select class="form-control" name="Empleado" >
                    <?php
                    $db=Db::conectar();
                    $select=$db->query('select * from empleado');           
                    $rows = $select->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($rows as $row)
                    echo "<option value='".$row["IdEmpleado"]."'>". $row["NombreEmpleado"]." ".$row["PrimerApellidoE"]." ".$row["SegundoApellidoE"]."</option>";
                    ?>
                    </select>
                    </div>
                    <label>Fecha</label>
                    <input name="Fecha" class="form-control" type="date" required>
                    <label>Hora</label>
                    <input  class="form-control" type="time" id="appt" name="tiempo" min="9:00" max="18:00" required>
                    <button name="submit" type="submit" class="btn btn-danger active">Actualizar</button>
                    
              </div>
    </div>





</body>
<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- jQuery Custom Scroller CDN -->
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {


        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>

</html>