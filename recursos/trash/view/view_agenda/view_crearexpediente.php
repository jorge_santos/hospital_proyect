<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../../recursos/estilos/css.css">

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <title>Archivo Clinico</title>
</head>

<body onload="mueveReloj()">
    <header>
        <div>
            <h2>Archivo Clinico</h2>
        </div>
    </header>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Archivo Clinico </h3>
                </h3>
            </div>

            <ul class="list-unstyled components">

                <li class="active">
                    <a href="view_agenda.php" class="btn">Agenda General</a>
                    <a href="view_agendarcita.php" class="btn">Agendar Cita</a>
                    <a focus="true" class="btn">Crear Expediente</a>
                     <br><br><br><br><br><br><br>
                    <form method="post">
                        <button class="btn" id="lobby" name="lobby" value="lobby">Cerrar sesion</button>
                    </form>
                </li>

            </ul>


        </nav>

    </div>
         <div style="display: flex;justify-content: space-between;">
        <img src="../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()" alt="Ocultar">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>

    <!--- parte del cuerpo-->
    <div id="content">
        <div class="Datos_personales">
            <div class="card" >
             <div class="card-body">
    <form action='../../model/model_agenda/RegistroPacientes.php' method='post'>
             <h5 class="card-title">Datos Personales</h5>
            <label for="curp">Curp:</label><input class="form-control" type="text" name="curp" placeholder="Curp" required>
            <label for="sp">Seguro Papular:</label><input class="form-control" type="text" name="seguro" placeholder="Seguro Popular" >
            <label for="nombre">Nombre:</label><input class="form-control" type="text" name="nombre" placeholder="Nombre(s)" required>
            <label for="PA">Apellido Paterno:</label><input class="form-control" type="text" name="PA" placeholder="Primer apellido" required>
            <label for="SA">Apellido materno:</label><input class="form-control" type="text" name="SA" placeholder="Segundo apellido" required>
            <label for="fn">Fecha de nacimiento:</label><input class="form-control" type="date" name="fn" placeholder="Fecha de nacimiento" required>
            <label for="tel">Telefono:</label><input class="form-control" type="number" name="tel"  placeholder="Ingresa su Numero telefonico" required>
     
            <label for="sexo">Sexo</label>
                    <select class="form-control" name="sexo" required>
                        <option value="M">M</option>
                        <option value="F">F</option>
                    </select>
                    <label for="rh">Tipo de sangre:</label><input class="form-control" type="text" name="rh" placeholder="Tipo de sangre">

              </div>
        <hr>
    </div>
</div>
        <div class="Datos_domicilio">
            <div class="card" >
                 <div class="card-body">
                  <h5 class="card-title">Datos Domicilio</h5>
                  <label for="calle">Municipio:</label><input class="form-control" type="text" name="municipio" placeholder="Calle" required>
                  <label for="calle">Ciudad:</label><input class="form-control" type="text" name="ciudad" placeholder="Ciudad" required>
                  <label for="colonia">Colonia:</label><input class="form-control" type="text" name="colonia" placeholder="colonia" required>
                 <label for="calle">Calle:</label><input class="form-control" type="text" name="calle" placeholder="Calle" required>
                 
                 <label for="lt">Lote:</label><input required class="form-control" type="number" name="lt" placeholder="Lote" pattern="[0-9]*">
                 <label for="mza">Manzana:</label><input required class="form-control" type="number" name="mza" placeholder="Manzana" pattern="[0-9]*">
          
                 
                 <label for="CP">Codigo postal:</label><input class="form-control" type="text" name="CP" placeholder="Codigo postal"
                pattern="[0-9]*" required>
        </div>
        <hr>
</div>
<button  name="submit" type="submit" class="btn btn-danger active"> <i class="fas fa-user-plus"></i> guardar</button>
</form>
</div>
        <div class="Datos_anexos">
            <!-- <form name="formulario" method="post" action="http://pagina.com/send.php" enctype="multipart/form-data">
                ¡No olvides el enctype! -->
                <!-- Campo de selección de archivo -->
               <!--  <label for="identifiacion">Identificacion oficial:</label>
                <input type="file" name="adjunto" accept=".pdf,.jpg,.png" multiple>
            </form>

            <form name="formulario" method="post" action="http://pagina.com/send.php" enctype="multipart/form-data">
                 ¡No olvides el enctype! -->
                <!-- Campo de selección de archivo -->
                <!-- <label for="socioeconomico">Estudio socio-economico:</label>
                <input type="file" name="adjunto" accept=".pdf,.jpg,.png" multiple>
            </form> -->

        </div>

        
    </div>


</body>
<script src="../../recursos/javascript/reloj.js"></script>
<script src="../../recursos/javascript/slider.js"></script>
<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- jQuery Custom Scroller CDN -->
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {


        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });


    
    
</script>

</html>