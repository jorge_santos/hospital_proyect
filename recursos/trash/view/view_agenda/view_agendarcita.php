
<?php

require_once dirname(__DIR__).'../../config/conexion.php';

require_once dirname(__DIR__).'../../controller/controller_ListaPaciente.php';
$crud=new CrudPaciente();
$Pacientes= new ListaPaciente();
//obtiene todos los libros con el método mostrar de la clase crud

$filtroEstado = isset($_POST['filtroEstado']) ? $_POST['filtroEstado'] : '';	
$idPaciente= isset($_POST['folio']) ? $_POST['folio'] : '';
if (empty($filtroEstado)){

    $NuevoPaciente=$crud->mostrarPaciente();
}
if($filtroEstado=="Folio"){
    $NuevoPaciente=$crud->PacienteFiltroFolio($idPaciente);
    if ($NuevoPaciente == NULL) {
        echo "<script>alert('No se encuentra registrado')</script>";
      }
}
if($filtroEstado=="Nombre"){
    $NuevoPaciente=$crud->PacienteFiltroFolio($idPaciente);
    if ($NuevoPaciente == NULL) {
        echo "<script>alert('No se encuentra registrado')</script>";
      };
}
// if (!(empty($FechaCita)) && !(empty($IdEspecialidad)) && !(empty($IdEmpleado))){

//     $listaPaciente=$crud->mostrarFiltro($FechaCita,$IdEspecialidad,$IdEmpleado);
    
// }else{

//     $listaPaciente=$crud->mostrar();
   
// }




?>
<!DOCTYPE html>
<html lang="en">

<head>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../../recursos/estilos/css.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        
    <title>Archivo Clinico</title>
</head>

<body onload="showUser(0)">
    <header>
        <div>
            <h2>Archivo Clinico</h2>
        </div>
    </header>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Archivo Clinico </h3>
                </h3>
            </div>

            <ul class="list-unstyled components">

                <li class="active">
                    <a href="view_agenda.php" class="btn">Agenda General</a>
                    <a focus="true" class="btn">Agendar Cita</a>
                    <a href="view_crearexpediente.php" class="btn">Crear Expediente</a>
                    <br><br><br><br><br><br><br>
                    <form method="post">
                        <button class="btn" id="lobby" name="lobby" value="lobby">Cerrar sesion</button>
                    </form>
                </li>

            </ul>


        </nav>

    </div>
     <div style="display: flex;justify-content: space-between;">
        <img src="../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()" alt="Ocultar">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>
    <!--- parte del cuerpo-->
    <div id="content">
        <div class="container">
            <div class="datos_busqueda">
              
                        <h5 class="card-title">Busqueda por:</h5>
                            
                            <select name="users" onchange="showUser(this.value)">
                               <option>Select:</option>
                                <option value="1">Nombre:</option>
                                <option value="2">Nº Expediente:</option>
                            </select>

                                <div id="porFolio">
                                <form action="view_agendarcita.php" method='post'>
                                     <div class="form-group">
                                         <label class="sr-only"for="folio">Folio</label>
                                         <input type="hidden" name="filtroEstado" value="Folio">
                                         <input required type="number" name="folio" id="fol" class="form-control" placeholder="No. Expediente">
                                         <button name="submit" type="submit" class="btn btn-danger active"><i class="fas fa-search"></i> BUSCAR</button>
                                     </div>
                                </form>
                                </div>
                                <div id="porNombre">
                                <form action="view_agendarcita.php" method='post'>
                                     <div class="form-group">
                                     <input type="hidden" name="filtroEstado" value="Nombre">
                                        <label class="sr-only" for="nombre">Nombre</label>
                                        <input required type="text" name="nombre" id="nombre" class="form-control" placeholder="nombre(s)">
                                        <label class="sr-only" for="papellido">Primer apellido</label>
                                        <input required type="text" name="papellido" id="papellido" class="form-control" placeholder="primer apellido">
                                        <label class="sr-only" for="sapellido">Segundo apellido</label>
                                        <input required type="text" name="sapellido" id="sapellido" class="form-control" placeholder="Segundo apellido">
                                        <button name="submit" type="submit" class="btn btn-danger active"> <i class="fas fa-search"></i> BUSCAR</button>
                                     </div>
                                </form>
                                </div>
                        
                 </div>
            <!-- Button to Open the Modal -->



                

            <div class="table-responsive">
                <br>
                <caption>
                    <h5>Lista de Pacientes</h5>
                </caption>
                <table class="table table-striped">

                    <tr>
                        <th>Nº Expediente</th>
                        <th>Nombre</th>
                        <th>Fecha de Nacimiento</th>
                        <th>Seguro popular</th>
                        <th></th>
                    </tr>

                    
                    <?php foreach ($NuevoPaciente as $Pacientes) {?>
                       
                        <tr>
				        <td><?php echo $Pacientes->getFolio() ?></td>
                        <td><?php echo $Pacientes->getNombrePaciente()." ". $Pacientes->getApellidoPaternoPaciente()." ".$Pacientes->getApellidoMaternoPaciente();
                            ?></td>
				       
				        <td><?php echo $Pacientes->getFechaNaciPaci() ?></td>
				        <td><?php echo $Pacientes->getSeguroPopuPacienete() ?></td>
				       
				        <td><a class="btn btn-danger fas fa-address-book" href="view_nuevocita.php?IdPersona=<?php echo $Pacientes->getFolio()?>"> Agregar</a>   </td>
			  
			        </tr>
			        <?php }?>
                    
                </table>


            </div>
        </div>
    </div>




</body>
<!-- jQuery CDN -->
<script src="../../recursos/javascript/reloj.js"></script>
<script src="../../recursos/javascript/slider.js"></script>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- jQuery Custom Scroller CDN -->
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {


        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
    function showUser(str) {
        if (str==0) {
            document.getElementById("porNombre").style.visibility = "hidden";
            document.getElementById("porFolio").style.visibility = "hidden";
}
if (str==1) {
 document.getElementById("porNombre").style.visibility = 'visible';
 document.getElementById("porFolio").style.visibility = "hidden";
}
if (str==2) {
  document.getElementById("porFolio").style.visibility = "visible";
  document.getElementById("porNombre").style.visibility = 'hidden';
}
}


</script>

</html>