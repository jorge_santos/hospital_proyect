<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Receta medica</title>
	<link rel="stylesheet" href="receta.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
	<div id="receta">
		<h3>Formato unico de receta medica</h3>
		<div id="cuerpo">
			<form name="recetaMedica">

				<div id="informacionH" class="form-row">
					<div class="col-4 division">
						<label for="Unidad">Unidad Medica</label>
						<input type="text" name="Unidad" id="" class="form-control">
					</div>

					<div class="col-2 division">
						<label for="municipio">Municipio</label>
						<input type="text" name="Municipio" id="" class="form-control">
					</div>

					<div class="col-2 division">
						<label for="Fecha">Fecha</label>
						<input type="text" name="fecha" id="" class="form-control" disabled>
					</div>

					<div class="col division">
						<label for="servicio">servicio</label>
						<input type="text" name="servicio" id="" class="form-control">
						<!--<input type="date" name="" id="">-->
					</div>
				</div><hr>

				<div id="informacionP" class="form-row">
					<div class="col-4 division">
						<label for="paciente">Nombre del paciente</label>
						<input type="text" name="paciente" id="" class="form-control">
					</div>

					<div class="col-2 division">
						<label for="Fnacimiento">Fecha de nacimiento</label>
						<input type="text" name="Fnacimiento" id="" class="form-control">
					</div>

					<div class="col-2 division">
						<label for="edad">Edad</label>
						<input type="text" name="edad" id="" class="form-control" disabled>
					</div>

					<!-- radiobutton -->
					<div>
						<center><p>sexo</p></center>
						<div class="form-check form-check-inline">
							<label class="form-check-label" for="mujer">F</label>
  							<input class="form-check-input" type="radio" name="sexo" id="mujer" value="Mujer">
						</div>
				
						<div class="form-check form-check-inline">
  							<label class="form-check-label" for="hombre">H</label>
  							<input class="form-check-input" type="radio" name="sexo" id="hombre" value="Hombre">
						</div>
					</div>
				
					<!-- radiobutton -->

					<div class="col division">
						<label for="sp">No. afiliacion seguro popular</label>
						<input type="text" name="sp" id="" class="form-control">
					</div>
				</div><hr>

				<div id="informacionM" class="form-row">
					<div class="col-4 division">
						<label for="medico">Nombre del medico</label>
						<input type="text" name="medico" id="" class="form-control">
					</div>

					<div class="col-4 division">
						<label for="cedula">Cedula profesional</label>
						<input type="text" name="cedula" id="" class="form-control">
					</div>

					<div class="col division">
						<label for="universidad">Universidad</label>
						<input type="text" name="universidad" id="" class="form-control">
					</div>
				</div><hr>

				<div id="medicamentos">
					<table class="table">
						<thead>
							<tr>
								<th>Medicamento (nombre generico)</th>
								<th>Presentacion</th>
								<th>cantidad</th>
								<th>Indicaciones de administracion</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div><hr>

				<div id="Diagnostico">
					<div class="firmas">
						<textarea name="" id="" cols="30" rows="10" placeholder="Diagnostico" style="float: left;"></textarea>

						<div id ="firmaM"><p>Firma del medico</p></div>
	
						<input type="text" name="observaciones" id="" placeholder="Observaciones/Recomendaciones generales" class="form-control">
					</div>

					<div class="firmas doctor"><p>Firma o huella del quien recibe la receta</p></div>

					<div class="firmas doctor"><p>Sello de la unidad de medica</p></div>
				</div>
			</form>
		</div>
	</div><br>
</body>

<script>
	// j = JSON.stringify(recetaMedica.elements); <--convierte datos en formato json
</script>
</html>