-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2019 at 06:22 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `cargo`
--

CREATE TABLE `cargo` (
  `IdCargo` int(11) NOT NULL,
  `Cargo` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `cargo`
--

INSERT INTO `cargo` (`IdCargo`, `Cargo`) VALUES
(1, 'Medico'),
(2, 'Recepcionista');

-- --------------------------------------------------------

--
-- Table structure for table `certificadodefuncion`
--

CREATE TABLE `certificadodefuncion` (
  `IdDefuncion` int(11) NOT NULL,
  `IdPaciente` int(11) DEFAULT NULL,
  `IdEmpleado` int(11) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Documento` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `certificadom`
--

CREATE TABLE `certificadom` (
  `IdcerM` int(11) NOT NULL,
  `IdEmpleado` int(11) NOT NULL,
  `IDPaciente` int(11) NOT NULL,
  `FechaActual` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certificadom`
--

INSERT INTO `certificadom` (`IdcerM`, `IdEmpleado`, `IDPaciente`, `FechaActual`) VALUES
(5, 2, 1, '2019-06-04'),
(6, 2, 1, '2019-06-04'),
(7, 1, 3, '2019-06-04'),
(8, 4, 1, '2019-06-04'),
(9, 4, 1, '2019-06-04'),
(10, 4, 1, '2019-06-04'),
(16, 1, 1, '2019-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `certificados`
--

CREATE TABLE `certificados` (
  `IdCertificado` int(11) NOT NULL,
  `IdPaciente` int(11) DEFAULT NULL,
  `IdEmpleado` int(11) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Edad` int(11) DEFAULT NULL,
  `TipoCertificado` varchar(15) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `certificados`
--

INSERT INTO `certificados` (`IdCertificado`, `IdPaciente`, `IdEmpleado`, `Fecha`, `Edad`, `TipoCertificado`) VALUES
(1, 1, 1, '2019-05-29 00:00:00', 21, 'Medico'),
(2, 2, 2, '2019-05-29 00:00:00', 14, 'prenupcial'),
(3, 3, 1, '2019-05-30 00:00:00', 33, 'medico');

-- --------------------------------------------------------

--
-- Table structure for table `citamedica`
--

CREATE TABLE `citamedica` (
  `IdCita` int(11) NOT NULL,
  `IdPaciente` int(11) DEFAULT NULL,
  `IdConsultorio` int(11) DEFAULT NULL,
  `IdEmpleado` int(11) DEFAULT NULL,
  `FechaCita` date DEFAULT NULL,
  `HoraInicio` time DEFAULT NULL,
  `HoraTerminacion` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `citamedica`
--

INSERT INTO `citamedica` (`IdCita`, `IdPaciente`, `IdConsultorio`, `IdEmpleado`, `FechaCita`, `HoraInicio`, `HoraTerminacion`) VALUES
(2, 2, 2, 2, '2019-05-07', '00:00:24', NULL),
(3, 3, 1, 2, '2019-05-07', '25:00:00', '00:00:08'),
(4, 1, 2, 2, '2019-05-25', '14:22:00', NULL),
(18, 6, 2, 1, '2019-05-27', '03:15:00', NULL),
(19, 4, 2, 4, '2019-05-30', '15:06:00', NULL),
(20, 5, 1, 2, '2019-05-30', '12:48:00', NULL),
(21, 6, 2, 2, '2019-05-29', '12:42:00', NULL),
(22, 1, 1, 1, '2018-05-09', '15:21:00', NULL),
(23, 7, 2, 4, '2019-05-23', '14:23:00', NULL),
(24, 4, 2, 2, '2019-06-05', '12:34:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `citamedicarestfull`
--

CREATE TABLE `citamedicarestfull` (
  `id` int(12) NOT NULL,
  `NombreDoctor` varchar(50) NOT NULL,
  `NombrePaciente` varchar(50) NOT NULL,
  `Fecha` varchar(50) NOT NULL,
  `HoraInicio` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citamedicarestfull`
--

INSERT INTO `citamedicarestfull` (`id`, `NombreDoctor`, `NombrePaciente`, `Fecha`, `HoraInicio`) VALUES
(1, 'Data is incomplete.', 'Data is incomplete.', 'Data is incomplete.', 'Data is incomplete.'),
(2, 'jorge itza', 'jorge itza', '12/02/1996', '23:15');

-- --------------------------------------------------------

--
-- Table structure for table `constanciam`
--

CREATE TABLE `constanciam` (
  `IdConsM` int(11) NOT NULL,
  `IdEmpleado` int(11) NOT NULL,
  `IdPaciente` int(11) NOT NULL,
  `FechaActual` date NOT NULL,
  `ConsultaM` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `constanciam`
--

INSERT INTO `constanciam` (`IdConsM`, `IdEmpleado`, `IdPaciente`, `FechaActual`, `ConsultaM`) VALUES
(1, 4, 1, '2019-06-04', 'SE LLEVA CONTROL EN CONSULTA EXTERNA DE PEDIATRIA CON DIAGNOSTICO DESDE EL 2015 DE HEPATOPATIA CRONICA (REFIERE PACIENTE QUE POSIBLEMENTE DE TIOLOGIA VIRAL). Y TROMBOCITOPENIA'),
(2, 1, 1, '2019-06-12', 'TRABAJADORA LA CUAL ACUDE A SU TURNO EL DÍA 17/01/2018 PRESENTANDO DOLOR LUMBAR EL CUAL LIMITA LOS MOVIMIENTOS DE LAS PIERNAS, SE LE REALIZA VALORACIÓN MEDICA EN DONDE SE OBTIENE EL DIAGNOSTICO DE LUMBALGIA POSTESFUERZOSE ENCUENTRA INGRESADO EN ESTA UNIDAD HOSPITALARIA A CARGO DEL ÁREA DE CIRUGÍA EN LA CAMA 218, DESDE EL DÍA 05/02/2018, POR LIPOMA EN EL BRAZO IZQUIERDO, '),
(3, 1, 1, '2019-06-12', 'INGRESO EL DIA 05/02/2018 A LAS 9:08 HRS. AL ÁREA DE URGENCIAS POR EL DIAGNOSTICO DE TRAUMATISMO ABDOMINAL, POLICONTUNDIDO SECUNDARIO A SUFRIR CAÍDA EN UN ANDAMIO, DURANTE SU ESTANCIA SE REALIZA MANEJO Y CONTROL, PACIENTE CLÍNICAMENTE CON MEJORÍA');

-- --------------------------------------------------------

--
-- Table structure for table `consultorio`
--

CREATE TABLE `consultorio` (
  `IdConsultorio` int(11) NOT NULL,
  `Ubicacion` varchar(15) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `consultorio`
--

INSERT INTO `consultorio` (`IdConsultorio`, `Ubicacion`) VALUES
(1, 'CONSULTORIO 1'),
(2, 'CONSULTORIO 2');

-- --------------------------------------------------------

--
-- Table structure for table `contacto`
--

CREATE TABLE `contacto` (
  `IdTelefono` int(11) NOT NULL,
  `Telefono` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `TipoTelefono` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `contacto`
--

INSERT INTO `contacto` (`IdTelefono`, `Telefono`, `TipoTelefono`) VALUES
(1, '9831052818', 'celular'),
(2, '9983536436', 'celular');

-- --------------------------------------------------------

--
-- Table structure for table `empleado`
--

CREATE TABLE `empleado` (
  `IdEmpleado` int(11) NOT NULL,
  `NombreEmpleado` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `PrimerApellidoE` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `SegundoApellidoE` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `FechaNacimiento` date NOT NULL,
  `Cedula` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `RFC` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `Colonia` varchar(40) COLLATE latin1_spanish_ci NOT NULL,
  `Ciudad` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `CP` int(6) DEFAULT NULL,
  `Pais` varchar(25) COLLATE latin1_spanish_ci DEFAULT NULL,
  `IdCargo` int(11) DEFAULT NULL,
  `IdEspecialidad` int(11) DEFAULT NULL,
  `IdHorario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `empleado`
--

INSERT INTO `empleado` (`IdEmpleado`, `NombreEmpleado`, `PrimerApellidoE`, `SegundoApellidoE`, `FechaNacimiento`, `Cedula`, `RFC`, `Colonia`, `Ciudad`, `CP`, `Pais`, `IdCargo`, `IdEspecialidad`, `IdHorario`) VALUES
(1, 'MANUEL', 'ALBARADO', 'MENDEZ', '2019-05-07', 'AECEM-24988', 'POIU78593010', 'PACTO OBRERO', 'CHETUMAL', 909, 'MÉXICO', 1, 1, 1),
(2, 'MIGUEL', 'JIMENEZ', 'GONZALEZ', '2019-05-06', '3279425', 'IIPT789031587', 'CARIBE', 'CHETUMAL', 99, 'MÉXICO', 1, 2, 2),
(4, 'CARLOS', 'SANTANA', 'GUITERREZ', '2019-05-01', 'AE-013468', 'CUPU800825569', 'LAGUNITAS', 'CHETUMAL', 12, 'MÉXICO', 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `especialidad`
--

CREATE TABLE `especialidad` (
  `IdEspecialidad` int(11) NOT NULL,
  `Especialidad` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `especialidad`
--

INSERT INTO `especialidad` (`IdEspecialidad`, `Especialidad`) VALUES
(1, 'CIRUJANO'),
(2, 'CARDIOLOGO');

-- --------------------------------------------------------

--
-- Table structure for table `expedienteclinico`
--

CREATE TABLE `expedienteclinico` (
  `Folio` int(11) DEFAULT NULL,
  `IdNota` int(11) DEFAULT NULL,
  `IdCertificado` int(11) DEFAULT NULL,
  `IdResumen` int(11) DEFAULT NULL,
  `IdDefuncion` int(11) DEFAULT NULL,
  `idPaciente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `expedienteclinico`
--

INSERT INTO `expedienteclinico` (`Folio`, `IdNota`, `IdCertificado`, `IdResumen`, `IdDefuncion`, `idPaciente`) VALUES
(NULL, NULL, 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `horario`
--

CREATE TABLE `horario` (
  `IdHorario` int(11) NOT NULL,
  `Turno` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Horario` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `horario`
--

INSERT INTO `horario` (`IdHorario`, `Turno`, `Horario`) VALUES
(1, 'MATUTINO', '23:16:00'),
(2, 'VESPERTINO', '00:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `interconsulta`
--

CREATE TABLE `interconsulta` (
  `IdInterconsulta` int(11) NOT NULL,
  `IdEmpleado` int(11) DEFAULT NULL,
  `IdPaciente` int(11) DEFAULT NULL,
  `Fecha` date NOT NULL,
  `Observaciones` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `FechaAgenda` date DEFAULT NULL,
  `Estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `interconsulta`
--

INSERT INTO `interconsulta` (`IdInterconsulta`, `IdEmpleado`, `IdPaciente`, `Fecha`, `Observaciones`, `FechaAgenda`, `Estado`) VALUES
(1, 1, 1, '2019-05-01', 'TRANSLADO AL AREA DE PEDIATRA', '0000-00-00', 0),
(2, 1, 3, '2019-05-14', 'TRANSLADO AL AREA DE CIRUJIA', '0000-00-00', 0),
(3, 4, 1, '2019-05-29', 'TRANFERIDO AL AREA DE URGENCIAS', '0000-00-00', 0),
(4, 2, NULL, '2019-05-29', 'CIRUJIA PROGRAMADA', '0000-00-00', 0),
(5, 2, NULL, '2019-05-29', 'TODO BIEN', '0000-00-00', 0),
(6, NULL, NULL, '2019-05-29', NULL, '0000-00-00', 0),
(7, NULL, NULL, '2019-05-29', NULL, '0000-00-00', 0),
(8, 4, NULL, '2019-05-29', 'TRANSFERIR AL AREA DE LIBERAMIENTO', '0000-00-00', 0),
(9, 1, NULL, '2019-05-29', 'TRANSLADO AL AREA DE ATENCION MEDICA', '0000-00-00', 0),
(10, 1, NULL, '2019-05-29', 'CUENTA CON SEGURO POPULAR', '0000-00-00', 0),
(11, NULL, NULL, '2019-05-29', NULL, '0000-00-00', 0),
(12, 1, NULL, '2019-05-29', 'NOTA AL MEDICO QUE NECEISTA ATENCION INMEDIATA', '0000-00-00', 0),
(13, 2, NULL, '2019-05-29', 'PARA TRATAMIENTO DE MENOR RANGO', '0000-00-00', 0),
(14, 2, 6, '2019-05-29', 'PARA TRATAMIENTO MEDICO', '0000-00-00', 0),
(15, 1, 1, '2019-06-05', 'PARA LIBERAR EN UNA HORA', '0000-00-00', 0),
(16, 4, 1, '2019-06-10', 'NECESITA SUERO', '0000-00-00', 0),
(17, 2, 1, '2019-06-11', 'AREA DE EMERGENCIA', '2019-06-11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `medicamentos`
--

CREATE TABLE `medicamentos` (
  `IdMedicamento` int(11) NOT NULL,
  `NombreGenerico` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Formula` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Presentacion` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `gramaje` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `medicamentos`
--

INSERT INTO `medicamentos` (`IdMedicamento`, `NombreGenerico`, `Formula`, `Presentacion`, `gramaje`) VALUES
(1, 'paracetamos', 'AMBROXOL', 'CAPSULAS', 500),
(2, 'Abacavir', 'ZIAGEN', 'CAPSULAS', 250),
(3, 'Alosetrón', 'LOTRONEX', 'JARABE', 500),
(4, 'Cefixima', 'SUPRAX', 'Tabletas ', 500),
(5, 'Cloxacilina', 'CLOXAPEN', 'Pomadas', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notamedica`
--

CREATE TABLE `notamedica` (
  `IdNota` int(11) NOT NULL,
  `IdPaciente` int(11) DEFAULT NULL,
  `IdEmpleado` int(11) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Frecuencia_Cardiaca` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `Frecuencia_Respiratoria` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `Tension_Arterial` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `Temperatura` double NOT NULL,
  `Talla` double NOT NULL,
  `Peso` double NOT NULL,
  `Glucosa` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `Consulta` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `notamedica`
--

INSERT INTO `notamedica` (`IdNota`, `IdPaciente`, `IdEmpleado`, `Fecha`, `Frecuencia_Cardiaca`, `Frecuencia_Respiratoria`, `Tension_Arterial`, `Temperatura`, `Talla`, `Peso`, `Glucosa`, `Consulta`) VALUES
(1, 1, 2, '2019-05-08 00:00:00', '72', '53', '56', 75, 32, 65, '25', 'EN EXCELENTE ESTADO'),
(2, 2, 1, '2019-05-08 00:00:00', '78', '58', '58', 90, 30, 70, '52', 'PERFECTO'),
(3, 1, 2, '2019-05-16 00:00:00', '25', '25', '65', 55, 65, 56, '5', 'ATENCION INMEDIATA'),
(4, 1, 1, '2019-05-24 00:00:00', '41', '85', '71', 25, 52, 22, '25', 'ATENDER INMEDIATO'),
(5, 3, 2, '2019-05-15 00:00:00', '47', '47', '47', 47, 44, 14, '4', 'DEBIL Y NECESITA ATENCION\r\n'),
(6, 2, 2, '2019-05-21 16:17:22', '74', '85', '47', 87, 47, 85, '74', 'ESTA BIEN'),
(7, 3, 2, '2019-05-21 16:18:56', '74', '57', '57', 85, 25, 82, '22', 'MAS O MENOS, NECESITA TRATAMIENTO'),
(8, 6, 1, '2019-05-27 07:37:38', '15/120', '35', '78', 35, 60, 78, '120', 'paciente con cefalea y otras sintomatologÃ­as '),
(9, 4, 2, '2019-05-27 22:13:48', '47', '4', '148', 18, 15, 15, '15', 'SUFRE DE HAMBRE'),
(10, 4, 2, '2019-06-05 16:01:12', '1', '1', '1', 1, 1, 1, '1', 'nota medica');

-- --------------------------------------------------------

--
-- Table structure for table `paciente`
--

CREATE TABLE `paciente` (
  `IdPaciente` int(11) NOT NULL,
  `NombrePaciente` varchar(35) COLLATE latin1_spanish_ci NOT NULL,
  `PrimerApallidoP` varchar(35) COLLATE latin1_spanish_ci NOT NULL,
  `SegundoApellidoP` varchar(35) COLLATE latin1_spanish_ci DEFAULT NULL,
  `FechaNacimiento` date NOT NULL,
  `Curp` varchar(18) COLLATE latin1_spanish_ci NOT NULL,
  `Genero` char(1) COLLATE latin1_spanish_ci NOT NULL,
  `Calle` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `Municipio` varchar(40) COLLATE latin1_spanish_ci NOT NULL,
  `Colonia` varchar(40) COLLATE latin1_spanish_ci NOT NULL,
  `Ciudad` varchar(40) COLLATE latin1_spanish_ci NOT NULL,
  `NumExt` int(11) NOT NULL,
  `NumInt` int(11) DEFAULT NULL,
  `CP` int(6) DEFAULT NULL,
  `TipoSangre` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `SeguroPopular` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `FechaAfiliacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `paciente`
--

INSERT INTO `paciente` (`IdPaciente`, `NombrePaciente`, `PrimerApallidoP`, `SegundoApellidoP`, `FechaNacimiento`, `Curp`, `Genero`, `Calle`, `Municipio`, `Colonia`, `Ciudad`, `NumExt`, `NumInt`, `CP`, `TipoSangre`, `SeguroPopular`, `FechaAfiliacion`, `Activo`) VALUES
(1, 'ROSA', 'GUADALUPE', 'BARILLAS', '1997-05-06', 'HEVL910224HCH', 'H', 'LAGUNA BACALAR', 'QUINTANA ROO', 'PACTO OBRERO', 'CHETUMAL', 22, 121, 131, 'O-', '15397895', '2019-06-13 15:48:24', 1),
(2, 'KRISTOPHER', 'ORTIZ', 'HERRERA', '1998-05-14', 'OIVA860210HCH', 'H', 'LAGUNA SAN FELIPE', 'QUINTANA ROO', 'AMERICAS 3', 'CHETUMAL', 12, 21, 21, 'O-', '15392015', '2019-06-13 16:21:27', 1),
(3, 'FERNANDO', 'LOPEZ', 'PEREZ', '1997-05-08', 'BAVC840614HCHR', 'M', 'ISLA CANCUN', 'QUINTANA ROO', 'PAYO OBISPO', 'CHETUMAL', 33, 22, 22, 'A+', '15362501', '2019-06-13 16:21:47', 1),
(4, 'JORGE', 'SANTOS', 'ITZA', '1997-05-19', 'AECJ940429HCHRRS01', 'M', 'LAGUNA NEGRA', 'QUINTANA ROO', 'LAGUNITAS', 'CHETUMAL', 45, 0, 77029, 'O+', '15390286', '2019-06-13 15:47:44', 1),
(5, 'JESUS', 'CHALE', 'CUXIM', '1997-05-19', 'GAAH860926HCHRR', 'M', 'LAGUNA NEGRA', 'QUINTANA ROO', 'LAGUNITAS', 'CHETUMAL', 45, 0, 77029, 'O+', '15390286', '2019-06-13 15:47:50', 1),
(6, 'PAUL', 'MEJIA', 'CHIMAL', '1997-10-14', 'VIRF890629HSL', 'H', 'CALLE CHETUMAL', 'QUINTANA ROO', 'MIRA FLORES', 'CHETUMAL', 78, 1, 77045, 'A+', '15390287', '2019-06-13 15:48:09', 1),
(7, 'FELIPE', 'PEÑA', 'GUZMAN', '1997-05-19', 'PAVE910731HC', 'M', 'ANDRES QUINTANA ROO', 'QUINTANA ROO', 'FRACC. CARIBE', 'CHETUMAL', 1, 1, 77029, 'A-', '17896502', '2019-06-13 15:48:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pacientexcontacto`
--

CREATE TABLE `pacientexcontacto` (
  `IdPaciente` int(11) DEFAULT NULL,
  `IdTelefono` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `pacientexcontacto`
--

INSERT INTO `pacientexcontacto` (`IdPaciente`, `IdTelefono`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `recetamedica`
--

CREATE TABLE `recetamedica` (
  `Folio` int(11) NOT NULL,
  `IdPaciente` int(11) DEFAULT NULL,
  `IdEmpleado` int(11) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Edad` int(11) DEFAULT NULL,
  `Diagnostico_1` text COLLATE latin1_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `recetamedica`
--

INSERT INTO `recetamedica` (`Folio`, `IdPaciente`, `IdEmpleado`, `Fecha`, `Edad`, `Diagnostico_1`) VALUES
(3, 2, 1, '2019-05-28 00:12:16', 0, 'GRIPA'),
(4, 2, 1, '2019-05-28 00:12:16', 0, 'TOS'),
(5, 2, 1, '2019-05-28 00:12:16', 0, 'NARIZ CONGESTIONADA'),
(6, 2, 1, '2019-05-28 00:31:58', 0, 'NARIZ CONGESTIONADA'),
(7, 2, 1, '2019-05-28 00:37:42', 0, 'PROBLEMA DEL CORAZON'),
(8, 2, 1, '2019-05-28 00:39:01', 0, 'NARIZ CONGESTIONADA'),
(9, 5, 1, '2019-05-28 00:47:06', 22, 'GRIPA'),
(10, 4, 2, '2019-05-27 22:30:21', 22, 'DOLOR EN EL ABDOMEN'),
(11, 1, 1, '2019-06-05 01:30:40', 22, 'DOLOR DE CABEZ'),
(12, 1, 2, '2019-06-05 16:05:25', 22, 'NARIZ CONGESTIONADA');

-- --------------------------------------------------------

--
-- Table structure for table `recetaxmedicamento`
--

CREATE TABLE `recetaxmedicamento` (
  `Folio` int(11) DEFAULT NULL,
  `IdMedicamento` int(11) DEFAULT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  `Indicaciones` text COLLATE latin1_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `recetaxmedicamento`
--

INSERT INTO `recetaxmedicamento` (`Folio`, `IdMedicamento`, `Cantidad`, `Indicaciones`) VALUES
(7, 1, 1, '2'),
(7, 1, 3, '4'),
(8, 1, 500, '1 al dia'),
(8, 1, 600, '6 dias x 1 semana'),
(8, 1, 700, 'regular'),
(8, 1, 800, '1 al dia'),
(9, 1, 500, '1 al dia'),
(9, 1, 500, '2 al dia'),
(10, 1, 500, '2 al dia'),
(10, 1, 500, '1 al dia por 14 dias'),
(11, 1, 500, '1'),
(11, 1, 600, '2'),
(11, 1, 700, '4'),
(12, 1, 500, '4 AL DIA'),
(12, 1, 500, 'POR UNA SEMANA');

-- --------------------------------------------------------

--
-- Table structure for table `resumenc`
--

CREATE TABLE `resumenc` (
  `IdResC` int(11) NOT NULL,
  `IdEmpleado` int(11) NOT NULL,
  `IdPaciente` int(11) NOT NULL,
  `FechaActual` date NOT NULL,
  `ConsultaC` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `resumenc`
--

INSERT INTO `resumenc` (`IdResC`, `IdEmpleado`, `IdPaciente`, `FechaActual`, `ConsultaC`) VALUES
(2, 4, 1, '2019-06-04', 'QUIEN  ACUDE A CONSULTA EXTERNA EN EL ÁREA DE URGENCIAS EL DÍA 01/02/2018, POR REFERIR MAREO, NAUSEAS, ACOMPAÑADO DE SINTOMATOLOGÍA FEBRIL'),
(3, 1, 1, '2019-06-12', ', QUIEN  CUENTA CON LOS DIAGNÓSTICOS DE DIABETES MELLITUS, PIE DIABÉTICO (ARTROSIS DE AMBAS RODILLAS), '),
(4, 1, 1, '2019-06-12', 'CON  FRACTURA EXPUESTA DE ROTULA Y EXCORIACIONES EN AMBOS CARPOS, SECUNDARIO POR PRESENTAR ACCIDENTE DE TRANSITO EL DÍA 18/01/2018 SIENDO INGRESADO EN LA UNIDAD DE FELIPE CARRILLO PUERTO POSTERIORMENTE TRASLADADO A ESTA UNIDAD HOSPITALARIA,');

-- --------------------------------------------------------

--
-- Table structure for table `resumenclinico`
--

CREATE TABLE `resumenclinico` (
  `IdResumen` int(11) NOT NULL,
  `IdPaciente` int(11) DEFAULT NULL,
  `IdEmpleado` int(11) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Nota` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `resumenclinico`
--

INSERT INTO `resumenclinico` (`IdResumen`, `IdPaciente`, `IdEmpleado`, `Fecha`, `Nota`) VALUES
(1, 3, 2, '2019-05-29 00:00:00', 'EN EXCELENTE ESTADO');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `IdEmpleado` int(11) DEFAULT NULL,
  `Usuario` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Contrasena` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`IdEmpleado`, `Usuario`, `Contrasena`) VALUES
(1, 'admin', '$2y$10$qWOWf6ScVjg1L3qO3DDQTuqZJ.deKdpPJVK.p43dRvx2OgdrCBukW'),
(NULL, 'jesus', '$2y$10$FLd1EbDNu9DETjvj8C5FlOxGh8.nz4yUlbFZrELhvbn5uNNaBoV..'),
(2, 'jose', '$2y$10$cBpMDZQfion0pi2z4xrZBurp6Pp5WmKyIO/cOV9EwtN39YCQ3SYO.'),
(4, '1', '$2y$10$ayySQQqes0Rx.SjIzrognOuUt2GfPjZxQT.ACt9oTQlYod.oM4h7m');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`IdCargo`);

--
-- Indexes for table `certificadodefuncion`
--
ALTER TABLE `certificadodefuncion`
  ADD PRIMARY KEY (`IdDefuncion`),
  ADD KEY `IdPaciente` (`IdPaciente`),
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- Indexes for table `certificadom`
--
ALTER TABLE `certificadom`
  ADD PRIMARY KEY (`IdcerM`),
  ADD KEY `fk_EmpCer` (`IdEmpleado`),
  ADD KEY `fk_PacCer` (`IDPaciente`);

--
-- Indexes for table `certificados`
--
ALTER TABLE `certificados`
  ADD PRIMARY KEY (`IdCertificado`),
  ADD KEY `IdPaciente` (`IdPaciente`),
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- Indexes for table `citamedica`
--
ALTER TABLE `citamedica`
  ADD PRIMARY KEY (`IdCita`),
  ADD KEY `IdPaciente` (`IdPaciente`),
  ADD KEY `IdConsultorio` (`IdConsultorio`),
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- Indexes for table `citamedicarestfull`
--
ALTER TABLE `citamedicarestfull`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `constanciam`
--
ALTER TABLE `constanciam`
  ADD PRIMARY KEY (`IdConsM`),
  ADD KEY `fk_EmpCons` (`IdEmpleado`),
  ADD KEY `fk_PacCons` (`IdPaciente`);

--
-- Indexes for table `consultorio`
--
ALTER TABLE `consultorio`
  ADD PRIMARY KEY (`IdConsultorio`);

--
-- Indexes for table `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`IdTelefono`);

--
-- Indexes for table `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`IdEmpleado`),
  ADD KEY `IdCargo` (`IdCargo`),
  ADD KEY `IdEspecialidad` (`IdEspecialidad`),
  ADD KEY `IdHorario` (`IdHorario`);

--
-- Indexes for table `especialidad`
--
ALTER TABLE `especialidad`
  ADD PRIMARY KEY (`IdEspecialidad`);

--
-- Indexes for table `expedienteclinico`
--
ALTER TABLE `expedienteclinico`
  ADD KEY `Folio` (`Folio`),
  ADD KEY `IdNota` (`IdNota`),
  ADD KEY `IdCertificado` (`IdCertificado`),
  ADD KEY `IdResumen` (`IdResumen`),
  ADD KEY `IdDefuncion` (`IdDefuncion`),
  ADD KEY `idPaciente` (`idPaciente`);

--
-- Indexes for table `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`IdHorario`);

--
-- Indexes for table `interconsulta`
--
ALTER TABLE `interconsulta`
  ADD PRIMARY KEY (`IdInterconsulta`),
  ADD KEY `IdEmpleado` (`IdEmpleado`),
  ADD KEY `IdPaciente` (`IdPaciente`);

--
-- Indexes for table `medicamentos`
--
ALTER TABLE `medicamentos`
  ADD PRIMARY KEY (`IdMedicamento`);

--
-- Indexes for table `notamedica`
--
ALTER TABLE `notamedica`
  ADD PRIMARY KEY (`IdNota`),
  ADD KEY `IdPaciente` (`IdPaciente`),
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- Indexes for table `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`IdPaciente`);

--
-- Indexes for table `pacientexcontacto`
--
ALTER TABLE `pacientexcontacto`
  ADD KEY `IdPaciente` (`IdPaciente`),
  ADD KEY `IdTelefono` (`IdTelefono`);

--
-- Indexes for table `recetamedica`
--
ALTER TABLE `recetamedica`
  ADD PRIMARY KEY (`Folio`),
  ADD KEY `IdPaciente` (`IdPaciente`),
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- Indexes for table `recetaxmedicamento`
--
ALTER TABLE `recetaxmedicamento`
  ADD KEY `Folio` (`Folio`),
  ADD KEY `IdMedicamento` (`IdMedicamento`);

--
-- Indexes for table `resumenc`
--
ALTER TABLE `resumenc`
  ADD PRIMARY KEY (`IdResC`),
  ADD KEY `fk_EmpRes` (`IdEmpleado`),
  ADD KEY `fk_PacRes` (`IdPaciente`);

--
-- Indexes for table `resumenclinico`
--
ALTER TABLE `resumenclinico`
  ADD PRIMARY KEY (`IdResumen`),
  ADD KEY `IdPaciente` (`IdPaciente`),
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cargo`
--
ALTER TABLE `cargo`
  MODIFY `IdCargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `certificadodefuncion`
--
ALTER TABLE `certificadodefuncion`
  MODIFY `IdDefuncion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `certificadom`
--
ALTER TABLE `certificadom`
  MODIFY `IdcerM` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `certificados`
--
ALTER TABLE `certificados`
  MODIFY `IdCertificado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `citamedica`
--
ALTER TABLE `citamedica`
  MODIFY `IdCita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `citamedicarestfull`
--
ALTER TABLE `citamedicarestfull`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `constanciam`
--
ALTER TABLE `constanciam`
  MODIFY `IdConsM` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contacto`
--
ALTER TABLE `contacto`
  MODIFY `IdTelefono` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `empleado`
--
ALTER TABLE `empleado`
  MODIFY `IdEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `especialidad`
--
ALTER TABLE `especialidad`
  MODIFY `IdEspecialidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `horario`
--
ALTER TABLE `horario`
  MODIFY `IdHorario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `interconsulta`
--
ALTER TABLE `interconsulta`
  MODIFY `IdInterconsulta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `medicamentos`
--
ALTER TABLE `medicamentos`
  MODIFY `IdMedicamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `notamedica`
--
ALTER TABLE `notamedica`
  MODIFY `IdNota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `paciente`
--
ALTER TABLE `paciente`
  MODIFY `IdPaciente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `recetamedica`
--
ALTER TABLE `recetamedica`
  MODIFY `Folio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `resumenc`
--
ALTER TABLE `resumenc`
  MODIFY `IdResC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `resumenclinico`
--
ALTER TABLE `resumenclinico`
  MODIFY `IdResumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `certificadodefuncion`
--
ALTER TABLE `certificadodefuncion`
  ADD CONSTRAINT `CertificadoDefuncion_ibfk_1` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `CertificadoDefuncion_ibfk_2` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`);

--
-- Constraints for table `certificadom`
--
ALTER TABLE `certificadom`
  ADD CONSTRAINT `fk_EmpCer` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`),
  ADD CONSTRAINT `fk_PacCer` FOREIGN KEY (`IDPaciente`) REFERENCES `paciente` (`IdPaciente`);

--
-- Constraints for table `certificados`
--
ALTER TABLE `certificados`
  ADD CONSTRAINT `Certificados_ibfk_1` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `Certificados_ibfk_2` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`);

--
-- Constraints for table `citamedica`
--
ALTER TABLE `citamedica`
  ADD CONSTRAINT `CitaMedica_ibfk_1` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `CitaMedica_ibfk_2` FOREIGN KEY (`IdConsultorio`) REFERENCES `consultorio` (`IdConsultorio`),
  ADD CONSTRAINT `CitaMedica_ibfk_3` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`);

--
-- Constraints for table `constanciam`
--
ALTER TABLE `constanciam`
  ADD CONSTRAINT `fk_EmpCons` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`),
  ADD CONSTRAINT `fk_PacCons` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`);

--
-- Constraints for table `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `Empleado_ibfk_1` FOREIGN KEY (`IdCargo`) REFERENCES `cargo` (`IdCargo`),
  ADD CONSTRAINT `Empleado_ibfk_2` FOREIGN KEY (`IdEspecialidad`) REFERENCES `especialidad` (`IdEspecialidad`),
  ADD CONSTRAINT `Empleado_ibfk_3` FOREIGN KEY (`IdHorario`) REFERENCES `horario` (`IdHorario`);

--
-- Constraints for table `expedienteclinico`
--
ALTER TABLE `expedienteclinico`
  ADD CONSTRAINT `ExpedienteClinico_ibfk_1` FOREIGN KEY (`Folio`) REFERENCES `recetamedica` (`Folio`),
  ADD CONSTRAINT `ExpedienteClinico_ibfk_2` FOREIGN KEY (`IdNota`) REFERENCES `notamedica` (`IdNota`),
  ADD CONSTRAINT `ExpedienteClinico_ibfk_3` FOREIGN KEY (`IdCertificado`) REFERENCES `certificados` (`IdCertificado`),
  ADD CONSTRAINT `ExpedienteClinico_ibfk_4` FOREIGN KEY (`IdResumen`) REFERENCES `resumenclinico` (`IdResumen`),
  ADD CONSTRAINT `ExpedienteClinico_ibfk_5` FOREIGN KEY (`IdDefuncion`) REFERENCES `certificadodefuncion` (`IdDefuncion`),
  ADD CONSTRAINT `ExpedienteClinico_ibfk_6` FOREIGN KEY (`idPaciente`) REFERENCES `paciente` (`IdPaciente`);

--
-- Constraints for table `interconsulta`
--
ALTER TABLE `interconsulta`
  ADD CONSTRAINT `Interconsulta_ibfk_1` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`),
  ADD CONSTRAINT `Interconsulta_ibfk_2` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`);

--
-- Constraints for table `notamedica`
--
ALTER TABLE `notamedica`
  ADD CONSTRAINT `NotaMedica_ibfk_1` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `NotaMedica_ibfk_2` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`);

--
-- Constraints for table `pacientexcontacto`
--
ALTER TABLE `pacientexcontacto`
  ADD CONSTRAINT `PacienteXContacto_ibfk_1` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `PacienteXContacto_ibfk_2` FOREIGN KEY (`IdTelefono`) REFERENCES `contacto` (`IdTelefono`);

--
-- Constraints for table `recetamedica`
--
ALTER TABLE `recetamedica`
  ADD CONSTRAINT `RecetaMedica_ibfk_2` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `RecetaMedica_ibfk_3` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`);

--
-- Constraints for table `recetaxmedicamento`
--
ALTER TABLE `recetaxmedicamento`
  ADD CONSTRAINT `RecetaXMedicamento_ibfk_1` FOREIGN KEY (`Folio`) REFERENCES `recetamedica` (`Folio`),
  ADD CONSTRAINT `RecetaXMedicamento_ibfk_2` FOREIGN KEY (`IdMedicamento`) REFERENCES `medicamentos` (`IdMedicamento`);

--
-- Constraints for table `resumenc`
--
ALTER TABLE `resumenc`
  ADD CONSTRAINT `fk_EmpRes` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`),
  ADD CONSTRAINT `fk_PacRes` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`);

--
-- Constraints for table `resumenclinico`
--
ALTER TABLE `resumenclinico`
  ADD CONSTRAINT `ResumenClinico_ibfk_1` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `ResumenClinico_ibfk_2` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`);

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
