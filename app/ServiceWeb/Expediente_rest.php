<?php
require_once dirname(__DIR__)."/model/model_expedientes.php";

class controller_historial{
	private $model;

	function __construct($idExpediente = null){
		$this->model = new model_expediente();
	}

	private function Receta($id, $filtro){
		$resultado = $this->model->HojasMedicas($id,'recetamedica', $filtro);
		if (!empty($resultado)){
			return $resultado;
		}
	}

	private function Nota($id, $filtro){
		$resultado = $this->model->HojasMedicas($id,'notamedica', $filtro);
		if (!empty($resultado)){
			return $resultado;
		}
	}

	/*private function certificado($id, $filtro){
		$resultado = $this->model->HojasMedicas($id,'certificados', $filtro);
		if (!empty($resultado)){
			return $resultado;
		}
	}

	private function Resumen($id, $filtro){
		$resultado = $this->model->HojasMedicas($id,'resumenclinico', $filtro);
		if (!empty($resultado)){
			return $resultado;
		}
	}*/

	private function Interconsulta($id, $filtro){
		$resultado = $this->model->HojasMedicas($id,'interconsulta', $filtro);
		if (!empty($resultado)){
			return $resultado;
		}
	}
	//----------------------------------------------------------------------------------
	private function certificado($id, $filtro){
		$resultado = $this->model->reportesMedicas($id,'certificadom', $filtro);
		if (!empty($resultado)){
			return $resultado;
		}
	}
	private function constancia($id, $filtro){
		$resultado = $this->model->reportesMedicas($id,'constanciam', $filtro);
		if (!empty($resultado)){
			return $resultado;
		}
	}
	private function Resumen($id, $filtro){
		$resultado = $this->model->reportesMedicas($id,'resumenc', $filtro);
		if (!empty($resultado)){
			return $resultado;
		}
	}
	//----------------------------------------------------------------------------------

	public function VerExpediente($id, $fecha){
		$filtro = '%%';

		if ($fecha != ''){
			$filtro = '%'.$fecha.'%';
		}
		/*
		$Datos = array( $this->Receta($id, $filtro), $this->Nota($id, $filtro), $this->certificado($id, $filtro), $this->Resumen($id, $filtro), $this->Interconsulta($id, $filtro));
		$LLaves = array("Recetas Medicas", "Notas Medicas", "Certificados" ,"Resumenes", "Interconsultas" );*/


		$Datos = array( $this->Receta($id, $filtro), $this->Nota($id, $filtro), $this->certificado($id, $filtro), $this->constancia($id, $filtro), $this->Resumen($id, $filtro), $this->Interconsulta($id, $filtro));
		
		$LLaves = array("Recetas Medicas", "Notas Medicas", "Certificados", "Constancias" ,"Resumenes", "Interconsultas" );
		$expediente = array_combine($LLaves, $Datos);

		$rest = json_encode($expediente);
		echo $rest;	
	}
}

//$id = isset($_GET['id']) ? $_GET['id']: '';
$c = new controller_historial();
$id = isset($_POST['id']) ? $_POST['id']: '';
$fecha = isset($_POST['fecha']) ? $_POST['fecha']: '';

header("Content-Type: application/json; charset=UTF-8");
$c->VerExpediente($id, $fecha);

?>