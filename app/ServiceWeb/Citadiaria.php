<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
// include database and object files
include_once 'database.php';
include_once 'hospital.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$hospital = new Hospital($db);
$hospital->Fecha= isset($_GET['Fecha']) ? $_GET['Fecha'] : die();
$stmt = $hospital->citaDiaria();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){

    $products_arr=array();
    $products_arr["Cita"]=array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);
 
        $product_item=array(
            
            "NombrePaciente" => $NombrePaciente,
            "PrimerApallidoP" => $PrimerApallidoP,
            "SegundoApellidoP" => $SegundoApellidoP,
            "FechaNacimiento" => $FechaNacimiento
        );
 
        array_push($products_arr["Cita"], $product_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data in json format
    echo json_encode($products_arr);
}

else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user product does not exist
    echo json_encode(array("message" => "No existe Cita hoy!!"));
}
?>