<?php
    //require_once dirname(__DIR__).'/config/conexion.php';
    require_once 'modelo.php';

    $idE = isset($_POST['empleado']) ? $_POST['empleado']: '';
    $usuario = isset($_POST['Usuario']) ? $_POST['Usuario']: '';
    $passw = isset($_POST['pass']) ? $_POST['pass']: '';

    if (!empty($idE) && !empty($usuario)){
        Controller_addUser::AddUser($idE, $usuario, $passw);
    }else{
        echo "estoy vacio";
    }

	class Controller_addUser{

        private static function EnvioPasswordCifrado($contrasena){
            $sha = hash('sha256',$contrasena,false);
            $cifrado = password_hash($sha, PASSWORD_DEFAULT);
            return $cifrado;
        }

        public static function AddUser($empleado, $usuario, $Password){

        	$password = self::EnvioPasswordCifrado($Password);
            Model_addUser::adduser($empleado, $usuario, $password );
        }

	}
?>