<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once 'database.php';
 
// instantiate product object
include_once 'hospital.php';
 
$database = new Database();
$db = $database->getConnection();
 
$hospital = new Hospital($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// make sure data is not empty
if(
    !empty($data->NombrePaciente) &&
    !empty($data->NombreDoctor) &&
    !empty($data->Fecha) &&
    !empty($data->HoraInicio)
){
 
    // set product property values
    $hospital->NombrePaciente = $data->NombrePaciente;
    $hospital->NombreDoctor = $data->NombreDoctor;
    $hospital->Fecha = $data->Fecha;
    $hospital->HoraInicio = $data->HoraInicio;
   
 
    // create the product
    if($hospital->nuevo()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "Se anexo nueva Cita."));
    }
 
    // if unable to create the product, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Error al ingresar los datos de la citas"));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Data is incomplete."));
}
?>