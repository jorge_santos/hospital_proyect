<?php
class Hospital{
 
    // database connection and table name
    private $conn;
    private $table_name = "citamedica";
 
    // object properties
    public $id;
    //public $NombrePaciente;
    public $NombreDoctor;
    public $Fecha;
    public $HoraInicio;

    //certificado medico
    public $NombreEmpleado;
    public $PrimerApellidoE;
    public $SegundoApellidoE;
    public $NombrePaciente;
    public $PrimerApallidoP;
    public $SegundoApellidoP;
    public $TipoSangre;
    public $Genero;
    
    public $FechaNacimiento;
    public $Cedula;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    function readCitadia(){
 
        // query to read single record
        $query = "SELECT paciente.IdPaciente,paciente.NombrePaciente,paciente.PrimerApallidoP,paciente.SegundoApellidoP,paciente.FechaNacimiento FROM citamedica 
        INNER JOIN empleado ON citamedica.IdEmpleado=empleado.IdEmpleado 
        INNER JOIN paciente on paciente.IdPaciente=citamedica.IdPaciente 
        WHERE citamedica.FechaCita=
        WHERE paciente.IdPaciente = :id";
     
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(":id", $this->id);
        // bind id of product to be updated
        //$stmt->bindParam(1, $this->id);
       
        // execute query
        $stmt->execute();
      
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     var_dump( $this->NombreEmpleado);
        // set values to object properties
        $this->NombrePaciente = $row['NombrePaciente'];
        $this->PrimerApallidoP = $row['PrimerApallidoP'];

        $this->SegundoApellidoP = $row['SegundoApellidoP'];
        $this->TipoSangre = $row['TipoSangre'];
        $this->Genero = $row['Genero'];
        $this->FechaNacimiento = $row['FechaNacimiento'];
        $this->Cedula = $row['Cedula'];
    }
    function citaDiaria(){
 
        // select all query
        $query = "SELECT paciente.IdPaciente,paciente.NombrePaciente,paciente.PrimerApallidoP,paciente.SegundoApellidoP,paciente.FechaNacimiento FROM citamedica INNER JOIN empleado ON citamedica.IdEmpleado=empleado.IdEmpleado INNER JOIN paciente on paciente.IdPaciente=citamedica.IdPaciente
        WHERE citamedica.FechaCita=:Fecha
        ";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":Fecha", $this->Fecha);
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
    function readCertificado(){
 
        // query to read single record
        $query = "SELECT empleado.NombreEmpleado,empleado.PrimerApellidoE,empleado.SegundoApellidoE,empleado.Cedula,paciente.NombrePaciente,paciente.PrimerApallidoP,paciente.SegundoApellidoP,paciente.FechaNacimiento,paciente.TipoSangre,paciente.Genero
        FROM certificados
        INNER JOIN empleado
        ON certificados.IdEmpleado=empleado.IdEmpleado
        INNER JOIN paciente
        on paciente.IdPaciente=certificados.IdPaciente
                WHERE
                paciente.IdPaciente = :id";
     
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(":id", $this->id);
        // bind id of product to be updated
        //$stmt->bindParam(1, $this->id);
       
        // execute query
        $stmt->execute();
      
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     var_dump( $this->NombreEmpleado);
        // set values to object properties
        $this->NombreEmpleado = $row['NombreEmpleado'];
        $this->PrimerApellidoE = $row['PrimerApellidoE'];
        $this->SegundoApellidoE = $row['SegundoApellidoE'];
        $this->NombrePaciente = $row['NombrePaciente'];
        $this->PrimerApallidoP = $row['PrimerApallidoP'];

        $this->SegundoApellidoP = $row['SegundoApellidoP'];
        $this->TipoSangre = $row['TipoSangre'];
        $this->Genero = $row['Genero'];
        $this->FechaNacimiento = $row['FechaNacimiento'];
        $this->Cedula = $row['Cedula'];
    }

    function nuevo(){
 
        // query to insert record
        $query = "INSERT INTO citamedicarestfull SET NombrePaciente=:NombrePaciente, NombreDoctor=:NombreDoctor, Fecha=:Fecha, HoraInicio=:HoraInicio";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->NombrePaciente=htmlspecialchars(strip_tags($this->NombrePaciente));
        $this->NombreDoctor=htmlspecialchars(strip_tags($this->NombreDoctor));
        $this->Fecha=htmlspecialchars(strip_tags($this->Fecha));
        $this->HoraInicio=htmlspecialchars(strip_tags($this->HoraInicio));
       
     
        // bind values
        $stmt->bindParam(":NombrePaciente", $this->NombrePaciente);
        $stmt->bindParam(":NombreDoctor", $this->NombreDoctor);
        $stmt->bindParam(":Fecha", $this->Fecha);
        $stmt->bindParam(":HoraInicio", $this->HoraInicio);
      
     
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }
}