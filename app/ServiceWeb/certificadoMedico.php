<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once 'database.php';
include_once 'hospital.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$hospital = new Hospital($db);
 
// set ID property of record to read
$hospital->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of product to be edited
$hospital->readCertificado();

if($hospital->NombreEmpleado!=null){
    // create array
    $product_arr = array(
        "id" =>  $hospital->id,
        "NombreEmpleado" => $hospital->NombreEmpleado,
        "PrimerApellidoE" => $hospital->PrimerApellidoE,
        "SegundoApellidoE" => $hospital->SegundoApellidoE,
        "NombrePaciente" => $hospital->NombrePaciente,
        "PrimerApallidoP" => $hospital->PrimerApallidoP,

        "SegundoApellidoP" => $hospital->SegundoApellidoP,
        "TipoSangre" => $hospital->TipoSangre,
        "Genero" => $hospital->Genero,
        "FechaNacimiento" => $hospital->FechaNacimiento,
        "Cedula" => $hospital->Cedula
    );
 
    // set response code - 200 OK
    http_response_code(200);
 
    // make it json format
    echo json_encode($product_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user product does not exist
    echo json_encode(array("message" => "No existe Paciente"));
}
?>