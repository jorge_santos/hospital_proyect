<?php
include 'conexion3.php';
require_once 'model_pas.php';


class pacientes{

    private $pdo;
    public $IdPaciente;
    public $NombrePaciente;
    public $PrimerApallidoP;
    public $SegundoApellidoP;
    public function __CONSTRUCT(){
        try{
            $this->pdo = Database::StartUp();
        }catch(Exception $e){
            die($e->getMessage());
        }
    }
    public function listar($id){
        try{
            $this->pdo = Database::StartUp();
        }catch(Exception $e){
            die($e->getMessage());
        }
        try{
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM paciente WHERE IdPaciente = ?");
            $stm->execute(array($id));
            
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }

}
class especialidad{
	private $pdo;
	public $IdEspecialidad;
	public $Especialidad;
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }
    public function listar(){
        try{
            $this->pdo = Database::StartUp();
        }catch(Exception $e){
            die($e->getMessage());
        }
        try{
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM especialidad ");
            $stm->execute();
            
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }
}
class doctor{
    private $pdo;
    public $IdEmpleado;
    public $NombreEmpleado;
    public $PrimerApellidoE;
    public $idEspecialidad;
    public function getEspecialidad()
    {
        return $this->idEspecialidad;
    } 
    public function setEspecialidad($idEspecialidad){
        $this->idEspecialidad = $idEspecialidad;
     }

    public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }
   
    public function listar($especialidad2){
        try{
            $this->pdo = Database::StartUp();
        }catch(Exception $e){
            die($e->getMessage());
        }
        try{
            $result = array();
            $stm = $this->pdo->prepare("SELECT empleado.IdEmpleado, empleado.NombreEmpleado, empleado.PrimerApellidoE, empleado.SegundoApellidoE, especialidad.IdEspecialidad, especialidad.Especialidad FROM empleado,especialidad WHERE empleado.IdEspecialidad = especialidad.IdEspecialidad AND especialidad.IdEspecialidad = ?  ");
            $stm->execute(array($especialidad2));
            
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch(Exception $e){
            die($e->getMessage());
        }
    }
}
class interconsulta
{
    private $IdInterconsulta;
    private $IdEmpleado;
    private $IdPaciente;
    private $Fecha;
    private $Observaciones;
    function __CONSTRUCT(){}

    public function getIdInterconsulta()
    {
        return $this->IdInterconsulta;
    } 
    public function setIdInterconsulta($IdInterconsulta){
        $this->IdInterconsulta = $IdInterconsulta;
     }
    public function getIdEmpleado(){
        return $this->IdEmpleado;
    }
    public function setIdEmpleado($IdEmpleado){
       $this->IdEmpleado = $IdEmpleado;
    }

    public function getIdPaciente(){
        return $this->IdEmpleado;
    }
    public function setIdPaciente($IdPaciente){
       $this->IdPaciente = $IdPaciente;
    }

    public function getFecha(){
        return $this->Fecha;
    }
    public function setFecha($Fecha){
       $this->Fecha = $Fecha;
    }

    public function getObservaciones(){
        return $this->Observaciones;
    }
    public function setObservaciones($Observaciones){
       $this->Observaciones = $Observaciones;
    }


}
class crud{
    public function __construct(){
        try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }
    
        public function insertar($paciente){
            try{
                $this->pdo = Database::StartUp();
            }catch(Exception $e){
                die($e->getMessage());
            }
			
			$insert=$db->prepare('INSERT INTO interconsulta values(NULL,:IdInterconsulta,:IdEmpleado,:IdPaciente,:Fecha,:Observaciones)');
			$insert->bindValue('IdInterconsulta',$paciente->getIdInterconsulta());
			$insert->bindValue('IdEmpleado',$paciente->getIdEmpleado());
            $insert->bindValue('IdPaciente',$paciente->getIdPaciente());
            $insert->bindValue('Fecha',$paciente->getFecha());
			$insert->bindValue('Observaciones',$paciente->getObservaciones());
		
			$insert->execute();

		}
}
?>