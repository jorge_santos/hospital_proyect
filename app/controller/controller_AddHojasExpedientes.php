<?php
	require_once "controller_expediente.php";


	$option = isset($_POST['option']) ? $_POST['option'] : '';

	class addHojas{

		public static function addNotas($Nota){ 
		
			controller_expediente::Nota($Nota);
			echo '<script>alert("Nota guardada correctamente"); window.location.href = "../view/Expediente/view_notas.php";</script>';

		}

		public static function addReceta($Paciente, $Empleado, $Fecha, $Edad, $Diagnostico, $Medicamento, $Presentacion, $Cantidad, $Indicaciones){
			echo "<meta http-equiv='Refresh' content='5;url=http: ../index.php'>";
			echo "<h1>Nueva receta medica agregada al expediente</h1>";
			controller_expediente::Receta($Paciente, $Empleado, $Fecha, $Edad, $Diagnostico, $Medicamento, $Presentacion, $Cantidad, $Indicaciones);
		}

		public static function addCerM($CerM){ 

			$option= $CerM[3];

			echo "<form formaction=\"../view/Expediente/pdf.php\" method=\"POST\">";
			echo "<INPUT TYPE=\"HIDDEN\" ID=\"option\" NAME=\"option\" VALUE=\"$option\">";
 			echo ' <button method="POST" type="submit" name="pdf_report" id="pdf_report" formaction="../view/Expediente/pdf.php" class=" btn-secondary btn-sm" ><i class="fas fa-file-pdf"></i>Imprimir</button>';
			 
			//echo "<meta http-equiv='Refresh' content='5;url=http: ../../../view/Expediente/pdf.php'>";
			echo "</form>";
		
			controller_expediente::CerM($CerM);
		}

		public static function addConsM($ConsM){

			$ConsultaM = $ConsM[3];
			$option= $ConsM[4];

			echo "<form formaction=\"../view/Expediente/pdf.php\" method=\"POST\">";
			echo "<INPUT TYPE=\"HIDDEN\" ID=\"option\" NAME=\"option\" VALUE=\"$option\">";
			echo "<INPUT TYPE=\"HIDDEN\" ID=\"ConsultaM\" NAME=\"ConsultaM\" VALUE=\"$ConsultaM \">";
 			echo ' <button method="POST" type="submit" name="pdf_report" id="pdf_report" formaction="../view/Expediente/pdf.php" class=" btn-secondary btn-sm" ><i class="fas fa-file-pdf"></i>Imprimir</button>';
			 
			//echo "<meta http-equiv='Refresh' content='5;url=http: ../../../view/Expediente/pdf.php'>";
			echo "</form>";
			controller_expediente::ConsM($ConsM);
		
			
		}

		public static function addResC($ResC){
			
			$ConsultaC = $ResC[3];
			$option= $ResC[4];

			echo $ResC[3];
			echo $ResC[4];

			echo "<form formaction=\"../view/Expediente/pdf.php\" method=\"POST\">";
			echo "<INPUT TYPE=\"HIDDEN\" ID=\"option\" NAME=\"option\" VALUE=\"$option\">";
			echo "<INPUT TYPE=\"HIDDEN\" ID=\"ConsultaC\" NAME=\"ConsultaC\" VALUE=\"$ConsultaC \">";
 			echo ' <button method="POST" type="submit" name="pdf_report" id="pdf_report" formaction="../view/Expediente/pdf.php" class=" btn-secondary btn-sm" ><i class="fas fa-file-pdf"></i>Imprimir</button>';
			 
			//echo "<meta http-equiv='Refresh' content='5;url=http: ../../../view/Expediente/pdf.php'>";
			echo "</form>";
			
			controller_expediente::ResC($ResC);
		}
	}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Registro</title>
</head>
<body>
	<?php
	$opcion = isset($_POST['option']) ? $_POST['option'] : '';

	switch ($opcion) {
		case 'Nota':
			$fecha = isset($_POST['fecha']) ? $_POST['fecha'] : '';
			$paciente = isset($_POST['idPaciente']) ? $_POST['idPaciente'] : '';
			$empleado = isset($_POST['IdEmpleado']) ? $_POST['IdEmpleado'] : '';
			$fc = isset($_POST['fc']) ? $_POST['fc'] : '';
			$fr = isset($_POST['fr']) ? $_POST['fr'] : '';
			$ta = isset($_POST['ta']) ? $_POST['ta'] : '';
			$temp = isset($_POST['temp']) ? $_POST['temp'] : '';
			$talla = isset($_POST['talla']) ? $_POST['talla'] : '';
			$peso = isset($_POST['peso']) ? $_POST['peso'] : '';
			$dx = isset($_POST['dx']) ? $_POST['dx'] : '';
			$consulta = isset($_POST['Consulta']) ? $_POST['Consulta'] : '';

			$Nota = array($paciente, $empleado, $fecha, $fc, $fr, $ta, $temp, $talla, $peso, $dx,$consulta);
			
			addHojas::addNotas($Nota);
			break;
		case 'Receta':
			$Fecha = isset($_POST['fecha']) ? $_POST['fecha'] : '';
			$Paciente = isset($_POST['idPaciente']) ? $_POST['idPaciente'] : '';
			$Empleado = isset($_POST['IdEmpleado']) ? $_POST['IdEmpleado'] : '';
			$Edad = isset($_POST['edad']) ? $_POST['edad'] : '';

			$a = isset($_POST['Medicamento']) ? $_POST['Medicamento'] : '';
			$b = isset($_POST['Presentacion']) ? $_POST['Presentacion'] : '';
			$c = isset($_POST['cantidad']) ? $_POST['cantidad'] : '';
			$d = isset($_POST['Indicaciones']) ? $_POST['Indicaciones'] : '';

			$Diagnostico = isset($_POST['diagnostico']) ? $_POST['diagnostico'] : '';

			$Medicamento = explode(',', $a);
			$Presentacion = explode(',', $b);
			$Cantidad = explode(',', $c);
			$Indicaciones = explode(',', $d);

			//$datos = file_get_contents("php://input");
			addHojas::addReceta($Paciente, $Empleado, $Fecha, $Edad, $Diagnostico, $Medicamento, $Presentacion, $Cantidad, $Indicaciones);
			break;
		case 'CerM':
			$opt = isset($_POST['option']) ? $_POST['option'] : '';
			$fecha = isset($_POST['fecha']) ? $_POST['fecha'] : '';
			$paciente = isset($_POST['idPaciente']) ? $_POST['idPaciente'] : '';
			$empleado = isset($_POST['IdEmpleado']) ? $_POST['IdEmpleado'] : '';

			$CerM = array($empleado,$paciente, $fecha,$opt);
			
			addHojas::addCerM($CerM);
			break;
		case 'ConsM':
			$opt = isset($_POST['option']) ? $_POST['option'] : '';
			$fecha = isset($_POST['fecha']) ? $_POST['fecha'] : '';
			$paciente = isset($_POST['idPaciente']) ? $_POST['idPaciente'] : '';
			$empleado = isset($_POST['IdEmpleado']) ? $_POST['IdEmpleado'] : '';
			$text =  isset($_POST['ConsultaM']) ? $_POST['ConsultaM'] : '';

			$ConsM = array($empleado, $paciente, $fecha,$text, $opt);
			
			addHojas::addConsM($ConsM);
			break;
		case 'ResC':
			$opt = isset($_POST['option']) ? $_POST['option'] : '';
			$fecha = isset($_POST['fecha']) ? $_POST['fecha'] : '';
			$paciente = isset($_POST['idPaciente']) ? $_POST['idPaciente'] : '';
			$empleado = isset($_POST['IdEmpleado']) ? $_POST['IdEmpleado'] : '';
			$text =  isset($_POST['ConsultaC']) ? $_POST['ConsultaC'] : '';

			$ResC = array($empleado, $paciente,$fecha,$text,$opt);
			
			addHojas::addResC($ResC);
			break;
		default:
			// $datos = file_get_contents("php://input");
			// echo $datos;
			//echo "<meta http-equiv='Refresh' content='5;url=http: ../index.php'>"
			break;
	}
	?>
</body>
</html>
