<?php
require_once dirname(__DIR__)."/model/model_agenda/model_agendaPaciente.php";

  
	class Interconsultante{

		private $IdInterconsulta;
		private $Folio;
		private $NombrePaciente;
		private $ApellidoPaternoPaciente;
		private $ApellidoMaternoPaciente;
		private $NombreDoctor;
		private $ApellidoPaternoDoctor;
		private $ApellidoMaternoDoctor; 
		private $Especialidad;
		private $FechaInterconsulta;
		Private $Observaciones;
		private $fechaInterconsultaAgendada;
		function __construct(){}



		public function getIdInterconsulta(){
			return $this->IdInterconsulta;
			}
	
			public function setIdInterconsulta($IdInterconsulta){
				$this->IdInterconsulta = $IdInterconsulta;
			}
			public function getFechaInterconsultaAgendada(){
				return $this->fechaInterconsultaAgendada;
				}
		
				public function setFechaInterconsultaAgendada($fechaInterconsultaAgendada){
					$this->fechaInterconsultaAgendada = $fechaInterconsultaAgendada;
				}

		public function getEspecialidad(){
		return $this->Especialidad;
		}

		public function setEspecialidad($Especialidad){
			$this->Especialidad = $Especialidad;
		}

		public function getFolio(){
		return $this->folio;
		}

		public function setFolio($folio){
			$this->folio = $folio;
		}

		public function getNombrePaciente(){
			return $this->NombrePaciente;
		}

		public function setNombrePaciente($NombrePaciente){
			$this->NombrePaciente = $NombrePaciente;
		}

		public function getApellidoPaternoPaciente(){
		return $this->ApellidoPaternoPaciente;
		}
	

		public function getApellidoMaternoPaciente(){
		return $this->ApellidoMaternoPaciente;
		}
		public function setApellidoMaternoPaciente($apellidomaternopaciente){
			$this->ApellidoMaternoPaciente = $apellidomaternopaciente;
		}
		public function setApellidoPaternoPaciente($apellidopaternopaciente){
			$this->ApellidoPaternoPaciente = $apellidopaternopaciente;
		}
		public function getNombreDoctor(){
			return $this->NombreDoctor;
		}

		public function setNombreDoctor($NombreDoctor){
			$this->NombreDoctor = $NombreDoctor;
		}
		public function getApellidoPaternoDoctor(){
			return $this->ApellidoPaternoDoctor;
		}
		public function setApellidoPaternoDoctor($ApellidoPaternoDoctor){
			$this->ApellidoPaternoDoctor = $ApellidoPaternoDoctor;
		}
		public function getApellidoMaternoDoctor(){
			return $this->ApellidoMaternoDoctor;
		}
		public function setApellidoMaternoDoctor($ApellidoMaternoDoctor){
			$this->ApellidoMaternoDoctor = $ApellidoMaternoDoctor;
		}

		public function setFechaInterconsulta($FechaInterconsulta){
			$this->FechaInterconsulta = $FechaInterconsulta;
		}

		public function getFechaInterconsulta(){
		return $this->FechaInterconsulta;
		}
		public function getObservaciones(){
			return $this->Observaciones;
			}
	
			public function setObservaciones($Observaciones){
				$this->Observaciones = $Observaciones;
			}
		
	}
?>
