<?php

require_once dirname(__DIR__)."/model/model_doctor.php"; 

class controller_doctor{

	function __construct(){	}
	
	private static function tabla(){
		$resultado = model_doctor::agendaDiaria();

		if (!empty($resultado)){
			echo "<table class='table'>
					<thead>
						<tr>
							<th>Turno</th>
                        	<th>Expediente</th>
                        	<th>Nombre</th>
                        	<th>Apellido Paterno</th>
                        	<th>Apellido Materno</th>
						</tr>
					</thead>
				</tbody>";
			foreach ($resultado as $key => $value) {
				echo "<tr>
						<td>{$value['IdCita']}</td>
						<td>{$value['IdPaciente']}</td>
						<td>{$value['NombrePaciente']}</td>
						<td>{$value['PrimerApallidoP']}</td>
						<td>{$value['SegundoApellidoP']}</td>
						<td><a href='../Expediente/view_expediente.php?Expediente={$value['IdPaciente']}' class='btn btn-primary'>Abrir</a></td>
				 	</tr>";
			}
			echo "</tbody>";
			echo "</table>";
		}else{
			echo "<h1>No se tiene registrado citas el día de hoy</h1>";
		}	

		$interconsultas = model_doctor::AgendaDiariaInterconsulta(); 

		if (!empty($interconsultas)){
			echo "<hr>";
			echo "<h2>Interconsultas</h2>";
			echo "<table class='table'>
					<thead>
						<tr>
							<th>Turno</th>
                        	<th>Expediente</th>
                        	<th>Nombre</th>
                        	<th>Apellido Paterno</th>
							<th>Apellido Materno</th>
							<th>Motivo</th>
						</tr>
					</thead>
				</tbody>";
			foreach ($interconsultas as $key => $value) {
				echo "<tr>
						<td>{$value['IdInterconsulta']}</td>
						<td>{$value['IdPaciente']}</td>
						<td>{$value['NombrePaciente']}</td>
						<td>{$value['PrimerApallidoP']}</td>
						<td>{$value['SegundoApellidoP']}</td>
						<td>{$value['Observaciones']}</td>
						<td><a href='../Expediente/view_expediente.php?Expediente={$value['IdPaciente']}' class='btn btn-primary'>Abrir</a></td>
				 	</tr>";
			}
			echo "</tbody>";
			echo "</table>";
		}else{
			echo "<hr>";
			echo "<h1>Sin interconsultas registradas</h1>";
		}
	}

	public static function vista(){
		self::tabla();
	}
}

?>
