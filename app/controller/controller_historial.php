<?php
require_once dirname(__DIR__)."/model/model_expedientes.php";

class controller_historial{

	function __construct(){ }

    public function paciente($id){
        $resultado = model_expediente::Paciente($id);
        return $resultado;
    }

    public function contacto($id){
        $resultado = model_expediente::Telefono($id);
        return $resultado;
    }

	private static function Nota($id){
		$resultado = model_expediente::HojasMedicas($id,'notamedica');
		if (!empty($resultado)){
			return $resultado;
		}
	}

	private static function Receta($id){
		
		$resultado = model_expediente::HojasMedicas($id,'recetamedica');
		if (!empty($resultado)){
			return $resultado;
		}
	}

	private static function certificado($id){
		
		$resultado = model_expediente::HojasMedicas($id,'certificado');
		if (!empty($resultado)){
			return $resultado;
		}
	}

	private static function Resumen($id){
		
		$resultado = model_expediente::HojasMedicas($id,'resumenclinico');
		if (!empty($resultado)){
			return $resultado;
		}
	}

	private static function Interconsulta($id){
		$resultado = model_expediente::HojasMedicas($id,'interconsulta');
		if (!empty($resultado)){
			return $resultado;
		}
	}

	public function vista($id){
        $Nota = controller_historial::Nota($id);
        $Receta = controller_historial::Receta($id);
        $Certificado = controller_historial::certificado($id);
        $Resumen = controller_historial::Resumen($id);
        $interconsulta = controller_historial::Interconsulta($id);

        if (!empty($Nota)){
            foreach ($Nota as $key => $value) {
                echo "<tr>";
                //echo "<td>{$value['IdNota']}</td>";
                echo "<td>{$value['Fecha']}</td>";
                echo "<td>Nota Medica</td>";
                echo "<td>{$value['Medico']}</td>";
                echo "<td>{$value['Especialidad']}</td>";
                echo "<td><a class='btn' href=\"javascript:ventanaSecundaria('nota',{$value['IdNota']} )\">Abrir</a></td>";
                echo "</tr>";
            }
        }

        if (!empty($Receta)){
            foreach ($Receta as $key => $value) {
                echo "<tr>";
                echo "<td>{$value['Fecha']}</td>";
                echo "<td>Receta Medica</td>";
                echo "<td>{$value['Medico']}</td>";
                echo "<td>{$value['Especialidad']}</td>";
                echo "<td><a class='btn' href=\"javascript:ventanaSecundaria('receta',{$value['Folio']})\">Abrir</a></td>";
                echo "</tr>";
            }
        }

        if (!empty($Certificado)){
            foreach ($Certificado as $key => $value) {
                echo "<tr>";
                echo "<td>{$value['Fecha']}</td>";
                echo "<td>Certificado</td>";
                echo "<td>{$value['Medico']}</td>";
                echo "<td>{$value['Especialidad']}</td>";
                echo "<td><a class='btn' href=\"javascript:ventanaSecundaria('nota',{$value['IdCertificado']})\">Abrir</a></td>";
                echo "</tr>";
            }
        }

        if (!empty($Resumen)){
            foreach ($Resumen as $key => $value) {
                echo "<tr>";

                echo "<td>{$value['Fecha']}</td>";
                echo "<td>Resumen</td>";
                echo "<td>{$value['Medico']}</td>";
                echo "<td>{$value['Especialidad']}</td>";
                echo "<td><a class='btn' href=\"javascript:ventanaSecundaria('resumen',{$value['IdResumen']})\">Abrir</a></td>";
                echo "</tr>";
            }
        }
        
        if (!empty($interconsulta)){
            foreach ($interconsulta as $key => $value) {
                echo "<tr>";
                echo "<td>{$value['Fecha']}</td>";
                echo "<td>Interconsulta</td>";
                echo "<td>{$value['Medico']}</td>";
                echo "<td>{$value['Especialidad']}</td>";
                echo "<td><a class='btn' href=\"javascript:ventanaSecundaria('interconsultaInfo',{$value['IdInterconsulta']})\">Abrir</a></td>";
                echo "</tr>";
            }
        }            
        
	}

}