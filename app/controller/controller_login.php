<?php
	session_start();
	require_once dirname(__DIR__)."/model/model_login.php";

	$usuario = isset($_POST['usuario']) ? $_POST['usuario'] : '';
	$pass = isset($_POST['pass']) ? $_POST['pass'] : '';

	if ( !(empty($usuario) && empty($pass)) ){
		$login = new controller_login($usuario, $pass);
	}else{
		echo json_encode( array("error"=>false,"mensaje"=>"Error: No se recibió ningún dato") );
	}
	
	class controller_login{
		private $usuario;
        private $contrasena;
		private $servidor;
		
		function __construct($usuario, $pass){
			$this->servidor = new model_login();
			$this->usuario= $usuario;
			$this->contrasena = $pass;
			$this->sesion();
		} //fin del contructor

		// funcion que cifra la contraseña, implementar mas adelante
        private function EnvioPasswordCifrado($contrasena){
            $sha = hash('sha256',$contrasena,false);
            $cifrado = password_hash($sha, PASSWORD_DEFAULT);
            return $cifrado;
        }

        public function sesion(){
        	$sha = hash('sha256', $this->contrasena, false);
        	$datos = $this->servidor->getUser($this->usuario);
        	$r = $datos->fetch();

            if ($this->usuario === $r['Usuario']){
                if(password_verify($sha, $r['Contrasena'])){
                    $sesion = array($r['IdEmpleado'],$r['Nombre'],$r['Cargo'], $r['Cedula']);
                	$_SESSION['inicio'] = $sesion;

                	switch ($r['Cargo']) {
                		case 'Medico':
                			$url = "app/view/view_Medicos/view_doctor.php";
                    		$datos  = array("error"=>true, "url"=>$url);
                    		echo json_encode($datos, JSON_FORCE_OBJECT);
                			break;
                		case 'Recepcionista':
                            $url = "app/view/view_agenda/view_agenda.php";
                            $datos  = array("error"=>true, "url"=>$url);
                            echo json_encode($datos, JSON_FORCE_OBJECT);
                			break;
                        case 'Directivo':
                            // $url = "app/view/view_doctor.php";
                            // $datos  = array("error"=>true, "url"=>$url);
                            // echo json_encode($datos, JSON_FORCE_OBJECT);
                            # code...
                            break;
                		default:
                			# code...
                			break;
                	}
                }else{
                    echo json_encode( array("error"=>false,"mensaje"=>"Usuario incorrecto") );
                	
                }   
            }else{
            	echo json_encode( array("error"=>false,"mensaje"=>"Usuario incorrecto") );
            } //fin del if
        } //fin de sesion
	} //fin de la clase
?>