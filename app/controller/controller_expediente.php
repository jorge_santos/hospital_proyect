<?php 
require dirname(__DIR__)."/model/model_HojasE.php"; 

class controller_expediente{

	function __construct(){ }

	public static function Receta($Paciente, $Empleado, $Fecha, $Edad, $Diagnostico, $Medicamento, $Presentacion, $Cantidad, $Indicaciones){
		model_expediente::AddRecetaMedica($Paciente, $Empleado, $Fecha, $Edad, $Diagnostico);

		$l = count($Medicamento);
		for($i = 0; $i < $l ; $i++){ 
		model_expediente::addRecetaXmedicamento($Fecha ,$Medicamento[$i], $Presentacion[$i], $Cantidad[$i], $Indicaciones[$i]);
		}
	}

	public static function Nota($Nota){
		model_expediente::AddNotaPaciente($Nota);
	}

	public static function paciente($id){
		$resultado = model_expediente::Paciente($id);
		return $resultado;
	}

	public static function medicamento(){
		$medicamentos = model_expediente::Listamedicamento();
		return $medicamentos;

	}

	public static function CerM($CerM){
		model_expediente::AddCerMPaciente($CerM);
	}

	public static function ConsM($ConsM){
		model_expediente::AddConsMPaciente($ConsM);
	}

	public static function ResC($ResC){
		model_expediente::AddResMPaciente($ResC);
	}

}

?>