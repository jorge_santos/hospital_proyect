<?php
	require dirname(__DIR__).'/model/model_doctor.php';

	class controller_busqueda{
		//private static $mensanje = array("Error" => "vacio");
		
		function __construct(){ }

		public static function busquedaFolio($folio){
			header("Content-Type: application/json; charset=UTF-8");

			$resultado = model_doctor::busquedaFolio($folio);
			echo empty($resultado) ? json_encode(array("Error" => "vacio")) : json_encode($resultado);
		}

		public static function busquedaAp($Nombre, $ApPaterno, $ApMaterno){
			header("Content-Type: application/json; charset=UTF-8");

			$regex1 = !empty($Nombre) ? "%{$Nombre}%" : "";
			$regex2 = !empty($ApPaterno) ? "%{$ApPaterno}%" : "";
			$regex3 = !empty($ApMaterno) ? "%{$ApMaterno}%" : "";

			$resultado = model_doctor::busquedaAp($regex1, $regex2, $regex3);
			echo empty($resultado) ? json_encode(array("Error" => "vacio")) : json_encode($resultado);
		}
	}


$action = isset($_GET['action']) ? $_GET['action'] : '';

$folio = isset($_POST['folio']) ? $_POST['folio'] : '';
$Nombre = isset($_POST['nombre']) ? $_POST['nombre'] : '';
$ApPaterno = isset($_POST['paterno']) ? $_POST['paterno'] : '';
$ApMaterno = isset($_POST['materno']) ? $_POST['materno'] : '';	

	if($action == 1){
		controller_busqueda::busquedaFolio($folio);
	}
	if($action == 2){
		controller_busqueda::busquedaAp($Nombre, $ApPaterno, $ApMaterno);
	}

?>