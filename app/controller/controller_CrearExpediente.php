<?php
class CrearExpendiente{
	function __construct(){}
	

			private $Municipio;
			private $Ciudad;
			private $ESocioeconomicos;
			private $Ine;
			private $Activo;
			private $FechaAfiliacion;

			private $Curp;
			private $SeguroP;
			private $NombrePacienteN;
			private $ApellidoPaternoPacienteN;
			private $ApellidoMaternoPacienteN;
			private $PacienteNuevoFechaNacimiento;
			private $Telefono;
			private $Sexo;
			private $TipoSagre;
			private $Calle;
			private $Lote;
			private $Manzana;
			private $Colonia;
			private $CP;
			public function getActivo(){
				return $this->activo;
				}
				public function setActivo($activo){
					$this->activo = $activo;

				}

				public function getIne(){
					return $this->ine;
					}
					public function setIne($ine){
						$this->ine = $ine;
	
					}
			public function getMunicipio(){
				return $this->municipio;
				}
		
				public function setMunicipio($municipio){
					$this->municipio = $municipio;
				}
				public function getCiudad(){
				return $this->ciudad;
				}
		
				public function setCiudad($ciudad){
					$this->ciudad = $ciudad;
				}
		
				public function getESocioeconomicos(){
				return $this->eSocioeconomicos;
				}
				public function setESocioeconomicos($eSocioeconomicos){
					$this->eSocioeconomicos = $eSocioeconomicos;
				}

			public function getCurp(){
			return $this->curp;
			}
	
			public function setCurp($curp){
				$this->curp = $curp;
			}
			public function getSeguroP(){
			return $this->segurop;
			}
	
			public function setSeguroP($segurop){
				$this->segurop = $segurop;
			}
	
			public function getFolio(){
			return $this->folio;
			}
	
			public function setFolio($folio){
				$this->folio = $folio;
			}
	
			public function getNombrePacienteN(){
				return $this->nombrepacienten;
			}
	
			public function setNombrePacienteN($nombrepacienten){
				$this->nombrepacienten = $nombrepacienten;
			}
			public function getApellidoPaternoPacienteN(){
				return $this->apellidopaternopacienten;
				}
		
				public function setApellidoPaternoPacienteN($apellidopaternopacienten){
					$this->apellidopaternopacienten = $apellidopaternopacienten;
				}
			public function getApellidoMaternoPacienteN(){
			return $this->apellidomaternopacienten;
			}
	
			public function setApellidoMaternoPacienteN($apellidomaternopacienten){
				$this->apellidomaternopacienten = $apellidomaternopacienten;
			}
			public function getPacienteNuevoFechaNacimiento(){
				return $this->pacienteNuevoFechaNacimiento;
			}
	
			public function setPacienteNuevoFechaNacimiento($pacienteNuevoFechaNacimiento){
				$this->pacienteNuevoFechaNacimiento = $pacienteNuevoFechaNacimiento;
			}
			public function getTelefono(){
				return $this->telefono;
			}
			public function setTelefono($telefono){
				$this->telefono = $telefono;
			}
			public function getSexo(){
				return $this->sexo;
			}
			public function setSexo($sexo){
				$this->sexo = $sexo;
			}
			public function getTipoSagre(){
				return $this->tipoSagre;
				}
		
				public function setTipoSagre($tipoSagre){
					$this->tipoSagre = $tipoSagre;
				}
				public function getManzana(){
					return $this->manzana;
				}
		
				public function setManzana($manzana){
					$this->manzana = $manzana;
				}
				public function getLote(){
					return $this->lote;
				}
				public function setLote($lote){
					$this->lote = $lote;
				}
				public function getColonia(){
					return $this->colonia;
				}
				public function setColonia($colonia){
					$this->colonia = $colonia;
				}
				public function getCP(){
					return $this->cp;
				}
				public function setCP($cp){
					$this->cp = $cp;
				}
				public function getCalle(){
					return $this->calle;
				}
				public function setCalle($calle){
					$this->calle = $calle;
				}
				public function getFechaAfiliacion(){
					return $this->fechaAfiliacion;
				}
				public function setFechaAfiliacion($fechaAfiliacion){
					$this->fechaAfiliacion = $fechaAfiliacion;
				}


        }
?>