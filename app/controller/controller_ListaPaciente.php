<?php
require_once dirname(__DIR__)."/model/model_agenda/model_agendaPaciente.php";

  $idEliminarCita = isset($_GET['IdCita']) ? $_GET['IdCita'] : '';
	

	if ( !(empty($idEliminarCita) )){
		$crud= new CrudPaciente(); 
	$crud->eliminar($idEliminarCita);
	header('Location: /proyecto_hospital/app/view/view_agenda/view_agenda.php'); 
	}
	class ListaPaciente{

		private $FechaNaciPaci;

		private $SeguroPopuPacienete;
		private $Id;
		private $Folio;
		private $NombrePaciente;
		private $ApellidoPaternoPaciente;
		private $ApellidoMaternoPaciente;
		private $NombreDoctor;
		private $ApellidoPaternoDoctor;
		private $ApellidoMaternoDoctor;
		private $Especialidad;
		private $FechaCita;
		private $Turno;
		function __construct(){}
			public function getSeguroPopuPacienete(){
				return $this->seguroPopuPacienete;
				}
		
				public function setSeguroPopuPacienete($seguroPopuPacienete){
					$this->seguroPopuPacienete = $seguroPopuPacienete;
				}
			public function getFechaNaciPaci(){
				return $this->fechaNaciPaci;
				}
		
				public function setFechaNaciPaci($fechaNaciPaci){
					$this->fechaNaciPaci = $fechaNaciPaci;
				}
		public function getTurno(){
		return $this->turno;
		}

		public function setTurno($turno){
			$this->turno = $turno;
		}

		public function getId(){
			return $this->id;
			}
	
			public function setId($id){
				$this->id = $id;
			}
		public function getEspecialidad(){
		return $this->especialidad;
		}

		public function setEspecialidad($especialidad){
			$this->especialidad = $especialidad;
		}

		public function getFolio(){
		return $this->folio;
		}

		public function setFolio($folio){
			$this->folio = $folio;
		}

		public function getNombrePaciente(){
			return $this->nombrepaciente;
		}

		public function setNombrePaciente($nombrepaciente){
			$this->nombrepaciente = $nombrepaciente;
		}

		public function getApellidoPaternoPaciente(){
		return $this->ApellidoPaternoPaciente;
		}
	

		public function getApellidoMaternoPaciente(){
		return $this->ApellidoMaternoPaciente;
		}
		public function setApellidoMaternoPaciente($apellidomaternopaciente){
			$this->ApellidoMaternoPaciente = $apellidomaternopaciente;
		}
		public function setApellidoPaternoPaciente($apellidopaternopaciente){
			$this->ApellidoPaternoPaciente = $apellidopaternopaciente;
		}
		public function getNombreDoctor(){
			return $this->nombredoctor;
		}

		public function setNombreDoctor($nombredoctor){
			$this->nombredoctor = $nombredoctor;
		}
		public function getApellidoPaternoDoctor(){
			return $this->apellidopaternodoctor;
		}
		public function setApellidoPaternoDoctor($apellidopaternodoctor){
			$this->apellidopaternodoctor = $apellidopaternodoctor;
		}
		public function getApellidoMaternoDoctor(){
			return $this->apellidomaternodoctor;
		}
		public function setApellidoMaternoDoctor($apellidomaternodoctor){
			$this->apellidomaternodoctor = $apellidomaternodoctor;
		}

		public function setFechaCita($FechaCita){
			$this->FechaCita = $FechaCita;
		}

		public function getFechaCita(){
		return $this->FechaCita;
		}
		
	}
?>
