<?php
// incluye la clase Db
require_once dirname(__DIR__).'../../config/conexion2.php';


	class Crudpaciente{
			function __construct(){	 
			}


			public function pacienteFiltroFolio($idpaciente){

				$db=Db::conectar();
				$Nuevopaciente=[];
				$select2=$db->query('SELECT Idpaciente,Nombrepaciente,PrimerApallidoP,SegundoApellidoP,FechaNacimiento,SeguroPopular FROM paciente WHERE `Idpaciente` ='.$idpaciente.'' );
			
				// $select2->bindValue(':idpaciente',$IdPacien);
				foreach($select2->fetchAll() as $paciente){
					$Citapaciente = new Listapaciente();
				$Citapaciente->setFolio($paciente['Idpaciente']);
				
				$Citapaciente->setNombrepaciente($paciente['Nombrepaciente']);
				$Citapaciente->setApellidoPaternopaciente($paciente['PrimerApallidoP']);
				$Citapaciente->setApellidoMaternopaciente($paciente['SegundoApellidoP']);
				$Citapaciente->setFechaNaciPaci($paciente['FechaNacimiento']);
				$Citapaciente->setSeguroPopuPacienete($paciente['SeguroPopular']);
				$Nuevopaciente[]=$Citapaciente;
			}
			 
			return $Nuevopaciente;
		}

		public function pacienteFiltroNombre($nombre, $papellido, $sapellido){

			$db=Db::conectar();
			$Nuevopaciente=[];
			
			//$select3=$db->query('SELECT  Idpaciente,Nombrepaciente,PrimerApallidoP,SegundoApellidoP,FechaNacimiento,SeguroPopular FROM paciente WHERE `Nombrepaciente` ='.$nombre.' OR `PrimerApallidoP` ='.$papellido.' OR `SegundoApellidoP` ='.$sapellido.' ' );
			$select3 = $db->query("SELECT `Idpaciente`,`Nombrepaciente`,`PrimerApallidoP`,`SegundoApellidoP`,`FechaNacimiento`,`SeguroPopular` FROM `paciente` WHERE Nombrepaciente = '".$nombre."' and  PrimerApallidoP = '".$papellido."'  and  SegundoApellidoP = '".$sapellido."'  ");
			// $select2->bindValue(':idpaciente',$IdPacien);
			foreach($select3->fetchAll() as $paciente){
				$Citapaciente = new Listapaciente();
			$Citapaciente->setFolio($paciente['Idpaciente']);
			$Citapaciente->setNombrepaciente($paciente['Nombrepaciente']); 
			$Citapaciente->setApellidoPaternoPaciente($paciente['PrimerApallidoP']);
			$Citapaciente->setApellidoMaternoPaciente($paciente['SegundoApellidoP']);
			$Citapaciente->setFechaNaciPaci($paciente['FechaNacimiento']);
			$Citapaciente->setSeguroPopuPacienete($paciente['SeguroPopular']);
			$Nuevopaciente[]=$Citapaciente;
		}
		
		return $Nuevopaciente;
	}


			public function mostrarFiltro($FechaCita,$Idespecialidad,$Idempleado){

				$db=Db::conectar();
				$listaPac2=[];
				$select=$db->query("SELECT citamedica.IdCita,paciente.Idpaciente,paciente.Nombrepaciente,paciente.PrimerApallidoP,paciente.SegundoApellidoP,empleado.Nombreempleado,empleado.PrimerApellidoE,empleado.SegundoApellidoE,especialidad.especialidad,citamedica.FechaCita,horario.Turno from citamedica 
				INNER JOIN paciente
				on citamedica.Idpaciente=paciente.Idpaciente
				INNER JOIN empleado
				on citamedica.Idempleado=empleado.Idempleado
				INNER JOIN especialidad
				on empleado.Idespecialidad=especialidad.Idespecialidad
				INNER JOIN horario
				ON empleado.Idhorario=horario.Idhorario 
                
                WHERE citamedica.FechaCita = '$FechaCita' AND especialidad.Idespecialidad='$Idespecialidad' AND empleado.Idempleado='$Idempleado'");
			
			
				foreach($select->fetchAll() as $agenda){
					$listapaciente3= new Listapaciente();
					$listapaciente3->setId($agenda['IdCita']);
					$listapaciente3->setFolio($agenda['Idpaciente']);
					$listapaciente3->setNombrepaciente($agenda['Nombrepaciente']);
					$listapaciente3->setApellidoPaternopaciente($agenda['PrimerApallidoP']);
					$listapaciente3->setApellidoMaternopaciente($agenda['SegundoApellidoP']);
					$listapaciente3->setNombreDoctor($agenda['Nombreempleado']);
					$listapaciente3->setApellidoPaternoDoctor($agenda['PrimerApellidoE']);
					$listapaciente3->setApellidoMaternoDoctor($agenda['SegundoApellidoE']);
					$listapaciente3->setespecialidad($agenda['especialidad']);
					$listapaciente3->setFechaCita($agenda['FechaCita']);
					$listapaciente3->setTurno($agenda['Turno']);
				
					$listaPac2[]=$listapaciente3;
				}
				return $listaPac2;
			}
			
		public function mostrar(){

			$db=Db::conectar();
			$diaActual=date("Y-m-d");
			$listaPac=[];
			$select=$db->query("SELECT citamedica.IdCita,paciente.Idpaciente,paciente.Nombrepaciente,paciente.PrimerApallidoP,paciente.SegundoApellidoP,empleado.Nombreempleado,empleado.PrimerApellidoE,empleado.SegundoApellidoE,especialidad.especialidad,citamedica.FechaCita,horario.Turno from citamedica 
			INNER JOIN paciente
			on citamedica.Idpaciente=paciente.Idpaciente
			INNER JOIN empleado
			on citamedica.Idempleado=empleado.Idempleado
			INNER JOIN especialidad
			on empleado.Idespecialidad=especialidad.Idespecialidad
			INNER JOIN horario
			ON empleado.Idhorario=horario.Idhorario
			WHERE citamedica.FechaCita='$diaActual'");
			
		

			foreach($select->fetchAll() as $agenda){
				$listapaciente= new Listapaciente();
				$listapaciente->setId($agenda['IdCita']);
				$listapaciente->setFolio($agenda['Idpaciente']);
				$listapaciente->setNombrepaciente($agenda['Nombrepaciente']);
				$listapaciente->setApellidoPaternopaciente($agenda['PrimerApallidoP']);
				$listapaciente->setApellidoMaternopaciente($agenda['SegundoApellidoP']);
				$listapaciente->setNombreDoctor($agenda['Nombreempleado']);
				$listapaciente->setApellidoPaternoDoctor($agenda['PrimerApellidoE']);
				$listapaciente->setApellidoMaternoDoctor($agenda['SegundoApellidoE']);
				$listapaciente->setespecialidad($agenda['especialidad']);
				$listapaciente->setFechaCita($agenda['FechaCita']);
				$listapaciente->setTurno($agenda['Turno']);
			
				$listaPac[]=$listapaciente;
			}
			return $listaPac;
		}


		public function pacienteAgendar($IdPacien){

			$db=Db::conectar();
			
			$select2=$db->prepare('SELECT Idpaciente,Nombrepaciente,PrimerApallidoP,SegundoApellidoP FROM paciente WHERE `Idpaciente` ='.$IdPacien.'' );
		
			// $select2->bindValue(':idpaciente',$IdPacien);
			$select2->execute();
			$paciente=$select2->fetch();		
				$Citapaciente = new Listapaciente();
				$Citapaciente->setFolio($paciente['Idpaciente']);
				$Citapaciente->setNombrepaciente($paciente['Nombrepaciente']);
				$Citapaciente->setApellidoPaternopaciente($paciente['PrimerApallidoP']);
				$Citapaciente->setApellidoMaternopaciente($paciente['SegundoApellidoP']);
			return $Citapaciente;
		}

public function obtenerListapaciente($cveart){
			$db=Db::conectar();
			$select=$db->prepare('SELECT * FROM Articulos WHERE CveArt=:cveart');
			$select->bindValue('cveart',$cveart);
			$select->execute();
			$articulo=$select->fetch();
			$myArticulo= new Articulo();
			$myArticulo->setCveArt($articulo['CveArt']);
			$myArticulo->setDescripcion($articulo['Descripcion']);
			$myArticulo->setPrecio($articulo['Precio']);
			$myArticulo->setIVA($articulo['IVA']);
			$myArticulo->setdescuento($articulo['Descuento']);
			return $myArticulo;
		}

		public function mostrarpaciente(){

			$db=Db::conectar();
			$NuevoPac=[];
			$select2=$db->query('SELECT Idpaciente,Nombrepaciente,PrimerApallidoP,SegundoApellidoP,FechaNacimiento,SeguroPopular FROM paciente');
		
			
			foreach($select2->fetchAll() as $paciente){
				$Nuevopaciente = new Listapaciente();

				
				
				$Nuevopaciente->setFolio($paciente['Idpaciente']);
				$Nuevopaciente->setNombrepaciente($paciente['Nombrepaciente']);
				$Nuevopaciente->setApellidoPaternopaciente($paciente['PrimerApallidoP']);
				$Nuevopaciente->setApellidoMaternopaciente($paciente['SegundoApellidoP']);
				$Nuevopaciente->setFechaNaciPaci($paciente['FechaNacimiento']);
				$Nuevopaciente->setSeguroPopuPacienete($paciente['SeguroPopular']);
				
				$NuevoPac[]=$Nuevopaciente;
			}
			
			return $NuevoPac;
		}

		public function eliminar($IdCita){
			$db=Db::conectar();
			$eliminar=$db->prepare('DELETE FROM citamedica WHERE IdCita=:IdCita');
			$eliminar->bindValue('IdCita',$IdCita);
			$eliminar->execute();

			
		}

		public function insertarExpedienteNuevo($crearExpendiente){

			
			
			$db=Db::conectar();
			$insert=$db->prepare('INSERT INTO paciente (`Idpaciente`, `Nombre`, `ApellidoPaterno`, `ApellidoMaterno`, `FechaNacimiento`, `Curp`, `Genero`, `Calle`, `Municipio`, `Colonia`, `Ciudad`, `Lote`, `Manzana`, `CP`, `TipoSangre`, `SeguroPopular`, `FechaAfiliacion`, `Activo`, `Ine`, `ESocioeconomicos`, `Telefono`) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
			$insert->bindValue('Idpaciente',12);
			$insert->bindValue('Nombre',"dggdf");
			$insert->bindValue('ApellidoPaterno',"dggdf");
			$insert->bindValue('ApellidoMaterno',"dggdf");
			$insert->bindValue('FechaNacimiento',"dggdf");
			$insert->bindValue('Curp',"dggdf");
			$insert->bindValue('Genero',"dggdf");
			$insert->bindValue('Calle',"dggdf");
			$insert->bindValue('Municipio',"dggdf");
			$insert->bindValue('Colonia',"dggdf");
			$insert->bindValue('Ciudad',"dggdf");
			$insert->bindValue('Lote',"dggdf");
			$insert->bindValue('Manzana',"dggdf");
			$insert->bindValue('CP',"dggdf");
			$insert->bindValue('TipoSangre',"dggdf");
			$insert->bindValue('SeguroPopular',"dggdf");
			$insert->bindValue('FechaAfiliacion',"dggdf");
			$insert->bindValue('Activo',"dggdf");
			$insert->bindValue('Ine',"dggdf");
			$insert->bindValue('ESocioeconomicos',"dggdf");
			$insert->bindValue('Telefono',"dggdf");
			$insert->execute();
			

		}

		
		public function mostrarInterconsulta(){

			$db=Db::conectar();
			$listaPac=[];
			$select=$db->query("SELECT interconsulta.IdInterconsulta,paciente.Idpaciente,paciente.Nombrepaciente,paciente.PrimerApallidoP,paciente.SegundoApellidoP,empleado.Nombreempleado,empleado.PrimerApellidoE,empleado.SegundoApellidoE,especialidad.especialidad,interconsulta.Fecha, interconsulta.Observaciones from interconsulta 
			INNER JOIN paciente
			on interconsulta.Idpaciente=paciente.Idpaciente
			INNER JOIN empleado
			on interconsulta.Idempleado=empleado.Idempleado
			INNER JOIN especialidad
			on empleado.Idespecialidad=especialidad.Idespecialidad
			WHERE interconsulta.Estado=0"); 
			
		

			foreach($select->fetchAll() as $agenda){
				$listapaciente= new Interconsultante();
				$listapaciente->setIdInterconsulta($agenda['IdInterconsulta']);
				$listapaciente->setFolio($agenda['Idpaciente']);
				$listapaciente->setNombrepaciente($agenda['Nombrepaciente']);
				$listapaciente->setApellidoPaternopaciente($agenda['PrimerApallidoP']);
				$listapaciente->setApellidoMaternopaciente($agenda['SegundoApellidoP']);
				$listapaciente->setNombreDoctor($agenda['Nombreempleado']);
				$listapaciente->setApellidoPaternoDoctor($agenda['PrimerApellidoE']);
				$listapaciente->setApellidoMaternoDoctor($agenda['SegundoApellidoE']);
				$listapaciente->setespecialidad($agenda['especialidad']);
				$listapaciente->setFechaInterconsulta($agenda['Fecha']);
				$listapaciente->setObservaciones($agenda['Observaciones']);
			
				$listaPac[]=$listapaciente;
			}
			return $listaPac;
		}

		public function mostrarInterconsultaPaciente($IdInterconsulta){

			$db=Db::conectar();
			$listaPac=[];
			$select=$db->query("SELECT interconsulta.IdInterconsulta,paciente.Idpaciente,paciente.Nombrepaciente,paciente.PrimerApallidoP,paciente.SegundoApellidoP,empleado.Nombreempleado,empleado.PrimerApellidoE,empleado.SegundoApellidoE,especialidad.especialidad,interconsulta.Fecha, interconsulta.Observaciones from interconsulta 
			INNER JOIN paciente
			on interconsulta.Idpaciente=paciente.Idpaciente
			INNER JOIN empleado
			on interconsulta.Idempleado=empleado.Idempleado
			INNER JOIN especialidad
			on empleado.Idespecialidad=especialidad.Idespecialidad
			WHERE interconsulta.IdInterconsulta='$IdInterconsulta'"); 
			
		

			foreach($select->fetchAll() as $agenda){
				$listapaciente= new Interconsultante();
				$listapaciente->setIdInterconsulta($agenda['IdInterconsulta']);
				$listapaciente->setFolio($agenda['Idpaciente']);
				$listapaciente->setNombrepaciente($agenda['Nombrepaciente']);
				$listapaciente->setApellidoPaternopaciente($agenda['PrimerApallidoP']);
				$listapaciente->setApellidoMaternopaciente($agenda['SegundoApellidoP']);
				$listapaciente->setNombreDoctor($agenda['Nombreempleado']);
				$listapaciente->setApellidoPaternoDoctor($agenda['PrimerApellidoE']);
				$listapaciente->setApellidoMaternoDoctor($agenda['SegundoApellidoE']);
				$listapaciente->setespecialidad($agenda['especialidad']);
				$listapaciente->setFechaInterconsulta($agenda['Fecha']);
				$listapaciente->setObservaciones($agenda['Observaciones']);
			
				$listaPac[]=$listapaciente;
			}
			
			return $listaPac;
		}



		
	}
?>