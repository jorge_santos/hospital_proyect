<?php
	require_once dirname(__DIR__).'/config/conexion.php'; 

	class model_doctor{

		function __construct(){	}

		public static function busquedaFolio($folio){
			$servidor = conexion::con();
			$query="select P.IdPaciente, P.NombrePaciente, P.PrimerApallidoP, P.SegundoApellidoP, LEFT(FechaNacimiento,4) as Fecha , P.SeguroPopular
					from paciente P 
					where P.IdPaciente = :f ";
			$prepared = $servidor->prepare($query);
			$prepared->bindParam(":f", $folio);
			$prepared->execute();
			$resultado = $prepared->fetchAll(PDO::FETCH_ASSOC);
			return $resultado;
		}

		public static function busquedaAp($nombre, $ap, $am){
			$servidor = conexion::con();
			$query="select p.IdPaciente, p.NombrePaciente, p.PrimerApallidoP, p.SegundoApellidoP, LEFT(FechaNacimiento,4) as Fecha,  p.SeguroPopular
					from paciente p 
					where p.NombrePaciente like :nombre 
					or p.PrimerApallidoP like :ap
					or p.SegundoApellidoP like :am ";
			$prepared = $servidor->prepare($query);
			$prepared->bindParam(":nombre", $nombre);
			$prepared->bindParam(":ap", $ap);
			$prepared->bindParam(":am", $am);
			$prepared->execute();

			$resultado = $prepared->fetchAll(PDO::FETCH_ASSOC);
			return $resultado;
		}

		public static function AgendaDiaria(){
			$servidor = conexion::con();
			$id = $_SESSION['inicio'][0];
			$diaActual=date("Y-m-d");
			/*$query = "select AM.IdCita, AM.IdPaciente, P.NombrePaciente, P.PrimerApallidoP, P.SegundoApellidoP from citamedica AM inner join paciente P on AM.IdPaciente = P.IdPaciente where AM.IdEmpleado = 2 and AM.FechaCita = "2019-05-07" ";*/

			$query="select AM.IdCita, AM.IdPaciente, P.NombrePaciente, P.PrimerApallidoP, P.SegundoApellidoP
					from citamedica AM
					inner join paciente P
					on AM.IdPaciente = P.IdPaciente
					where AM.IdEmpleado = :id AND AM.FechaCita=:fecha";
			$prepared = $servidor->prepare($query);
			$prepared->bindParam(":id", $id);
			$prepared->bindParam(":fecha", $diaActual);
			$prepared->execute();
			$resultado = $prepared->fetchAll(PDO::FETCH_ASSOC);
			return $resultado;
		}
		
		public static function AgendaDiariaInterconsulta(){ 
			$servidor = conexion::con();
			$id = $_SESSION['inicio'][0];
			$diaActual=date("Y-m-d");
			/*$query = "select AM.IdCita, AM.IdPaciente, P.NombrePaciente, P.PrimerApallidoP, P.SegundoApellidoP from citamedica AM inner join paciente P on AM.IdPaciente = P.IdPaciente where AM.IdEmpleado = 2 and AM.FechaCita = "2019-05-07" ";*/

			$query="SELECT interconsulta.IdInterconsulta,paciente.IdPaciente,paciente.NombrePaciente,paciente.PrimerApallidoP,paciente.SegundoApellidoP,empleado.Nombreempleado,empleado.PrimerApellidoE,empleado.SegundoApellidoE,especialidad.especialidad,interconsulta.Fecha, interconsulta.Observaciones from interconsulta 
			INNER JOIN paciente
			on interconsulta.Idpaciente=paciente.Idpaciente
			INNER JOIN empleado
			on interconsulta.Idempleado=empleado.Idempleado
			INNER JOIN especialidad
			on empleado.Idespecialidad=especialidad.Idespecialidad
			WHERE interconsulta.FechaAgenda=:fecha AND empleado.IdEmpleado=:id";

			$prepared = $servidor->prepare($query);
			$prepared->bindParam(":id", $id);
			$prepared->bindParam(":fecha", $diaActual);
			$prepared->execute();
			$resultado = $prepared->fetchAll(PDO::FETCH_ASSOC);
			return $resultado;
		}
	}

?>
