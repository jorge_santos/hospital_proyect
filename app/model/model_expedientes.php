<?php
require_once dirname(__DIR__). "/config/conexion.php";

class model_expediente{

	function __construct(){	}

	public static function HojasMedicas($id, $tabla, $fecha=""){
		// SELECT * FROM `notamedica` where IdPaciente = 1 and Fecha like '%%';
		$servidor = conexion::con();
		$llave = '';
		switch ($tabla) {
			case 'recetamedica':
				$llave = 'Folio';
				break;
			case 'notamedica':
				$llave = 'IdNota';
				break;
			/*case 'certificados':
				$llave = 'IdCertificado';
				break;
			case 'resumenclinico':
				$llave = 'IdResumen';
				break;*/
			case 'interconsulta':
				$llave = 'IdInterconsulta';
				break;
			
			default:
				break;
		}
		$query="select C.{$llave}, C.Fecha, concat(E.NombreEmpleado,' ',E.PrimerApellidoE, ' ', E.SegundoApellidoE) as Medico , ES.Especialidad
				from {$tabla} C
				inner JOIN paciente P
				on P.IdPaciente = C.IdPaciente
				inner join empleado E
				on C.IdEmpleado = E.IdEmpleado
				inner join especialidad ES
				on E.IdEspecialidad = ES.IdEspecialidad
				WHERE P.IdPaciente = :id
				AND C.Fecha like :fecha";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":id", $id);
		$prepared->bindParam(":fecha", $fecha);
		$prepared->execute();
		$resultado = $prepared->fetchAll(PDO::FETCH_ASSOC);
		return $resultado;
	}

	public static function consMedica($id){
		$servidor = conexion::con();
		$query = "select * from constanciam where IdPaciente = :id";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":id", $id);
		$prepared->execute();
		$resultado = $prepared->fetchAll(PDO::FETCH_ASSOC);
	}


	public function CertDifuncion(){
		
	}

	public static function Paciente($id){
		$servidor = conexion::con();
		$query="select concat(NombrePaciente,' ',PrimerApallidoP,' ',SegundoApellidoP) as Nombre, FechaNacimiento, Curp, Genero, SeguroPopular, Calle, Municipio, Colonia, Ciudad, NumExt, NumInt, CP, TipoSangre, SeguroPopular, FechaAfiliacion, Activo
				from paciente
				where IdPaciente = :id";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":id", $id);
		$prepared->execute();
		$resultado = $prepared->fetch(PDO::FETCH_ASSOC);
		return $resultado;
	}

	public static function Telefono($id){
		$servidor = conexion::con();
		$query = "select T.Telefono, T.TipoTelefono 
		from pacientexcontacto PXT 
		INNER join paciente P on P.IdPaciente = PXT.IdPaciente
		inner join contacto T
		on T.IdTelefono = PXT.IdTelefono
		where P.IdPaciente = :id ";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":id", $id);
		$prepared->execute();
		$resultado = $prepared->fetchAll(PDO::FETCH_ASSOC);
		return $resultado;
	}

	public static function Listamedicamento(){
		$servidor = conexion::con();
		$query = "select IdMedicamento, NombreGenerico, Presentacion from medicamento";
		$prepared = $servidor->prepare($query);
		$prepared->execute();
		$resultado = $prepared->fetch(PDO::FETCH_ASSOC);
		return $resultado;
	}

	public static function reportesMedicas($id, $tabla, $fecha=""){
		// SELECT * FROM `notamedica` where IdPaciente = 1 and Fecha like '%%';
		$servidor = conexion::con();
		$llave = '';
		$query = '';
		switch ($tabla) {
			case 'certificadom':
				$query="select C.IdcerM, C.FechaActual, concat(E.NombreEmpleado,' ',E.PrimerApellidoE, ' ', E.SegundoApellidoE) as Medico, ES.Especialidad
					from certificadom C
					inner JOIN paciente P on P.IDPaciente = C.IDPaciente
					inner join empleado E on C.IdEmpleado = E.IdEmpleado
					inner join especialidad ES on E.IdEspecialidad = ES.IdEspecialidad
					WHERE P.IDPaciente = :id
					AND C.FechaActual like :fecha";
				break;
			case 'constanciam':
				$query="select C.IdConsM, C.FechaActual, concat(E.NombreEmpleado,' ',E.PrimerApellidoE, ' ', E.SegundoApellidoE) as Medico, ES.Especialidad
					from constanciam C
					inner JOIN paciente P on P.IdPaciente = C.IdPaciente
					inner join empleado E on C.IdEmpleado = E.IdEmpleado
					inner join especialidad ES on E.IdEspecialidad = ES.IdEspecialidad
					WHERE P.IdPaciente = :id
					AND C.FechaActual like :fecha";
				break;
			case 'resumenc':
				$query="select C.IdResC, C.FechaActual, concat(E.NombreEmpleado,' ',E.PrimerApellidoE, ' ', E.SegundoApellidoE) as Medico, ES.Especialidad 
					from resumenc C 
					inner JOIN paciente P on P.IdPaciente = C.IdPaciente 
					inner join empleado E on C.IdEmpleado = E.IdEmpleado 
					inner join especialidad ES on E.IdEspecialidad = ES.IdEspecialidad 
					WHERE P.IdPaciente = :id
					AND C.FechaActual like :fecha";
				break;
			default:
				break;
		}

		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":id", $id);
		$prepared->bindParam(":fecha", $fecha);
		$prepared->execute();
		$resultado = $prepared->fetchAll(PDO::FETCH_ASSOC);
		return $resultado;
	}

}
// que comando php oṕtengo la informacion del cuerpo de un paquete http
?>