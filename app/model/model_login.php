<?php
	require dirname(__DIR__)."/config/conexion.php";

	class model_login{

		function __construct(){ }

		public function getUser($usuario){
            $servidor = conexion::con();
            $query="select U.IdEmpleado, U.Usuario, U.Contrasena, concat(E.NombreEmpleado,' ',E.PrimerApellidoE, ' ', E.SegundoApellidoE) as Nombre, C.Cargo, E.Cedula
            	from usuario U
            	inner join empleado E
            	on U.IdEmpleado = E.IdEmpleado
            	inner join cargo C
            	on E.IdCargo = C.IdCargo where U.Usuario = :u";
            $prepared = $servidor->prepare($query);
            $prepared->bindParam(":u", $usuario);
            $prepared->execute();
            return $prepared;
        }
	}
?>