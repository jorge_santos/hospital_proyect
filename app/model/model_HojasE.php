<?php
require_once dirname(__DIR__). "/config/conexion.php";

class model_expediente{

	function __construct(){	}

	public static function AddNotaPaciente($Nota){
		$servidor = conexion::con();
		$query = "INSERT INTO notamedica VALUES (NULL, :IdPaciente, :IdEmpleado, :Fecha, :Frecuencia_Cardiaca, :Frecuencia_Respiratoria, :Tension_Arterial, :Temperatura, :Talla, :Peso, :Glucosa, :consulta);";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":IdPaciente",$Nota[0]);
		$prepared->bindParam(":IdEmpleado",$Nota[1]);
		$prepared->bindParam(":Fecha",$Nota[2]);
		$prepared->bindParam(":Frecuencia_Cardiaca",$Nota[3]);
		$prepared->bindParam(":Frecuencia_Respiratoria", $Nota[4]);
		$prepared->bindParam(":Tension_Arterial", $Nota[5]);
		$prepared->bindParam(":Temperatura", $Nota[6]);
		$prepared->bindParam(":Talla",$Nota[7]);
		$prepared->bindParam(":Peso",$Nota[8]);
		$prepared->bindParam(":Glucosa",$Nota[9]);
		$prepared->bindParam(":consulta", $Nota[10]);
		$prepared->execute();
	}

	public static function AddRecetaMedica($Paciente, $Empleado, $Fecha, $Edad, $Diagnostico){
		$servidor = conexion::con();
		$query = "insert into recetamedica value(null, :paciente, :empleado, :fecha, :edad, :diagnostico);";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":paciente",$Paciente);
		$prepared->bindParam(":empleado",$Empleado);
		$prepared->bindParam(":fecha",$Fecha);
		$prepared->bindParam(":edad",$Edad);
		$prepared->bindParam(":diagnostico",$Diagnostico);
		$prepared->execute();
	}

	public static function addRecetaXmedicamento($Fecha, $Medicamento, $Presentacion, $Cantidad, $Indicaciones){
		$servidor = conexion::con();
		$query = "insert into recetaxmedicamento value ((select a.Folio from recetamedica a WHERE a.Fecha = :fecha), :medicamento , :cantidad, :indicaciones );";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":fecha", $Fecha);
		$prepared->bindParam(":medicamento", $Medicamento);
		$prepared->bindParam(":cantidad", $Cantidad);
		$prepared->bindParam(":indicaciones", $Indicaciones);
		$prepared->execute();
	}

	public static function Paciente($id){
		$servidor = conexion::con();
		$query="select concat(NombrePaciente,' ',PrimerApallidoP,' ',SegundoApellidoP) as Nombre, IdPaciente,FechaNacimiento, Genero, SeguroPopular, TipoSangre
				from paciente
				where IdPaciente = :id";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":id", $id);
		$prepared->execute();
		$resultado = $prepared->fetch(PDO::FETCH_ASSOC);
		return $resultado;
	}

	public static function Listamedicamento(){
		$servidor = conexion::con();
		//$query = "select IdMedicamento, NombreGenerico, Presentacion from medicamentos";
		$query = "select IdMedicamento, NombreGenerico from medicamentos";
		$prepared = $servidor->prepare($query);
		$prepared->execute();
		$resultado = $prepared->fetchAll(PDO::FETCH_ASSOC);
		return $resultado;
	}

public static function AddCerMPaciente($CerM){
		$servidor = conexion::con();
		$query = "INSERT INTO certificadom VALUES (NULL, :IdEmpleado, :IdPaciente, :Fecha);";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":IdEmpleado",$CerM[0]);
		$prepared->bindParam(":IdPaciente",$CerM[1]);
		$prepared->bindParam(":Fecha",$CerM[2]);
		$prepared->execute();
	}

	public static function AddConsMPaciente($ConsM){
		$servidor = conexion::con();
		$query = "INSERT INTO constanciam VALUES (NULL, :IdEmpleado, :IdPaciente, :Fecha, :ConsultaM);";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":IdEmpleado",$ConsM[0]);
		$prepared->bindParam(":IdPaciente",$ConsM[1]);
		$prepared->bindParam(":Fecha",$ConsM[2]);
		$prepared->bindParam(":ConsultaM",$ConsM[3]);
		$prepared->execute();
	}

	public static function AddResMPaciente($ResC){
		$servidor = conexion::con();
		$query = "INSERT INTO resumenc VALUES (NULL, :IdEmpleado, :IdPaciente, :Fecha, :ConsultaC);";
		$prepared = $servidor->prepare($query);
		$prepared->bindParam(":IdEmpleado",$ResC[0]);
		$prepared->bindParam(":IdPaciente",$ResC[1]);
		$prepared->bindParam(":Fecha",$ResC[2]);
		$prepared->bindParam(":ConsultaC",$ResC[3]);
		$prepared->execute();
	}
}
/*
select * from recetaxmedicamento RM
inner JOIN recetamedica R
on RM.Folio = R.Folio
where R.Folio = (select a.Folio from recetamedica a WHERE a.Fecha = '2019-05-07');

con un for
insert into recetaxmedicamento value ((select a.Folio from recetamedica a WHERE a.Fecha = '2019-05-30 00:29:00'), 1,400, 'asdasd' )
*/

// que comando php oṕtengo la informacion del cuerpo de un paquete http
?>