<?php

class conexion{
        private $usuario = "root";
        private $password = "";
        private $service = "localhost";
        private $DB ="hospital";
        private static $prueba;
        private static $conexion=NULL;

        public static function con(){
                self::$prueba = new PDO("mysql:host=localhost; dbname=hospital","root",'' );
                return self::$prueba;
        }

        public static function conectar(){
                $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                self::$conexion= new PDO('mysql:host=localhost;dbname=hospital','root','',$pdo_options);
                //self::$conexion= new PDO('mysql:host=localhost;dbname=ventas;charset=utf8', 'root', '');
                return self::$conexion;
                }
}
?>