
<?php
$id4= isset($_GET['id']) ? $_GET['id'] : '';
require_once dirname(__DIR__).'../../config/conexion.php';

	
$db=conexion::conectar();
$select=$db->query("SELECT * FROM notamedica
INNER JOIN empleado on notamedica.IdEmpleado=empleado.IdEmpleado
INNER JOIN paciente on paciente.IdPaciente=notamedica.IdPaciente
INNER JOIN especialidad on empleado.IdEspecialidad=especialidad.IdEspecialidad
WHERE notamedica.IdNota='$id4'");           

?>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   
<body>

<div class="card" style="width: 25rem;">
  <!-- <img src="..." class="card-img-top" alt="..."> -->
  <div class="card-body">
    <h5 class="card-title text-center font-weight-bold">Nota Medica</h5>
<?php
$rows = $select->fetchAll(PDO::FETCH_ASSOC);
foreach ($rows as $row)
echo "<p class='text-center text-uppercase font-weight-light'>"."Dr.".$row["NombreEmpleado"]." ".$row["PrimerApellidoE"]." ".$row["SegundoApellidoE"]."</p>";
echo "<p class='text-center text-capitalize font-weight-light'>".$row["Especialidad"]."</p>";
echo "<p class='text-center text-capitalize'>"."Paciente. ".$row["NombrePaciente"]." ".$row["PrimerApallidoP"]." ".$row["SegundoApellidoP"]."</p>";
echo "<ul class='list-group list-group-flush'>";
echo "<li class='list-group-item'><table class='table'>
<tbody>
  <tr>
    
    <td><p class='text-center'>FC: ".$row["Frecuencia_Cardiaca"]."</p></td>
    <td><p class='text-center'>FR: ".$row["Frecuencia_Respiratoria"]."</p></td>
    <td><p class='text-center'>Temp: ".$row["Temperatura"]."</p></td>
    <td><p class='text-center'>Talla: ".$row["Talla"]."</p></td>
  </tr>
  <tr>
   
  <td><p class='text-center'>Peso: ".$row["Peso"]."</p></td>
  <td><p class='text-center'>Glucosa: ".$row["Glucosa"]."</p></td>
  <td></td>
  <td></td>
  </tr>
  
</tbody>
</table></li>";

echo "<p class='text-justify text-capitalize font-weight-bold'>".$row["Consulta"]."</p>"
?>
  
  </ul>
  <div class="card-body">
  <a href="#" class="card-link">Imprimir</a>
    <a href="#" class="card-link">Salir</a>
    
 </div>
</div>
</body>

</html>