<?php
session_start();

if ( !isset($_SESSION['inicio']) ){
    header("Location: ../../..");
}

$expediente = isset($_GET['Expediente']) ? $_GET['Expediente'] : '';

if (!isset($_COOKIE['expediente'])){
    setcookie('expediente', $expediente);
}

if (isset($_REQUEST['notas'])){
    header("Location: view_notas.php ");
}
if (isset($_REQUEST['Recetas'])){
    header("Location: view_recetaMedica.php");
}
if (isset($_REQUEST['Interconsulta'])){
    header("Location: view_interconsulta.php");
}
if (isset($_REQUEST['Reportes'])){
    header("Location: view_reportes.php");
}

if (isset($_REQUEST['lobby'])){
    header("Location: ../view_Medicos/view_doctor.php");
    setcookie('expediente', $expediente, time()-1000);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dbusqueda.css">
    <title>Expediente</title>

    <style>
        #frame {
            display: block;
            padding: 1% 1%;
            height: 80%;
        }
    </style>
</head>

<body onload="mueveReloj()">

    <div id="buscador">
        <div class="cabecera">
            <h2>Expediente clínico </h2>
        </div>
        <hr>
        <center>
            <div>
                <form method="post">
                    <h3 style="color: white"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h3>
                    <p>Crear:</p>
                    <button class="btn" id="nota" name="notas" value="notas">Nota medica</button>
                    <button class="btn" id="receta" name="Recetas" value="receta">Receta medica</button>
                    <button class="btn" id="interconsulta" name="Interconsulta" value="interconsulta">Interconsulta</button>
                    <button class="btn" id="reportes" name="Reportes" value="reportes">Reporte medico</button>
                    <button class="btn" id="lobby" name="lobby" value="lobby" style="margin: 50% 0%;">Volver</button>

                </form>
            </div>
        </center>
    </div>

</div>
<div style="display: flex;justify-content: space-between;">
    <img src="../../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()">
    <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
</div>
<hr> 

<div id="frame" class="modulos"> 
    <iframe src="view_historial.php?expediente=<?php echo $expediente ?>" style="width: 100%;height: 110%;" frameborder="0"></iframe>
</div>

</body>

<script src="../../../recursos/javascript/reloj.js"></script>
<script src="../../../recursos/javascript/slider.js"></script>

</html>