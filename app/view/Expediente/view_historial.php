<?php  
require_once '../../controller/controller_historial.php';
$Nexpediente = isset($_GET['expediente']) ? $_GET['expediente'] : '';
$Nexpediente = isset( $_COOKIE['expediente'] ) ? $_COOKIE['expediente'] : '';

    // filtrar la entrada del Nexpediente para evitar xss, csfx y sqlinyection
    // si no es valido la entrada, no ejecutar las function vista
$Expediente = new controller_historial();
$datosPaciente = $Expediente->paciente($Nexpediente);
$nacimiento = explode("-", $datosPaciente['FechaNacimiento']);
$edad = (date("Y") - $nacimiento[0]);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">

    <title>historial</title>
</head>
<style>

</style>

<body onload="request(<?php echo $_COOKIE['expediente'] ?>, '')">
    <div style="float: right;">
        <a href="view_informacion.php" target="__black"><img src="../../../recursos/iconos/icons8-help-25.png" alt="info"></a>
    </div>
    <div style="display: flex;justify-content: space-around;">
        <h3>Paciente: <span><?php echo $datosPaciente['Nombre']?></span> - Número Expediente: <span><?php echo $Nexpediente ?></span> - edad: <span><?php echo $edad?></span></h3>
    </div>

    <div>
        <label for="filtro">Filtrar por</label>
        <input type="date" name="filtro" id="date">
        <button class="btn-primary" onclick="buscar(<?php echo $_COOKIE['expediente'] ?>)"><img src="../../../recursos/iconos/icons8-search-25.png" alt="info"></button>
        <button class="btn-secundary" onclick="request(<?php echo $_COOKIE['expediente'] ?>, '')"><img src="../../../recursos/iconos/icons8-synchronize-25.png" alt="info"></button>
    </div>

    <div class="row">
        <noscript>
            <center>
                <h1>Habilitar javascript para una mejor experiencia</h1>
            </center>
        </noscript>
        <div class="col-lg-12 text-center">
            <div class="section-title">
                <div class="contenido-expediente">

                    <center>
                        <div class="table-responsive">

                            <table class="table" border="1" text-align="center">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Contenido</th>
                                        <th>Medico</th>
                                        <th>Especialidad</th>
                                    </tr>
                                </thead>

                                <tbody name="result">

                                </tbody>
                            </table>
                            <!-- Limite de la tabla que se trabajara, hasta parar -->
                            
                        </div>
                    </center>

                </div>
            </div>
        </div>
    </div>

</body>

<script src="../../../recursos/javascript/expediente.js"></script>

<script>
    function ventanaSecundaria(tipoVentana, id) {
        window.open("view_" + tipoVentana + ".php?id="+id, tipoVentana, "width=500,height=500");
    } 
</script>

</html>