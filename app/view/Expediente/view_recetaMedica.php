<?php
include_once "../../controller/controller_expediente.php";
session_start();

$paciente = controller_expediente::paciente($_COOKIE['expediente']);
$Medicamento = controller_expediente::medicamento();
$nacimiento = explode("-", $paciente['FechaNacimiento']);
$edad = (date("Y") - $nacimiento[0]);

if (isset($_REQUEST['notas'])){
    header("Location: view_notas.php ");
}
if (isset($_REQUEST['Interconsulta'])){
    header("Location: view_interconsulta.php ");
}
if (isset($_REQUEST['Reportes'])){
    header("Location: view_reportes.php");
}
if (isset($_REQUEST['expediente'])){
    header("Location: view_expediente.php");
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dbusqueda.css">
    <title>Receta medica</title>

    <style>
        #receta{
            display: flex;
            flex-direction: column;
            overflow: auto;
            padding: 0% 1% 0%;
        }

        .Datos_paciente{
            display: flex;
            overflow: auto;
            clear: left;
            justify-content: space-between;
        }

    </style>
</head>

<body onload=" mueveReloj()">

    <div id="buscador">
        <div class="cabecera">
            <h2>Receta medica</h2>
        </div>
        <hr>
        <center>
            <div>
                <form method="post">
                    <h3 style="color: white"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h3>
                    <p>Crear:</p>
                    <button class="btn" id="nota" name="notas" value="notas">Nota medica</button>
                    <a focus="true" class="btn">Receta</a>
                    <button class="btn" id="interconsulta" name="Interconsulta"
                    value="interconsulta">Interconsulta</button>
                    <button class="btn" id="reportes" name="Reportes" value="reportes">Reporte medico</button>
                    
                    <button class="btn" id="expediente" name="expediente" value="expediente" style="margin: 50% 0%;">Volver al expediente</button>

                </form>
            </div>
        </center>
    </div>

    <div style="display: flex;justify-content: space-between;">
        <img src="../../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>

    <hr>

    <div id="receta" class="modulos">
        <center><h3>Formato único de receta medica</h3></center>
        <hr>
        <div class="Datos_institucion">
            <center>
                <label for="UM">Hospital General de Chetumal</label>
                <label for="municipio">Othon P. Blanco</label>
                <span><label for="servicio">Servicio: Medico</label></span>
            </center>
        </div>
        <hr>
        <h4>
            <div class="Datos_paciente">
                <label for="paciente">Paciente: <?php echo $paciente['Nombre']; ?></label>
                <label>|</label>
                <label for="edad">Edad: <?php echo $edad ?></label>
                <label>|</label>
                <label for="sexo">Genero: <?php echo $paciente['Genero']; ?></label>
                <label>|</label>
                <label for="nsp">SP: <?php echo $paciente['SeguroPopular']; ?></label>
            </div>
        </h4>
        <hr>

        <form method="post" name="recetaM">
            <input type="hidden" name="option" value="">
            <input type="hidden" name="fecha" value="">
            <input type="hidden" name="idPaciente" value="">
            <input type="hidden" name="IdEmpleado" value="">
            <input type="hidden" name="edad" value="">

            <div id="medicamentos" class="Datos_medicamentos">
                <table class="table" name="tabla_receta">
                    <thead>
                        <tr>
                            <th>Medicamento (nombre generico)</th>
                            <th>Presentación</th>
                            <th>cantidad</th>
                            <th>Indicaciones de administración</th>
                            <th><img src="../../../recursos/iconos/icons8-plus-26.png" onclick="add()" alt="Agregar Medicamento"></th>
                        </tr>
                    </thead>
                    <tbody name="cuerpo">

                        <tr>
                            <td>
                                <select name="Medicamento" class="NombreM select-css">
                                    <?php 
                                    foreach ($Medicamento as $value) {                
                                        echo "<option value={$value['IdMedicamento']}>{$value['NombreGenerico']}</option>";
                                    }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <select name="Presentacion" class="presentacionM select-css presentacionM">
                                    <option value="jarabe">jarabe</option>
                                    <option value="pastillas">pastillas</option>
                                    <option value="Inyectable">Inyectable</option>
                                    <option value="ungüento">ungüento</option>
                            </select>
                        </td>

                        <td><input type="text" name="cantidad" class="cantidad" required></td>
                        <td><textarea name="Indicaciones" cols="40" rows="3" class="indicacion" required></textarea></td>
                        <td><img src="../../../recursos/iconos/icons8-waste-24.png" onclick="eliminarMedicamento()" alt="Eliminar Medicamento"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="Datos_diafnostico">
            <div class="form-group">
                <textarea name="diagnostico" cols="120" rows="2" placeholder="Diagnostico" id='diag' required></textarea>
            </div>
            <input class="button" type="button" onclick="datos()" value="Guardar">
            <input class="button2" type="reset" >
        </div>

    </form>
</div>
</body>

<script src="../../../recursos/javascript/receta.js"></script>
<script src="../../../recursos/javascript/reloj.js"></script>
<script src="../../../recursos/javascript/slider.js"></script>

<script>
    (function(){

        momentoActual = new Date();
        fecha = momentoActual.getFullYear()+"-"+(momentoActual.getMonth()+1)+"-"+momentoActual.getDate();
        hora = momentoActual.getHours()+":"+momentoActual.getMinutes()+":"+momentoActual.getSeconds();

        document.recetaM[0].value = "Receta";
        document.recetaM[1].value = fecha+" "+hora;
        document.recetaM[2].value = "<?php echo $_COOKIE['expediente']; ?>";
        document.recetaM[3].value = "<?php echo $_SESSION['inicio'][0]; ?>";
        document.recetaM[4].value = "<?php echo $edad ?>";

    })();

    function add(){
        tabla = document.querySelector("table");

        if (tabla.rows.length < 6){
            copiar = tabla.rows[1].cloneNode(true);
            tabla.tBodies.cuerpo.appendChild(copiar);
        }
    }

    function eliminarMedicamento() {
        tabla = document.querySelector("table");
        total = tabla.rows.length;

        if(total > 2){
            tabla.deleteRow(total-1);
        }
    }
</script>

</html>