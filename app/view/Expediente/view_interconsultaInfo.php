<?php
$id4= isset($_GET['id']) ? $_GET['id'] : '';
require_once dirname(__DIR__).'../../config/conexion.php';

	
$db=conexion::conectar();
$select=$db->query("SELECT * FROM interconsulta INNER JOIN paciente ON paciente.IdPaciente=interconsulta.IdPaciente INNER JOIN empleado on empleado.IdEmpleado=interconsulta.IdEmpleado inner join especialidad on especialidad.idespecialidad= empleado.idEspecialidad WHERE interconsulta.IdInterconsulta='$id4'");           
$rows = $select->fetchAll(PDO::FETCH_ASSOC);
foreach ($rows as $row)

$NombreCompletoPacinete = $row["NombrePaciente"]." ".$row["PrimerApallidoP"]." ".$row["SegundoApellidoP"];
$NombreCompletoEmpleado = $row["NombreEmpleado"]." ".$row["PrimerApellidoE"]." ".$row["SegundoApellidoE"];
$especialidad =$row["Especialidad"];
$genero=$row["Genero"];
$Fecha = $row["Fecha"];
$Observas = $row["Observaciones"];

?>


<style>
.card{
    margin: 0 auto;
    float: none;
    margin-bottom:10px;
}
</style>


<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   
<body>
<div class="card align-middle"  style="width: 43rem;">
  <div class="card-body">
  <h5 class="card-title text-center font-weight-bold">HOSPITAL GENERAL DE CHETUMAL</h5>
  <h5 class="card-title text-center font-weight-bold">INTERCONSULTA</h5>

  <p class="font-weight-bold text-capitalize">SERVICIO: CONSULTA EXTERNA</p>
  <p class="font-weight-bold text-capitalize">SERVICIO AL QUE SE INTERCONSULTA: <?php echo $especialidad;?></p>
  <p class="font-weight-bold text-capitalize">NOMBRE DEL PACIENTE:   <?php echo $NombreCompletoPacinete; ?></p>
  <p class="font-weight-bold text-capitalize">FECHA DE NACIMIENTO: <?php echo $Fecha; ?> <br> SEXO:<?php echo $genero ?> <br> DIAGNOSTICO: <?php echo $Observas; ?></p>
 
  <p class="font-weight-bold text-capitalize">MÉDICO INTERCONSULTANTE: <?php echo $NombreCompletoEmpleado; ?></p>
  

  <table class="table table-sm">
 
  <tbody>
    <tr>
      
      <td><p class="font-weight-bold text-capitalize">ORDINARIO ()</p></td>
      <td><p class="font-weight-bold text-capitalize">URGENTE ()</p></td>
      <td></td>

      <td><p class="font-weight-bold text-capitalize">FECHA: <?php echo $Fecha; ?></p></td>

    </tr>
  </tbody>
</table>
  </div>
</div>
</body>
</html>