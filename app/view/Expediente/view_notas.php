<?php
include_once "../../controller/controller_expediente.php";

session_start();

if (isset($_REQUEST['Recetas'])){
    header("Location: view_recetaMedica.php");
}
if (isset($_REQUEST['Interconsulta'])){
    header("Location: view_interconsulta.php");
}
if (isset($_REQUEST['Reportes'])){
    header("Location: view_reportes.php");
}
if (isset($_REQUEST['expediente'])){
    header("Location: view_expediente.php");
}

$paciente = controller_expediente::paciente($_COOKIE['expediente']);

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dbusqueda.css">
    <title>Nota medica</title>

    <style>
        #word{
            padding: 1% 1%;
            display: flex;
            flex-direction: column;
        }

    </style>
</head>

<body onload=" mueveReloj()">

    <div id="buscador">
        <div class="cabecera">
            <h2>Nota</h2>
        </div>
        <hr>
        <center>
            <div>
                <form method="post">
                    <h3 style="color: white"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h3>
                    <p>Crear:</p>
                    <a focus="true" class="btn">Nota medica</a>
                    <button class="btn" id="receta" name="Recetas" value="receta">Receta medica</button>
                    <button class="btn" id="interconsulta" name="Interconsulta" value="interconsulta">Interconsulta</button>
                    <button class="btn" id="reportes" name="Reportes" value="reportes">Reporte medico</button>
                    <button class="btn" id="expediente" name="expediente" value="expediente" style="margin: 50% 0%;">Volver al expediente</button>
                </form>
            </div>
        </center>
    </div>

    </div>
    <div style="display: flex;justify-content: space-between;">
        <img src="../../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>
    <hr>

    <div id="word">
        <form method="POST" action="../../controller/controller_AddHojasExpedientes.php" name="Nota">
            <input type="hidden" name="option" value="">
            <input type="hidden" name="fecha" value="">
            <input type="hidden" name="idPaciente" value="">
            <input type="hidden" name="IdEmpleado" value="">
            <div class="Datos_institucion">
        </div>
        <h4>
            <div class="Datos_paciente">
                <label for="paciente">Paciente: <?php echo $paciente['Nombre']; ?></label>
               
            </div>
        </h4>
            <div>
            <table class="table">
            
            <tbody>
            <tr>
            <td>
                <label for="fc">Fc:</label>
                <input type="text" name="fc" required>

            </td>
            <td>
            <label for="fr">Fr:</label>
                <input type="text" name="fr" required>

            </td>
            <td>  
                <label for="ta">Ta:</label>
                <input type="text" name="ta" required>
            </td>
            <td> <label for="temp">Temp:</label>
                <input type="text" name="temp" required>
               </td>
            </tr>
            <tr>
            
               <td><label for="talla">Talla:</label>
                <input type="text" name="talla" required>
            </td>
        <td> <label for="peso">Peso:</label>
                <input type="text" name="peso" required>
        </td>
        <td>
        <label for="dx">DX:</label>
                <input type="text" name="dx" required>  
           
        </td>
        <td></td>
            </tr>
            </tbody>
            
            </table>
                
              
               
            
               
              </div>

            
            <div class="form-group">
            <center>
            <h3>Observaciones</h3>
            </center>
            <textarea class="form-control" name="Consulta" rows="3" placeholder="Consulta medica" required></textarea>
            </div>

            <input class="button" type="submit" value="Guardar">
        </form>
    </div>

    </body>

    <footer>
    <center>
                <label for="UM">Hospital General de Chetumal</label>
                <label for="municipio">Othon P. Blanco</label>
                <span><label for="servicio">Servicio: Medico</label></span>
            </center>
    </footer>
    <!-- <script src="../../recursos/javascript/doctor.js"></script> -->
    <script src="../../../recursos/javascript/reloj.js"></script>
    <script src="../../../recursos/javascript/slider.js"></script>


    <script>
        (function(){
            momentoActual = new Date();
            fecha = momentoActual.getFullYear()+"-"+(momentoActual.getMonth()+1)+"-"+momentoActual.getDate();
            hora = momentoActual.getHours()+":"+momentoActual.getMinutes()+":"+momentoActual.getSeconds();
            document.Nota[0].value= "Nota";
            document.Nota[1].value= fecha+" "+hora;
            document.Nota[2].value= "<?php echo $_COOKIE['expediente']; ?>";
            document.Nota[3].value= "<?php echo $_SESSION['inicio'][0]; ?>";

        })();
    </script>

</html>