<?php
session_start();
require_once '../../controller/controller_historial.php';

if ( !isset($_SESSION['inicio']) ){
    header("Location: ../../..");
}

$Expediente = new controller_historial();
$datosPaciente = $Expediente->paciente($_COOKIE['expediente']);
$contacto = $Expediente->contacto($_COOKIE['expediente']);

$nacimiento = explode("-", $datosPaciente['FechaNacimiento']);
$edad = (date("Y") - $nacimiento[0]);

$estado;
$color;
if ($datosPaciente['Activo']){
    $estado = "Activo";
    $color= "green";
}else{
    $estado = "fallecido";
    $color = "red";

}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dbusqueda.css">
    <title>Información</title>
    <style>
        p{
            color: black;
        }
        span{
            color: #999;
            text-decoration: underline;
        }
        h3{
            text-decoration: underline;
        }
        #generales{
            display: flex;flex-direction: row;
            justify-content: space-between;
        }
        #avanzados{
            display: flex;
            justify-content: space-around;
        }

        
    </style>
</head>

<body>
    <div>
        <center><h2>Datos personales del paciente <h2> </center>
        <hr>
        <div id="generales">
            <p>Estado: <span style="color:<?php echo $color; ?>"><?php echo $estado ?></span></p>
            
            <p>Nombre: <span><?php echo $datosPaciente['Nombre']; ?></span></p>
            <p>Edad: <span><?php echo $edad ?></span></p>
            <p>Fecha de nacimiento: <span><?php echo $datosPaciente['FechaNacimiento']; ?></span></p>
            <p>CURP: <span><?php echo $datosPaciente['Curp']; ?></span></p>
            <p><span></span></p>
        </div>
        <hr>
        <div  id="avanzados">
            <div>
                <center><h3>Datos medicos</h3></center>
                <p>N° de expediente <span><?php echo $_COOKIE['expediente']; ?></span></p>
                <p>Fecha de afiliacion <span><?php echo $datosPaciente['FechaAfiliacion']; ?></span></p>
                <p>Tipo de sangre <span><?php echo $datosPaciente['TipoSangre']; ?></span></p>
                <p>Seguro Popular <span><?php echo $datosPaciente['SeguroPopular']; ?></span></p>
                <p>Genero <span><?php echo $datosPaciente['Genero']; ?></span></p>
            </div>

            <div>
                <center><h3>Datos de domicilio</h3></center>
                <p>Calle <span><?php echo $datosPaciente['Calle']; ?></span></p>
                <p>Lote <span><?php echo $datosPaciente['NumInt']; ?></span> Manzana <span><?php echo $datosPaciente['NumExt']; ?></span></p>
                <p>Codigo postal <span><?php echo $datosPaciente['CP']; ?></span></p>
                <p>Colonia <span><?php echo $datosPaciente['Colonia']; ?></span></p>
                <p>Ciudad <span><?php echo $datosPaciente['Ciudad']; ?></span></p>
                <p>Municipio <span><?php echo $datosPaciente['Municipio']; ?></span></p>
            </div>
            <div>
                <center><h3>Datos de contacto</h3></center>
                <?php 
                    if(count($contacto) == 0){
                        echo "<h4>No se cuenta con ningun Numero</h4>";
                    }else{
                        foreach ($contacto as $value) {
                            echo "<p>Telefono: <span>{$value['Telefono']}</span> tipo: <span>{$value['TipoTelefono']}</span></p>";
                        }
                    }
                ?>
            </div>
        </div>
    </div>


</body>

<script>
    (function(){
        document.title = "Información de <?php echo $datosPaciente['Nombre']; ?>";
    })();
</script>
</html>