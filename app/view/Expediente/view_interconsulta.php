<?php
include_once "../../controller/controller_expediente.php";
session_start();

$especialidad3 = isset($_POST['IdEmpleado']) ? $_POST['IdEmpleado'] : '';

setcookie('especialidad');
require_once '../../controller/pacientes.php';

//echo date('Y-d-m h:i:s');
$pacienter= $_COOKIE['expediente']; 

$model = new pacientes();
$especialidad = new especialidad();
$doc = new doctor();
$idd;
$especialidad2 ;
$IdEmp;
$IdP;
$Fecha;
$Obser;

if (isset($_REQUEST['notas'])){
	header("Location: view_notas.php ");
}
if (isset($_REQUEST['Recetas'])){
	header("Location: view_recetaMedica.php");
}
if (isset($_REQUEST['Reportes'])){
    header("Location: view_reportes.php");
}
if (isset($_REQUEST['expediente'])){
	header("Location: view_expediente.php");
}

$paciente = controller_expediente::paciente($_COOKIE['expediente']);

?>

<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
	<link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">
	<link rel="stylesheet" href="../../../recursos/estilos/doctores/Dbusqueda.css">

	<h2>Interconsulta</h2>

</head>
<style>

</style>

<body onload="mueveReloj()">
	<div id="buscador">
		<div class="cabecera">


		</div>
		<hr>
		<center>
			<div>
				<form method="post">
					<h3 style="color: white"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h3>
					<p>Crear:</p>
					<button class="btn" id="nota" name="notas" value="notas">Nota medica</button>
					<button class="btn" id="receta" name="Recetas" value="receta">Receta medica</button>
					<a focus="true" class="btn">Interconsulta</a>
					<button class="btn" id="reportes" name="Reportes" value="reportes">Reporte medico</button>
					
					<button class="btn" id="expediente" name="expediente" value="expediente" style="margin: 50% 0%;">Volver al expediente</button>

				</form>
			</div>
		</center>
	</div>
	<center>
                <label for="UM">Hospital General de Chetumal</label>
                <label for="municipio">Othon P. Blanco</label>
                <span><label for="servicio">Servicio: Medico</label></span>
            </center>
        </div>
       

	<div>
		<!-- <form action='../../controller/crud.php' method='POST' name="inter"> -->

			<form action='../../controller/crud.php' method='POST' name="datos">
				<div align="center">
					<br><br>
					<h4>
					<?php foreach($model->listar($pacienter) as $r): ?>
						
						<label for="nombre">Paciente:   <?php echo $r->NombrePaciente; ?>  </label>
						<label for="PA"> <?php echo $r->PrimerApallidoP;?>  </label>
						<label for="SA">  <?php echo $r->SegundoApellidoP;?>  </label>
					<?php endforeach; ?>
					</h4>
					<br><br>

				</div>
			</form>

			<form action="view_interconsulta.php" method='POST' name="inter">
				<div align="center">
					<label for="a">Especialidad</label>
					<select align="letf" name="IdEmpleado" id="tipos" class="select-css" onchange="doctores(document.querySelector('select').value)">
						<?php foreach($especialidad->listar() as $n): ?>
							<?php  $ide = $n->IdEspecialidad; ?>
							<option value="<?php echo $ide ?>" ><?php echo $n->Especialidad;?> </option>

						<?php endforeach; ?> 
						

					</select>
					<input class="button" type='submit' value='buscar doctor'  >
			</form>

			<form action='../../controller/crud.php' method='POST' name="inter">
				<input type="hidden" name="idPaciente" value="<?php echo $_COOKIE['expediente']; ?>">

					<label for="a">Doctor</label>

					<select align="letf" name="a" id="tipos" class="select-css" >
						<?php foreach($doc->listar( $especialidad3) as $l): ?>
							<?php  $idd = $l->IdEmpleado; ?>
							<option value="<?php echo $idd ?>" ><?php echo $l->NombreEmpleado;?> <?php echo $l->PrimerApellidoE;?> <?php echo $l->SegundoApellidoE;?> </option>

						<?php endforeach; ?> 
					</select>           
				</div>
				<br><br>
				<div align="center">
					<p>Causas/Observaciones</p>
					<textarea for ="obs" name="Observaciones" class="estilotextarea3" rows="10" cols="40"  ></textarea>
					<br>
					<a href="view_interconsulta.php"><input class="button"  type='submit' value='Guardar'  ></a>

				</div>

			</form>
		</div>

	</body>
	<script src="../../recursos/javascript/doctor.js"></script>
	<script src="../../recursos/javascript/reloj.js"></script>
	<script>


		(function(){
			console.log(document.querySelector("select").value);
		})();
		function doctores(id){
	//console.log(id);
	//<? $_COOKIE['especialidad']?>
}
</script>
<?php 

?>

</html>