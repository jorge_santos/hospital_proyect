<?php
 include_once "../../controller/controller_expediente.php";
 include_once  "../../controller/controller_AddHojasExpedientes.php";
 //require_once '../../config/conexion.php';

 //needed in the file of print php that i will use top print the file
 require_once '../../config/conexion2.php';
 require '../../../recursos/TCPDF-master/tcpdf.php';
    session_start();
    
    $paciente = controller_expediente::paciente($_COOKIE['expediente']);
    $Medicamento = controller_expediente::medicamento();
    $nacimiento = explode("-", $paciente['FechaNacimiento']);
    $edad = (date("Y") - $nacimiento[0]);
    $var= $_SESSION['inicio'];
    //var_dump($var);
    //echo var_dump($paciente);

if (isset($_REQUEST['notas'])){
    header("Location: view_notas.php ");
}
if (isset($_REQUEST['Recetas'])){
    header("Location: view_recetaMedica.php");
}
if (isset($_REQUEST['Interconsulta'])){
    header("Location: view_interconsulta.php");
}
if (isset($_REQUEST['expediente'])){
    header("Location: view_expediente.php");
}
if (isset($_REQUEST['Reportes'])){
    header("Location: view_reportes.php");
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dcitas.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dbusqueda.css">
    <link rel=" stylesheet" href="../../../recursos/estilos/doctores/Dexpedientes.css">
    <title>Nota medica</title>

    <style>
        #word{
            padding: 1% 1%;
            display: flex;
            flex-direction: column;
        }

        .bold { font-weight: bold; }
        .uppercase { text-transform: uppercase; }
    </style>
</head>

<body onload=" mueveReloj()">

    <div id="buscador">
        <div class="cabecera">
            <h2>Nota</h2>
        </div>
        <hr>
        <center>
            <div>
                <form method="post">
                    <h3 style="color: white"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h3>
                    <p>Crear:</p>
                    <button class="btn" id="nota" name="notas" value="notas">Nota medica</button>
                    <button class="btn" id="receta" name="Recetas" value="receta">Receta medica</button>
                    <button class="btn" id="interconsulta" name="Interconsulta" value="interconsulta">Interconsulta</button>
                    <button class="btn" id="reportes" name="Reportes" value="reportes">Reporte medico</button>
                    <button class="btn" id="expediente" name="expediente" value="expediente" style="margin: 50% 0%;">Volver al expediente</button>
                </form>
            </div>
        </center>
    </div>

    </div>
    <div style="display: flex;justify-content: space-between;">
        <img src="../../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>
    <hr>


 <div id="Dcitas" class="modulos">
        <label for="a">Buscar por</label>
        <select name="a" id="tipos" onchange="typeR(document.getElementsByClassName('table-responsive'))">
            <option>elija una opcion</option>
            <option value="res">Resumen Clínico</option>
            <option value="cons">Constancia Médica</option>
            <option value="cer">Certificado Médico</option>
        </select>
        <hr>

        <!-- Div de resumen clinico   -->

  <div id="Bres" class="tras word">
        <form method="POST"  action="../../controller/controller_AddHojasExpedientes.php" name="ResC">
             <form method="POST" formaction="../../../app/view/Expediente/pdf.php">
            
            <input type="hidden" name="option" value="">
            <input type="hidden" name="fecha" value="">
            <input type="hidden" name="idPaciente" value="">
            <input type="hidden" name="IdEmpleado" value="">

               <!--  DOC------
                //nombre del doc
                //Puesto
                //ced prof
                //Paciente-----
                //nombre del paciente
                //sexo
                // edad                
                //fecha de hoy -->
           <!--   <input type="" name="option2" value="">
            <input type="" name="fecha2" value="">
            <input type="" name="idPaciente2" value=""> -->
            <input type="hidden" name="nomDoc" value=" <?php echo $_SESSION['inicio'][1]; ?>">
            <input type="hidden" name="puestoDoc" value="<?php echo $_SESSION['inicio'][2]; ?>">
            <input type="hidden" name="cedDoc" value="<?php echo $_SESSION['inicio'][3]; ?>">
            <input type="hidden" name="nomPac" value="<?php echo $paciente['Nombre']; ?>">
            <input type="hidden" name="sexPac" value="<?php echo $paciente['Genero']; ?>">
            <input type="hidden" name="edadPac" value="<?php echo $edad ?>">
            <input type="hidden" id="consD" name="consD" value="">
             <div>
                <h6 class="uppercase bold">A QUIEN CORRESPONDA:</h6>
                <br>
                <label>EL/LA QUE SUSCRIBE </label>
                
                <label  class="uppercase bold" type="text">C. DR/A.</label>
                 <label class="uppercase bold"  type="text" name="nom_Doc" id="nom_Doc" >  <?php echo $_SESSION['inicio'][1]; ?></label>
                 <label  type="text" name="puesto_Doc" id="puesto_Doc" disabled value="<?php echo $_SESSION['inicio'][2]; ?>,"> </label>
                <label class="uppercase" type="text" name="puesto_Doc" id="puesto_Doc"> <?php echo $_SESSION['inicio'][2]; ?></label> <label>, ADSCRITO AL HOSPITAL GENERAL DE CHETUMAL, DEPENDIENTE DE</label>
                <label>LOS SERVICIOS ESTATALES DE SALUD,</label>
                <label > CON CED. PROF. </label>
                <label class="uppercase" type="text" name="ced_Doc" id="ced_Doc"> <?php echo $_SESSION['inicio'][3]; ?></label> 
                <label> Y CON AUTORIZACIÓN </label>
                <label>PARA EJERCICIO DE SU PROFESIÓN: </label>
                <br>
                <br>
                <label>SE TRATA DE(L)/ LA. </label>
                <label class="uppercase bold" type="text" name="nom_Pac" id="nom_Pac">C. <?php echo $paciente['Nombre']; ?></label>

                <label>, DE SEXO </label>
                <label class="uppercase" type="text" name="sex_Pac" id="sex_Pac"><?php echo $paciente['Genero']; ?> </label>

                <label >DE</label>
                <label type="text" name="edad_Pac" id="edad_Pac"><?php echo $edad ?></label>
                <label> AÑOS DE EDAD, </label>
                <div class="form-group">
                <textarea class="form-control" id="RES" name="ConsultaC" rows="3" placeholder="Detallas del Paciente Ej. , QUIEN  ACUDE A...." value="<?php  if(isset($_POST['ConsultaC'])){ echo htmlentities($_POST['ConsultaC']); } ?>" ></textarea>
                </div>
                <br>
                <label >SE EXTIENDE LA PRESENTE PARA FINES QUE AL INTERESADO CONVENGAN EN LA CIUDAD DE CHETUMAL, CAPITAL DEL ESTADO DE QUINTANA ROO 
                    <label type='date' name="fecha" id="fecha" > <?php echo date('d-M-Y');?>.</label>

            </div>
             <button class=" btn-primary btn-sm">Guardar</button>
          <!--   <button method="POST" type="submit" formaction="../../../app/view/Expediente/pdf.php" name="pdf_report" id="pdf_report" class=" btn-secondary btn-sm" ><i class="fas fa-file-pdf"></i>Imprimir</button> -->
            </form>
        </form>
    </div>
        <!-- Div de Constanica Medica -->

    <div id="Bcons" class="tras word">
        <form method="POST" action="../../controller/controller_AddHojasExpedientes.php" name="ConsM">
             <form method="POST" formaction="../../../app/view/Expediente/pdf.php" >

            <input type="hidden" name="option" value="">
            <input type="hidden" name="fecha" value=""> 
            <input type="hidden" name="idPaciente" value="">
            <input type="hidden" name="IdEmpleado" value="">
            <input type="hidden" name="pdf_report" id="pdf_report">

            <!--  DOC------
                //nombre del doc
                //Puesto
                //ced prof
                //Paciente-----
                //nombre del paciente
                //sexo
                // edad                
                //fecha de hoy -->
           <!--   <input type="" name="option2" value="">
            <input type="" name="fecha2" value="">
            <input type="" name="idPaciente2" value=""> -->
            <input type="hidden" name="nomDoc" value=" <?php echo $_SESSION['inicio'][1]; ?>">
            <input type="hidden" name="puestoDoc" value="<?php echo $_SESSION['inicio'][2]; ?>">
            <input type="hidden" name="cedDoc" value="<?php echo $_SESSION['inicio'][3]; ?>">
            <input type="hidden" name="nomPac" value="<?php echo $paciente['Nombre']; ?>">
            <input type="hidden" name="sexPac" value="<?php echo $paciente['Genero']; ?>">
            <input type="hidden" name="edadPac" value="<?php echo $edad ?>">

             <div>
                 <h6 class="uppercase bold">A QUIEN CORRESPONDA:</h6>
                 <br>
                <label>EL/LA QUE SUSCRIBE </label>

                
                <label  class="uppercase bold" type="text">C. DR/A.</label>
                 <label class="uppercase bold"  type="text" name="nom_Doc" id="nom_Doc" >  <?php echo $_SESSION['inicio'][1]; ?></label>
                <label class="uppercase" type="text" name="puesto_Doc" id="puesto_Doc"> <?php echo $_SESSION['inicio'][2]; ?></label> <label>, ADSCRITO AL HOSPITAL GENERAL DE CHETUMAL, DEPENDIENTE DE</label>
                <label>LOS SERVICIOS ESTATALES DE SALUD,</label>
                <label > CON CED. PROF. </label>
                <label class="uppercase" type="text" name="ced_Doc" id="ced_Doc"> <?php echo $_SESSION['inicio'][3]; ?></label> 
                <label> Y CON AUTORIZACIÓN </label>
                <label>PARA EJERCICIO DE SU PROFESIÓN: </label>
                <br>
                <br>
                <label>SE TRATA DE(L)/ LA. </label>
                <label class="uppercase bold" type="text" name="nom_Pac" id="nom_Pac">C. <?php echo $paciente['Nombre']; ?></label>

                <label>, DE SEXO </label>
                <label class="uppercase" type="text" name="sex_Pac" id="sex_Pac"><?php echo $paciente['Genero']; ?> </label>

                <label >DE</label>
                <label type="text" name="edad_Pac" id="edad_Pac"><?php echo $edad ?></label>
                <label> AÑOS DE EDAD, </label>

               <div class="form-group">
                <textarea class="form-control" name="ConsultaM" rows="3" placeholder="Detallas del Paciente Ej. , CURSA CON LOS DIAGNÓSTICOS" value="<?php  if(isset($_POST['ConsultaM'])){ echo htmlentities($_POST['ConsultaM']); } ?>" ></textarea>
                </div>
                <br>
                 <label >SE EXTIENDE LA PRESENTE PARA FINES QUE AL INTERESADO CONVENGAN EN LA CIUDAD DE CHETUMAL, CAPITAL DEL ESTADO DE QUINTANA ROO 
                    <label type='date' name="fecha" id="fecha" > <?php echo date('d-M-Y');?>.</label>

            </div>
             <button  type="submit"   class=" btn-primary btn-sm">Guardar</button>
             
           <!--  <button method="POST" type="submit" name="pdf_report" id="pdf_report" formaction="../../../app/view/Expediente/pdf.php" class=" btn-secondary btn-sm" ><i class="fas fa-file-pdf"></i>Imprimir</button> -->
        </form>
        </form>
    </div>

     <!--  Div de Certificado Medico-->
              <div id="Bcer"  class="tras word">
        <form method="POST" action="../../controller/controller_AddHojasExpedientes.php" name="CerM">
            <form method="POST" formaction="../../../app/view/Expediente/pdf.php">
            
            <input type="hidden" name="option" value="">
            <input type="hidden" name="fecha" value="">
            <input type="hidden" name="idPaciente" value="">
            <input type="hidden" name="IdEmpleado" value="">

<!--  DOC------
                //nombre del doc
                //Puesto
                //ced prof
                //Paciente-----
                //nombre del paciente
                //sexo
                // edad                
                //Tipo de sangre
                //fecha de hoy -->
           <!--   <input type="" name="option2" value="">
            <input type="" name="fecha2" value="">
            <input type="" name="idPaciente2" value=""> -->
            <input type="hidden" name="nomDoc" value="">
            <input type="hidden" name="puestoDoc" value="">
            <input type="hidden" name="cedDoc" value="">
            <input type="hidden" name="nomPac" value="">
            <input type="hidden" name="sexPac" value="">
            <input type="hidden" name="edadPac" value="">
            <input type="hidden" name="SangrePac" value="">

            
            <!-- <input formaction="../../../app/view/Expediente/pdf.php" type="" name="option2" value=""> -->

            
            <div>
                <h6 class="uppercase bold">A QUIEN CORRESPONDA:</h6>

                <label>EL/LA QUE SUSCRIBE </label>

                
                <label  class="uppercase bold" type="text">C. DR/A.</label>
                 <label class="uppercase bold"  type="text" name="nom_Doc" id="nom_Doc" >  <?php echo $_SESSION['inicio'][1]; ?></label>
                 <label  type="text" name="puesto_Doc" id="puesto_Doc" disabled value="<?php echo $_SESSION['inicio'][2]; ?>,"> </label>
                <label class="uppercase" type="text" name="puesto_Doc" id="puesto_Doc"> <?php echo $_SESSION['inicio'][2]; ?></label> <label>, ADSCRITO AL HOSPITAL GENERAL DE CHETUMAL, DEPENDIENTE DE</label>
                <label>LOS SERVICIOS ESTATALES DE SALUD,</label>
                <label > CON CED. PROF. </label> <label class="uppercase" type="text" name="ced_Doc" id="ced_Doc"> <?php echo $_SESSION['inicio'][3]; ?></label>
                <label>Y CON AUTORIZACIÓN </label>
                <label>PARA EJERCICIO DE SU PROFESIÓN</label>

                <h3 align="center">HACE CONSTAR</h3>

                <label>QUE EL/ LA</label> <label class="uppercase bold">C.</label>
                <label class="uppercase bold" type="text" name="nom_Pac" id="nom_Pac"> <?php echo $paciente['Nombre']; ?></label>

                <label>DE SEXO, </label>
                <label type="text" name="sex_Pac" id="sex_Pac"> <?php echo $paciente['Genero']; ?></label>

                <label >DE</label>
                <label type="text" name="edad_Pac" id="edad_Pac"><?php echo $edad ?></label>
                <label>AÑOS DE EDAD, SE LE REALIZÓ EXAMEN MÉDICO RESULTANDO CLÍNICAMENTE</label>
                <label> SANO CON TIPO DE SANGRE </label>
                <label class="uppercase bold" type="text" name="sangre_Pac" id="sangre_Pac"><?php echo $paciente['TipoSangre']; ?></label>
                <label>. EL PACIENTE NO PRESENTA ENFERMEDAD CRÓNICA, NI PATOLÓGICA O TRANSMISIBLE.</label>
                <br><br>                
                <label >SE EXTIENDE EL PRESENTE PARA FINES QUE AL INTERESADO CONVENGAN EN LA CIUDAD DE CHETUMAL, CAPITAL DEL ESTADO DE QUINTANA ROO, 
                    <label type='date' name="fecha" id="fecha"><?php echo date('d-M-Y');?> </label>

            </div>

             <button class=" btn-primary btn-sm">Guardar</button>
            <!--  <button method="POST" type="submit" name="pdf_report" formaction="../../../app/view/Expediente/pdf.php" id="pdf_report" class=" btn-secondary btn-sm" 
             ><i class="fas fa-file-pdf"></i>Imprimir</button> -->
         </form>
        </form>



 <div>
     <form name="test">
        <input type="hidden" name="option" value="">
        <input type="hidden" name="fecha" value="<?php echo date('Y-m-d ') ?>">
        <input type="hidden" name="idPaciente" value="<?php echo $paciente['IdPaciente']; ?>">
        <input type="hidden" name="IdEmpleado" value="<?php echo $_SESSION['inicio'][0]; ?>">

        <input type="hidden" name="nomDoc" value="">
        <input type="hidden" name="puestoDoc" value="">
        <input type="hidden" name="cedDoc" value="">
        <input type="hidden" name="nomPac" value="">
        <input type="hidden" name="sexPac" value="">
        <input type="hidden" name="edadPac" value="">
     </form>

 </div>

<script src="../../../recursos/javascript/reloj.js"></script>
<script src="../../../recursos/javascript/slider.js"></script>
<script src="../../../recursos/javascript/reporte.js"></script>
<script type="text/javascript"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>

<script>
    document.querySelectorAll('textarea');
    //request();
</script>

 <script>
        (function(){
            momentoActual = new Date();
            fecha = momentoActual.getFullYear()+"-"+(momentoActual.getMonth()+1)+"-"+momentoActual.getDate();
            hora = momentoActual.getHours()+":"+momentoActual.getMinutes()+":"+momentoActual.getSeconds();

            //This is what will be sent... for the vaules at top that are type hidden
            document.ConsM[0].value= "ConsM";
            document.ConsM[1].value= fecha+" "+hora;
            document.ConsM[2].value= "<?php echo $paciente['IdPaciente']; ?>";
            document.ConsM[3].value= "<?php echo $_SESSION['inicio'][0]; ?>";

        })();
    </script>

    <script>
        (function(){
            momentoActual = new Date();
            fecha = momentoActual.getFullYear()+"-"+(momentoActual.getMonth()+1)+"-"+momentoActual.getDate();
            hora = momentoActual.getHours()+":"+momentoActual.getMinutes()+":"+momentoActual.getSeconds();

            //This is what will be sent... for the vaules at top that are type hidden
            document.ResC[0].value= "ResC";
            document.ResC[1].value= fecha+" "+hora;
            document.ResC[2].value= "<?php echo $paciente['IdPaciente']; ?>";
            document.ResC[3].value= "<?php echo $_SESSION['inicio'][0]; ?>";

          

        })();
    </script>
        <script>
        (function(){
            momentoActual = new Date();
            fecha = momentoActual.getFullYear()+"-"+(momentoActual.getMonth()+1)+"-"+momentoActual.getDate();
            hora = momentoActual.getHours()+":"+momentoActual.getMinutes()+":"+momentoActual.getSeconds();

            //This is what will be sent... for the vaules at top that are type hidden
            document.CerM[0].value= "CerM";
            document.CerM[1].value= fecha+" "+hora;
            document.CerM[2].value= "<?php echo $paciente['IdPaciente']; ?>";
            document.CerM[3].value= "<?php echo $_SESSION['inicio'][0]; ?>";

                //datos para enviarse al pdf
                //DOC------
                //nombre del doc
                //Puesto
                //ced prof
                //Paciente-----
                //nombre del paciente
                //sexo
                // edad                
                //Tipo de sangre
                //fecha de hoy
            document.CerM[4].value= "<?php echo $_SESSION['inicio'][1]; ?>";
            document.CerM[5].value= "<?php echo $_SESSION['inicio'][2]; ?>";
            document.CerM[6].value= "<?php echo $_SESSION['inicio'][3]; ?>";

            document.CerM[7].value= "<?php echo $paciente['Nombre']; ?>";
            document.CerM[8].value= "<?php echo $paciente['Genero']; ?>";
            document.CerM[9].value= "<?php echo $edad ?>";
            document.CerM[10].value= "<?php echo $paciente['TipoSangre']; ?>";
             

        })();
    </script>
</html>