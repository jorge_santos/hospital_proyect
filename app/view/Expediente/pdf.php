<?php
//include connection file
include_once("../../config/conexion.php");
include_once('../../../recursos/TCPDF-master/tcpdf.php');
 include_once "../../controller/controller_expediente.php";
    session_start();
    
    $paciente = controller_expediente::paciente($_COOKIE['expediente']);
    $nacimiento = explode("-", $paciente['FechaNacimiento']);
    $edad = (date("Y") - $nacimiento[0]);

// $GLOBALS['option'] = isset($_POST['option']) ? $_POST['option'] : '';
// $GLOBALS['DocNom'] =isset($_POST['nomDoc']) ? $_POST['nomDoc'] : '';

if (isset($_POST['pdf_report'])) {
    error_reporting(0);
   
   $option = isset($_POST['option']) ? $_POST['option'] : '';

   $GLOBALS['option'] = isset($_POST['option']) ? $_POST['option'] : '';
   $nomDoc = $_SESSION['inicio'][1]; 
   $GLOBALS['DocNom'] = $nomDoc;
   $puestoDoc = $_SESSION['inicio'][2]; 
   $cedDoc = $_SESSION['inicio'][3];
   $nomPac = $paciente['Nombre'];
   $sexPac = $paciente['Genero'];
   $edadPac = $edad;
   $SangrePac = $paciente['TipoSangre'];

   $docdatosM = isset($_POST['ConsultaM']) ? $_POST['ConsultaM'] : '';
   $docdatosC = isset($_POST['ConsultaC']) ? $_POST['ConsultaC'] : '';
   $GLOBALS['consm'] = $docdatosM;
   $GLOBALS['resm'] = $docdatosC;

   $docdatos="";

     if ( $GLOBALS['consm'] != NULL) {
    $docdatos = $GLOBALS['consm'];
  }
  else $docdatos = $GLOBALS['resm'];

}
//     if($docdatosM != NULL) {
//     $docdatos = $docdatosM;
//   }
//   else $docdatos = $docdatosC;


class PDF extends TCPDF{
   

    //Page header
    public function Header() {
        // Logo
        $image_file1 = K_PATH_IMAGES.'salud.jpg';
        $image_file2 = K_PATH_IMAGES.'sesa.jpg';
        $image_file3 = K_PATH_IMAGES.'quintana.jpg';
        $image_file4 = K_PATH_IMAGES.'oportunidades.jpg';
        $this->Image($image_file1, 40, 20, 40, '', 'jpg', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->Image($image_file2, 80, 10, 30, '', 'jpg', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->Image($image_file3, 110, 10, 30, '', 'jpg', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->Image($image_file4, 150, 20, 30, '', 'jpg', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->Ln(20);
        $this->SetFont('helvetica', '', 8);
        // Title
        $this->Cell(275, 3, 'DEPENDENCIA: SERVICIOS ESTATALES DE SALUD.', 0, 1, 'C');

        $this->SetFont('helvetica', '', 8);
        $this->Cell(272,3,'DIRECCIÓN DE:   HOSPITAL GENERAL DE CHETUMAL',0,1,'C');
        $this->Cell(309,3,'ÁREA:        SUBDIRECCIÓN. ',0,1,'C');
        // $this->Cell(189,3,'1200, Bangladesh.',0,1,'C');
        // $this->Cell(189,3,'Phone:+88 02 xxxxxxx. Fax: xxxxxxx,',0,1,'C');
        // $this->Cell(189,3,'E-mail- example@gmail.com',0,1,'C');
        if($GLOBALS['option']=="CerM")
        {
        $this->SetFont('helvetica', 'B', 20);
        $this->Ln(2);
        $this->Cell(283,3,'Certificado Medico',0,1,'C');
        }
        else if($GLOBALS['option']=="ConsM")
        {
        $this->SetFont('helvetica', 'B', 20);
        $this->Ln(2);
        $this->Cell(283,3,'Constancia Medica',0,1,'C');
        }
        else if($GLOBALS['option']=="ResC")
        {
        $this->SetFont('helvetica', 'B', 20);
        $this->Ln(2);
        $this->Cell(283,3,'Resuemen Clinicio',0,1,'C');
        }
        
    }
    
    //Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-148);
        $this->Ln(50);
        $this->SetFont('times','B',11);
        $this->MultiCell(0, 12,'ATENTAME', 0, 'C', 0, 1, '', '', true);
        $this->MultiCell(0, 2,''. $GLOBALS['DocNom'] .'', 0, 'C', 0, 1, '', '', true);
        $this->MultiCell(0, 20,'DEL HOSPITAL GENERAL', 0, 'C', 0, 1, '', '', true);


        $this->MultiCell(0, 2,'Servicios Estatales de Salud', 0, 'C', 0, 1, '', '', true);
        $this->MultiCell(0, 2,'Av. Chapultepec No. 267  Col. Centro. C.P. 77000', 0, 'C', 0, 1, '', '', true);
        $this->MultiCell(0, 2,'Chetumal, Quintana Roo, México.', 0, 'C', 0, 1, '', '', true);
        $this->MultiCell(0, 2,'Tel.: (983) 83 51921 Ext. 00000', 0, 'C', 0, 1, '', '', true);
        $this->MultiCell(0, 25,'correo@gmail.com', 0, 'C', 0, 1, '', '', true);
         //$this->MultiCell(189, 15,'ATENTAME', 0, 'L',false, 0, '', '', true);
        //$pdf->Write(0, 'HACE CONSTAR', '', 0, 'C', true, 0, false, false, 0);

        
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        date_default_timezone_set("America/Cancun");
        $today = date("d-m-y");
        $this->Cell(25,5,'Generation Date: '.$today,0,0,'L');
        $this->Cell(164, 5, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');

    }
}
$pdf = new PDF('p','mm','LETTER');

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("HOSPITAL GENERAL");
$pdf->SetTitle('REPORTE DE UN PACIENTE');
$pdf->SetSubject('');
$pdf->SetKeywords('');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);


// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->AddPage();    
$pdf->Ln(50);


// $pdf->SetFont('times','B',10);
// $pdf->Cell(189,3,'Report as on :- '.$fecha,0,1,'C');
// $pdf->Ln(5);
//Set font
if($GLOBALS['option']=="CerM")
{

$pdf->SetFont('times','B',12);
$pdf->Cell(130,5,'A QUIEN CORRESPONDA: ',0,0);
//Set font
$pdf->SetFont('times','B',11);
//$pdf->Cell(59,5,'Client Code:- '.$sexPac.'',0,1);
$pdf->Ln(8);
$pdf->Write(2,'EL/LA QUE SUBSCRIBE '.$nomDoc.' '.$puestoDoc.' ADSCRITO AL HOSPITAL GENERAL DE CHETUMAL, DEPENDIENTE DE LOS SERVICIOS ESTATALES DE SALUD, CON CED. PROF. '.$cedDoc.' Y CON AUTORIZACIÓN PARA EJERCICIO DE SU PROFESIÓN:', '', 0, 'L', true, 0, false, false, 0);

$pdf->Ln(5);
$pdf->SetFont('times','B',17);
$pdf->Write(0, 'HACE CONSTAR', '', 0, 'C', true, 0, false, false, 0);
$pdf->Ln(5);

$pdf->SetFont('times','B',11);
$pdf->Write(2,'QUE EL/LA C. '.$nomPac.', '.$sexPac.' DE '.$edadPac.' AÑOS DE EDAD, SE LE REALIZÓ EXAMEN MÉDICO RESULTANDO CLÍNICAMENTE SANO CON TIPO DE SANGRE '.$SangrePac.'. EL PACIENTE NO PRESENTA ENFERMEDAD CRÓNICA, NI PATOLÓGICA O TRANSMISIBLE.', '', 0, 'L', true, 0, false, false, 0);
// $pdf->Cell(59,5,'BO Type: '.$cedDoc.'',0,1);
// $pdf->Ln(5);
// $pdf->Cell(189,5,'Mobile No: '.$cedDoc.'',0,1);
$pdf->Ln(5);
date_default_timezone_set("America/Cancun");
$today = date("Y");
$today2 = date("M");
$today_3 = date("d");
$pdf->Write(2,'SE EXTIENDE EL PRESENTE PARA FINES QUE AL INTERESADO CONVENGAN EN LA CIUDAD DE CHETUMAL, CAPITAL DEL ESTADO DE QUINTANA ROO, EL día '.$today_3.' de '.$today2.' del '.$today.'.', '', 0, 'L', true, 0, false, false, 0);
// $pdf->Cell(59,5,'BO Type: '.$cedDoc.'',0,1);

}elseif ($GLOBALS['option']=="ConsM") {
   
$pdf->SetFont('times','B',12);
$pdf->Cell(130,5,'A QUIEN CORRESPONDA: ',0,0);
//Set font
$pdf->SetFont('times','B',11);
//$pdf->Cell(59,5,'Client Code:- '.$sexPac.'',0,1);
$pdf->Ln(8);
$pdf->Write(2,'EL/LA QUE SUBSCRIBE '.$nomDoc.' '.$puestoDoc.' ADSCRITO AL HOSPITAL GENERAL DE CHETUMAL, DEPENDIENTE DE LOS SERVICIOS ESTATALES DE SALUD, CON CED. PROF. '.$cedDoc.' Y CON AUTORIZACIÓN PARA EJERCICIO DE SU PROFESIÓN:', '', 0, 'L', true, 0, false, false, 0);

$pdf->Ln(5);


$pdf->SetFont('times','B',11);
$pdf->Write(2,'HACE CONSTAR QUE EL/LA C. '.$nomPac.', '.$sexPac.' DE '.$edadPac.' AÑOS DE EDAD, '.$docdatos.'.', '', 0, 'L', true, 0, false, false, 0);
// $pdf->Cell(59,5,'BO Type: '.$cedDoc.'',0,1);
// $pdf->Ln(5);
// $pdf->Cell(189,5,'Mobile No: '.$cedDoc.'',0,1);
$pdf->Ln(5);
$today = date("Y");
$today2 = date("M");
$today3 = date("d");
$pdf->Write(2,'SE EXTIENDE EL PRESENTE PARA FINES QUE AL INTERESADO CONVENGAN EN LA CIUDAD DE CHETUMAL, CAPITAL DEL ESTADO DE QUINTANA ROO, EL día '.$today3.' de '.$today2.' del '.$today.'.', '', 0, 'L', true, 0, false, false, 0);
// $pdf->Cell(59,5,'BO Type: '.$cedDoc.'',0,1);


}
elseif ($GLOBALS['option']=="ResC") {
    
$pdf->SetFont('times','B',12);
$pdf->Cell(130,5,'A QUIEN CORRESPONDA: ',0,0);
//Set font
$pdf->SetFont('times','B',11);
//$pdf->Cell(59,5,'Client Code:- '.$sexPac.'',0,1);
$pdf->Ln(8);
$pdf->Write(2,'EL/LA QUE SUBSCRIBE '.$nomDoc.' '.$puestoDoc.' ADSCRITO AL HOSPITAL GENERAL DE CHETUMAL, DEPENDIENTE DE LOS SERVICIOS ESTATALES DE SALUD, CON CED. PROF. '.$cedDoc.' Y CON AUTORIZACIÓN PARA EJERCICIO DE SU PROFESIÓN:', '', 0, 'L', true, 0, false, false, 0);

$pdf->Ln(5);

$pdf->SetFont('times','B',11);
$pdf->Write(2,'SE TRATA DE(L)/LA C. '.$nomPac.', '.$sexPac.' DE '.$edadPac.' años de edad, '.$docdatos.'', '', 0, 'L', true, 0, false, false, 0);
// $pdf->Cell(59,5,'BO Type: '.$cedDoc.'',0,1);
// $pdf->Ln(5);
// $pdf->Cell(189,5,'Mobile No: '.$cedDoc.'',0,1);
$pdf->Ln(5);
$today = date("Y");
$today2 = date("M");
$today3 = date("d");
$pdf->Write(2,'SE EXTIENDE EL PRESENTE PARA FINES QUE AL INTERESADO CONVENGAN EN LA CIUDAD DE CHETUMAL, CAPITAL DEL ESTADO DE QUINTANA ROO, EL día '.$today3.' de '.$today2.' del '.$today.'.', '', 0, 'L', true, 0, false, false, 0);
// $pdf->Cell(59,5,'BO Type: '.$cedDoc.'',0,1);

}
$pdf->Output('Report-'.$nomPac.' .pdf', 'I');



?>