<?php
session_start();

if ( isset($_SESSION['inicio']) ) {
	if($_SESSION['inicio'][2] == "Medico"){
		header("Location: app/view/view_Medicos/view_doctor.php");
	}
	if($_SESSION['inicio'][2] == "Recepcionista"){
		header("Location: app/view/view_agenda/view_agendarcita.php");
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href= "recursos/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href= "recursos/estilos/login.css">
	<title>login</title>
</head>
<style>
.color{
	background-color:#57BC90;
}
#segundo{
	background-color:#77C9D4;
}
.segundo{
	background-color:#015249;
}
h2{
	color:#ffffff;
}
</style>

<body class="color">
	<div id="login" class="segundo" >
		<h2>Inicio de sesión</h2>
		<noscript>
        	<center>
            	<h1>Habilitar javascript para una mejor experiencia</h1>
        	</center>
    	</noscript>
		<p id="mensaje"></p>
		
		<div class="col-sm" id="segundo" >
			<img src="./recursos/imagenes/doc.png">
			<form method="POST" id="ingreso">
				<label for="usuario">Usuario</label>
				<input type="text" name="usuario" id="usuario" class="form-control" size="15" placeholder="usuario"><br><br>
				<label for="contraseña">Contraseña</label>
				<input type="password" name="pass" id="contraseña" class="form-control"placeholder="contraseña"><br>
			</form>

			<button id="ok" class="btn btn-accept">Ingresar</button>
			<button id="null" class="btn btn-cancel">Cancelar</button>
		</div>
	</div>

</body>

<script src="recursos/javascript/login.js"></script>
</html>