<?php
session_start(); 

if(isset($_REQUEST['ventana'])){
    header("Location: ../Expediente/view_expediente.php");
}

if ( !isset($_SESSION['inicio']) ){
    header("Location: ../../..");
}

if (isset($_REQUEST['lobby'])){
    header("Location: ../../..");
    session_destroy();
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dcitas.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dbusqueda.css">
    <link rel=" stylesheet" href="../../../recursos/estilos/doctores/Dexpedientes.css">
    <title>Doctores</title>
</head>

<body onload="mueveReloj()">

    <div id="buscador">
        <div class="cabecera">
            <h2>Doctores</h2>
        </div>
        <hr>
        <center>
            <div>
                <h3 style="color: white"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h3>
                <a href="view_doctor.php" class="btn">Citas</a>
                <a focus="true" class="btn">Búsqueda</a>
                <form method="post" id="salir">
                    <button class="btn" id="lobby" name="lobby" value="lobby">Cerrar sesión</button>
                </form>
            </div>
        </center>
    </div>
</div>

<div style="display: flex;justify-content: space-between;">
    <img src="../../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()" alt="Ocultar">
    <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>    
</div>

<hr>

<div id="Dcitas" class="modulos">
    <noscript>
        <center>
            <h1>Habilitar javascript para una mejor experiencia</h1>
        </center>
    </noscript>
    <center><h3>Busqueda de expedientes</h3></center>
    <hr>
    <label for="a">Buscar por</label>
    <select name="a" id="tipos" onchange="busqueda(document.getElementsByClassName('table-responsive'))">
        <option>Elija una opción</option>
        <option value="folio">Folio</option>
        <option value="apellido">Apellidos</option>
    </select>
    <hr>
    <!-- div por folio -->
    <div id="Bid" class="tras">
        <h2>Búsqueda por Folio</h2>
        <div>
            <label for="folio">Folio:</label>
            <input type="text" name="folio" id="fol" placeholder="Número de expediente">
            <button class="btn-accept" id="buscarfol">Buscar</button>
        </div>

        <div>
            <hr>
            <center>
                <div class="table-responsive" id="resultado1" >
                    <h1 id="mensaje"></h1>

                </div>
            </center>

        </div>
    </div>

    <!-- busqueda por apellidos -->
    <div id="BAp">
        <h2>Búsqueda por Apellidos</h2>
        <div class="Datos">
            <div>
                <label for="apellidoP">Nombre(s)</label>
                <input type="text" name="apellidoP" id="dato1" placeholder="Nombre del paciente" >
            </div>
            <div>
                <label for="apellidoM">Apellido paterno</label>
                <input type="text" name="apellidoM" id="dato2" placeholder="Primer apellido" >
            </div>
            <div>
                <label for="Nombre">Apellido materno</label>
                <input type="text" name="Nombre" id="dato3" placeholder="Segundo apellido">
            </div>
            <button class="btn-accept" id="buscarAp">Buscar</button>
        </div>
        
        <center>
            <div class="table-responsive" id="resultado2" >
                <h1 id="mensaje"></h1>

            </div>
        </center>
    </div>

</div>
</body>

<script src="../../../recursos/javascript/busqueda.js"></script>
<script src="../../../recursos/javascript/reloj.js"></script>
<script src="../../../recursos/javascript/slider.js"></script>

<script type="text/javascript">

</script>

</html>