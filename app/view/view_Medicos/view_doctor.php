<?php
session_start();
include_once "../../controller/controller_doctor.php";
//dirname(dirname(__DIR__))

if ( !isset($_SESSION['inicio']) ){
    header("Location: ../../..");
}

if (isset($_REQUEST['lobby'])){
    header("Location: ../../..");
    session_destroy();
}

$controller = new controller_doctor(); 

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="../../../recursos/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/doctor.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dcitas.css">
    <link rel="stylesheet" href="../../../recursos/estilos/doctores/Dbusqueda.css">
    <link rel=" stylesheet" href="../../../recursos/estilos/doctores/Dexpedientes.css">

    <title>Doctores</title>
</head>

<body onload="mueveReloj()">

    <div id="buscador">
        <div class="cabecera">
            <h2>Doctores</h2>
        </div>
        <hr>
        <center>
            <div>
                <h3 style="color: white"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h3>
                <a focus="true" class="btn">Citas</a>
                <a href="view_busqueda.php" class="btn">Búsqueda</a>
                <form method="post" id="salir">
                    <button class="btn" id="lobby" name="lobby" value="lobby">Cerrar sesión</button>
                </form>
            </div>
        </center>
    </div>
</div>

<div style="display: flex;justify-content: space-between;">
    <img src="../../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()" alt="Ocultar">
    <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
</div>
<hr>

<div id="Dcitas" class="modulos">
    <noscript>
        <center>
            <h1>Habilitar javascript para una mejor experiencia</h1>
        </center>
    </noscript>
    <center>
        <div class="table-responsive">
            <h2>Citas medicas</h2>
            <hr>
            <?php controller_doctor::vista(); ?>
        </div>
    </center>
</div>

</body>

<script src="../../../recursos/javascript/reloj.js"></script>
<script src="../../../recursos/javascript/slider.js"></script>

<script type="text/javascript">

</script>

</html>