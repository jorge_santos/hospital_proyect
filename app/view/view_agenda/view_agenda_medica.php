<?php
session_start();

if (isset($_REQUEST['lobby'])){
    header("Location: ../../..");
    session_destroy();
}
?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agenda Medica</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <link rel="stylesheet" href="../../../recursos/estilos/doctores/css1.css">
</head>

<body>
  <header>
    <div>
      <h2>Doctores</h2>
    </div>
  </header>

  
  <div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
      <div class="sidebar-header">
        <h3>Agenda Medica </h3>
      </div>

      <ul class="list-unstyled components">   
        <li class="active"><a href="#menu">Agenda Diaria</a></li>
        <li class="active"><a href="#menu">Agregar Cita</a></li>
          <br><br><br><br><br><br><br>
              <form method="post">
                    <button class="btn" id="lobby" name="lobby" value="lobby">Cerrar sesión</button>
          </form>
      </ul>
    </nav>
  </div>

  <div id="content">   
    <h4>Busqueda por Folio</h4>
    <form method="post">
      <label for="folio">Folio:</label>
      <input type="text" name="folio" id="fol" placeholder="Numero de expediente" >
      <input  class="btn-accept" type="submit" value="aceptar">
               
      <div class="md-form">
        <input type="date" id="form1" class="form-control">
        <label for="form1">Calendario</label>
      </div>              
    </form>

  <center>
    <div class="table-responsive">

      <hr>

        <caption><h2>Lista de Pacientes</h2></caption>
        <table class="table" >
          <thead>
            <tr>
              <th>Folio</th>
              <th>Nombre</th>
              <th>Detalles</th>
              <th>Acciones</th>
            </tr>
          </thead>
          
          <tbody>
            <tr>
              <td>15368745</td>
              <td>masiso masiso</td>            
              <td>
              
                <div>
                  <select>
                    <option value="Doctor">Juan Lucas Lucas</option>
                  </select>
                                    
                  <select>
                    <option value="cardiologo">cardiologo</option>
                  </select>
                  <select>
                    <option value="matutino">matutino</option>
                  </select>
                </div>
              </td>
              
              <td>
                <button class="button">Citar</button>
                <button class="button button2">Modificar</button>
                <button class="button button3">Eliminar</button>
              </td>
            </tr>  
          </tbody>
        </table>
      </center>
    </div>
  </div>

</body>
 <!-- jQuery CDN -->
 <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
  <!-- Bootstrap Js CDN -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- jQuery Custom Scroller CDN -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

  

  <script type="text/javascript">
    $(document).ready(function() {


      $('#sidebarCollapse').on('click', function() {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
      });
    });
  </script>
</html>
