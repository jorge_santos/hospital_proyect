<?php
//incluye la clase Libro y CrudLibro
session_start();

if ( !isset($_SESSION['inicio']) ){
    header("Location: ../../..");
}

if (isset($_REQUEST['lobby'])){
    header("Location: ../../..");
    session_destroy();
}
require_once dirname(__DIR__).'../../config/conexion.php';

require_once dirname(__DIR__).'../../controller/controller_ListaInterconsulta.php';

$crud=new CrudPaciente();
$paciente= new Interconsultante();

//obtiene todos los libros con el método mostrar de la clase crud

 $Idinterconsulta = isset($_GET['IdInterconsulta']) ? $_GET['IdInterconsulta'] : '';


	if ( !(empty($Idinterconsulta)) ){
        $resultados=$crud->mostrarInterconsultaPaciente($Idinterconsulta);
	}else{

        header('Location: /proyecto/app/view/view_agenda/view_interconsulta.php');
    }



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <script>
        function showUser(str) {
            if (str == "") {
                document.getElementById("txtHint").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txtHint").innerHTML = this.responseText;
                }
            }
            xmlhttp.open("GET", "componente.php?q=" + str, true);
            xmlhttp.send();
        }
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../../recursos/estilos/css1.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
        integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <title>Archivo Clinico</title>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
        <div class="sidebar-header text-center">
                <h4>Archivo Clínico</h4>
            </div>

            <ul class="list-unstyled components">

            <li class="active">
                    <h5 class="text-capitalize text-center"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h5>
                    <br>
                    <form>
                    <a href="view_agenda.php" class="btn btn-danger">Agenda General</a>
                    <a href="view_agendarcita.php" class="btn btn-secondary">Agendar Cita</a>
                    <a href="view_crearexpediente.php" class="btn btn-secondary">Crear Expediente</a>
                    <a href="view_interconsulta.php"class="btn btn-secondary">Interconsulta</a>
                    </form>
                    <form method="post">
                    <center>
                        <button class="btn btn-warning" id="lobby" name="lobby" value="lobby" style="margin: 50% 0%;">Cerrar sesión</button>
                        </center>
                    </form>

                </li>

            </ul>


        </nav>

    </div>

    <!--- parte del cuerpo-->
    <div id="content">
        <form action='../../model/model_agenda/modelAgendarInterconsulta.php' method='post' name="interconsulta">
            <div class="Datos_personales" style="width: 40rem;">
            <div class="card"> 
            <div class="card-body">
            <?php foreach ($resultados as $paciente) {?>
                
                   

                        <h5 class="card-title font-weight-light text-center">
                            Interconsulta</h5>
                            <h5 class="text-capitalize text-center"> <?php echo $paciente->getNombrePaciente()." ". $paciente->getApellidoPaternoPaciente()." ".$paciente->getApellidoMaternoPaciente();?>
                           
                            <input type="hidden" name="IdInterconsulta" value="<?php echo $Idinterconsulta ?>">                           
                        </h5>



                    </div>

                </div>

                <div>
 <br>
                    <h5 for="Especialidad">Especialidad</h5>
                    <label for=""><?php echo $paciente->getEspecialidad() ?> </label>
                     
                    

                    <br>

                    <div id="txtHint">
                    <br>
                    <h5 for="Especialidad">Doctor</h5>
                    <label for=""> <?php echo $paciente->getNombreDoctor()." ". $paciente->getApellidoPaternoDoctor()." ".$paciente->getApellidoMaternoDoctor();
                    ?></label>
                    </div>
                    <?php }?>
                    <br>
                    <h5>Fecha</h5>
                    <input name="Fecha" class="form-control" type="date" required>
                    <br>
                    <button name="submit" type="submit" class="btn btn-danger active">Actualizar</button>

                </div>

            </div>
        </form>
    </div>




</body>
<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- jQuery Custom Scroller CDN -->
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>


<script type="text/javascript">
    $(document).ready(function () {


        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>

</html>