<?php
session_start();
require_once dirname(__DIR__).'../../config/conexion.php';

require_once dirname(__DIR__).'../../controller/controller_ListaInterconsulta.php'; 
$crud=new CrudPaciente();
$Pacientes= new Interconsultante();
//obtiene todos los libros con el método mostrar de la clase crud
 
$filtroEstado = isset($_POST['filtroEstado']) ? $_POST['filtroEstado'] : '';	
$idPaciente= isset($_POST['folio']) ? $_POST['folio'] : '';
$nombre= isset($_POST['nombre']) ? $_POST['nombre'] : '';
$papellido= isset($_POST['papellido']) ? $_POST['papellido'] : '';
$sapellido= isset($_POST['sapellido']) ? $_POST['sapellido'] : '';
if (empty($filtroEstado)){

    $NuevoPaciente=$crud->mostrarInterconsulta();
}


if ( !isset($_SESSION['inicio']) ){
    header("Location: ../../..");
}

if (isset($_REQUEST['lobby'])){
    header("Location: ../../..");
    session_destroy();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../../recursos/estilos/css1.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
    integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <title>Archivo Clinico</title>
</head>

<body onload="showUser(0)">
    <header>
        <div>
            <h2>Archivo Clinico</h2>
        </div>
    </header>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>Archivo Clinico </h4>
            </div>

            <ul class="list-unstyled components">
                <li class="active">
                <h5 class="text-capitalize text-center"><?php echo isset($_SESSION['inicio']) ? $_SESSION['inicio'][1] : ''; ?></h5>
                    <br>
                    <form>
                    <a href="view_agenda.php" class="btn btn-secondary">Agenda General</a>
                    <a focus="true" class="btn btn-secondary">Agendar Cita</a>
                    <a href="view_crearexpediente.php" class="btn btn-secondary">Crear Expediente</a>
                    <a focus="true" class="btn btn-danger">Interconsulta</a>
                    <form>
                    <form method="post">
                    <center>
                        <button class="btn btn-warning" id="lobby" name="lobby" value="lobby" style="margin: 50% 0%;">Cerrar sesión</button>
                        </center> </form>
                </li>
            </ul>
        </nav>

    </div>
    <div style="display: flex;justify-content: space-between;">
        <img src="../../recursos/iconos/icons8-menú-filled-25.png" onclick="ocultar()" alt="Ocultar">
        <div>Fecha <span id="fecha"></span> Hora <span id="hora"></span></div>
    </div>
    <!--- parte del cuerpo-->
    <div id="content">
        <div class="container">

                    <!-- Button to Open the Modal -->

                    <div class="table-responsive">
                        <br>
                        <caption>
                            <h5>Lista de Pacientes Interconsultantes</h5>
                            <br>
                        </caption>
                        <table class="table table-striped table-sm">

                            <tr>
                                <th>Nº Expediente</th>
                                <th>Nombre</th>
                                <th>Fecha de interconsulta</th>
                                <th>Especialidad</th>
                                <th>Doctor</th>
                                <th>Observaciones</th>
                                <th>Agendar</th>
                            </tr>


                            <?php foreach ($NuevoPaciente as $Pacientes) {?>

                                <tr> 
                                    <td><?php echo $Pacientes->getFolio() ?></td>
                                    <td><?php echo $Pacientes->getNombrePaciente()." ". $Pacientes->getApellidoPaternoPaciente()." ".$Pacientes->getApellidoMaternoPaciente();
                                    ?></td>

                                    <td><?php echo $Pacientes->getFechaInterconsulta() ?></td>
                                    <td><?php echo $Pacientes->getEspecialidad() ?></td>
                                    <td><?php echo $Pacientes->getNombreDoctor()." ". $Pacientes->getApellidoPaternoDoctor()." ".$Pacientes->getApellidoMaternoDoctor();
                                    ?> </td>
                                    <td><?php echo $Pacientes->getObservaciones() ?></td>

                                    <td><a class="btn btn-danger fas fa-address-book"
                                        href="view_citaInterconsulta.php?IdInterconsulta=<?php echo $Pacientes->getIdInterconsulta()?>"></a> 
                                    </td>

                                </tr>
                            <?php }?>

                        </table>

                    </div>
                </div>
            </div>

        </body>
        <!-- jQuery CDN -->
        <script src="../../recursos/javascript/reloj.js"></script>
        <script src="../../recursos/javascript/slider.js"></script>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {


                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
            function showUser(str) {
                if (str == 0) {
                    document.getElementById("porNombre").style.visibility = "hidden";
                    document.getElementById("porFolio").style.visibility = "hidden";
                }
                if (str == 1) {
                    document.getElementById("porNombre").style.visibility = 'visible';
                    document.getElementById("porFolio").style.visibility = "hidden";
                }
                if (str == 2) {
                    document.getElementById("porFolio").style.visibility = "visible";
                    document.getElementById("porNombre").style.visibility = 'hidden';
                }
            }


        </script>
</html>